
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author xdohnal
 */
public class StatsToDB {
    public static void main(String... args) throws FileNotFoundException, IOException {
        String treeFile = "mtree-top9000-leaf100.txt";
        String logFile = "mtree-top9000-leaf100-knn30-all.txt";
        String db = "";
        
        // Structure log...
        try (BufferedReader log = new BufferedReader(new FileReader(treeFile))) {
            String line;
            while ( (line = log.readLine()) != null ) {
                if (line.startsWith("  Leaves:")) {
                    
                    continue;
                }
                if (line.startsWith("Unsorted leaf-node occupations:")) {
                    continue;
                }
            }
        }
         
        // Query log...
        Pattern pOper = Pattern.compile("^.*\\(([^\\)]*)\\).*returned ([0-9]*) objects.*$");
        Pattern pStat = Pattern.compile("^([^:]+): ((?:[0-9.]+)|{(?:[^}]*)})$");
        
        try (BufferedReader log = new BufferedReader(new FileReader(logFile))) {
            String line;
            String qId = null;
            int qAns = 0;
            Map<String,String> stats = new HashMap<>();
            while ((line = log.readLine()) != null) {
                // New query
                if (line.startsWith("KNNQueryOperation ")) {
                    // Process prev query
                    if (qId != null) {
                        
                        stats.clear();
                    }
                    // Read new query
                    Matcher m = pOper.matcher(line);
                    if (m.matches()) {
                        qId = m.group(1);
                        qAns = Integer.valueOf(m.group(2));
                    }
                    continue;
                }
                // Read stats...
                Matcher mStat = pStat.matcher(line);
                if (mStat.matches()) {
                    stats.put(mStat.group(1), mStat.group(2));      // Int stat / Hash stat
                    continue;
                }
            }
        }
    }
}
