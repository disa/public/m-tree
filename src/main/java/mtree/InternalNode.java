package mtree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;

/**
 * M-tree's fork
 * Changes:
 *    = node capacity is in number of objects now!!!
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class InternalNode extends Node implements Serializable {
    private static final long serialVersionUID = 2L;
    
    /** soft capacity of this node, this capacity can be exceeded (in objects) */
    private long softCapacity;
    
//    /** current occupation of this node in objects */
//    private long occupation = 0l;
    
    /** list of all internal node entries */
    private List<NodeEntry> entryList = new ArrayList<NodeEntry>();
    
    
    
    /****************** Constructors ******************/
    
    public InternalNode(int level, NodeEntry parentNodeEntry, long softCapacity) {
        super(level, parentNodeEntry);
        this.softCapacity = softCapacity;
    }
    
    
    /****************** Methods ******************/
    
    /**
     *  Returns <tt>false</tt> if the capacity of this node after inserting
     *  object o will be exceeded.
     *
     *  @return <tt>false</tt> if the capacity of this node after inserting
     *  object o will be exceeded
     */
    public boolean isFreeCapacity(LocalAbstractObject o) {
        //return occupation + 1 <= softCapacity;
        return entryList.size() < softCapacity;
    }
    
    /**
     *  Creates a new node entry in this node.
     *
     *  @param nhr length of HR array
     *  @return a new node entry
     */
    public NodeEntry createNodeEntry(LocalAbstractObject p, Node subtree, int nhr) {
        
        // Creation of array HR of new object o.
        NodeEntry.HREntry[] hr = new NodeEntry.HREntry[nhr];
        for (int i = 0; i < nhr; i++)
            hr[i] = new NodeEntry.HREntry();
        
        NodeEntry ne = new NodeEntry(this, p, subtree, hr);
        return this.createNodeEntry(ne);
    }
    
    /**
     *  Adds the specified node entry into this node.
     *
     *  @return added node entry
     */
    public NodeEntry createNodeEntry(NodeEntry ne) {
        
        ne.setNode(this);
        entryList.add(ne);
        //occupation++;
        
        return ne;
    }
    
    /**
     *  Returns the node entry at specified position.
     *
     *  @return the node entry at specified position
     */
    public NodeEntry getNodeEntry(int pos) {
	return entryList.get(pos);
    }
    
    /**
     *  Returns the node entry selected by an object stored in it.
     *
     *  @return the node entry selected by an object stored in it
     */
    public NodeEntry getNodeEntry(LocalAbstractObject o) {
        for (NodeEntry ne: entryList) {
            if (ne.getObject().equals(o))
                return ne;
        }
        return null;
    }
    
    /**
     *  Returns a list of stored node entries of this node.
     *
     *  @return a list of stored node entries of this node
     */
    public List<NodeEntry> getNodeEntryList() {
        return entryList;
    }
    
    /**
     *  Removes the specified node entry from this node.
     *
     *  @return <tt>true</tt> if the specified node entry has been removed
     */
    public boolean removeNodeEntry(NodeEntry ne) {
        if (entryList.remove(ne)) {
            return true;
        }
        return false;
    }
    
    /**
     *  Removes the specified node entry from this node.
     *
     *  @return <tt>true</tt> if the specified node entry has been removed
     */
    public boolean removeNodeEntry(NodeEntry ne, Iterator<NodeEntry> nodeEntryIterator) {
        nodeEntryIterator.remove();
        return true;
    }
    
    
    /****************** Implemented class Node ******************/
    
    /**
     *  Returns <tt>false</tt> if the capacity of this node is not exceeded.
     *
     *  @return <tt>false</tt> if the capacity of this node is not exceeded
     */
    public boolean isCapacityExceeded() {
        return entryList.size() > softCapacity;
    }
    
    /**
     *  Returns the number of stored objects in this node.
     *
     *  @return the number of stored objects in this node
     */
    public int getObjectCount() {
        return entryList.size();
    }

    @Override
    public int getNodeCount() {
        int cnt = 1;
        for (NodeEntry ne : entryList)
            cnt += ne.getSubtree().getNodeCount();
        return cnt;
    }
    
    /**
     *	Returns an iterator of all objects stored in this node.
     *
     *  @return an iterator of all objects stored in this node
     */
    public AbstractObjectIterator<LocalAbstractObject> getObjects() {
        AbstractObjectList<LocalAbstractObject> objects = new AbstractObjectList<LocalAbstractObject>();
        for (NodeEntry ne: entryList)
            objects.add(ne.getObject());
        return objects.iterator();
    }
    
    /**
     *  Adjusts the radius of the parent node.
     *
     *  @return <tt>false</tt> if the radius of the parent node hasn't needed
     *  to be adjusted
     */
    protected boolean adjustParentRadius() {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null)
            return false;
        
        // Computation of the radius.
        float r = 0f;
        for (NodeEntry ne: getNodeEntryList()) {
            float d = ne.getParentDist() + ne.getRadius();
            if (r < d)
                r = d;
        }
        
        // Adjustment of the radius.
        if (r != pne.getRadius())
            pne.setRadius(r);
        pne.getNode().adjustParentRadius();
        return true;
    }
    
    /**
     *  Adjusts the distances of HR array of the parent node.
     *
     *  @return <tt>false</tt> if the distances of the parent node haven't
     *  needed to be adjusted
     */
    protected boolean adjustParentHR() {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null)
            return false;
        
        // Computation of the parent HR array.
        NodeEntry.HREntry[] hr = pne.getHR();
        int nhr = hr.length;
        float[] min = new float[nhr];
        float[] max = new float[nhr];
        Arrays.fill(min, Float.MAX_VALUE);
        Arrays.fill(max, 0f);
        
        for (NodeEntry ne: this.getNodeEntryList()) {
            NodeEntry.HREntry[] nehr = ne.getHR();
            for (int l = 0; l < nhr; l++) {
                float minDist = nehr[l].getMin();
                float maxDist = nehr[l].getMax();
                if (minDist < min[l]) min[l] = minDist;
                if (maxDist > max[l]) max[l] = maxDist;
            }
        }
        
        // Adjustment of the parent HR array.
        for (int l = 0; l < nhr; l++) {
            if (hr[l].getMin() != min[l])
                hr[l].setMin(min[l]);
            if (hr[l].getMax() != max[l])
                hr[l].setMax(max[l]);
        }
        pne.getNode().adjustParentHR();
        return true;
    }
    
    
    /****************** Overrided class Object ******************/
    
    public Node findNode(Predicate<Node> cond) {
        if (cond.test(this))
            return this;
        
        for (NodeEntry e : entryList) {
            Node n = e.getSubtree();
            if (n.isLeaf()) {
                if (cond.test(n))
                    return n;
            } else {
               Node found = ((InternalNode)n).findNode(cond);
               if (found != null)
                   return found;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return "Internal node (#" + nodeIdPerLevel + ", " + entryList.size() + " entries)";
    }
    
}
