/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtree.distributed;

import java.io.IOException;
import java.io.Serializable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.AlgorithmMethodException;
import messif.algorithms.DistAlgReplyMessage;
import messif.algorithms.DistAlgRequestMessage;
import messif.algorithms.DistributedAlgorithm;
import messif.netcreator.CantStartException;
import messif.netcreator.MessageActivate;
import messif.netcreator.MessageActivateResponse;
import messif.network.Message;
import messif.network.NetworkNode;
import messif.network.Receiver;
import messif.network.ReplyMessage;
import messif.network.ReplyReceiver;
import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.OperationErrorCode;
import messif.operations.RankingSingleQueryOperation;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.InsertOperation;
import mtree.MTree;

/**
 * Primitive implementation of distributed M-tree.
 * 
 * Insertion is done in a round-robin way based on modulo cf hashes of object locators.
 * Querying is done on all nodes in the cluster.
 * 
 * Cluster is formed by calling {@link #join } on all nodes.
 * 
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 */
public class DistributedMTree extends DistributedAlgorithm implements Serializable {
    /** class id for serialization */
    private static final long serialVersionUID = 1L;
    
    private final Logger log = Logger.getLogger(DistributedMTree.class.getName());
    
    private final MTree alg;
    private final int algId;
    
    private final Map<Integer,NetworkNode> cluster;
    
    private final Receiver receiverActivate;

    /** Run all computations as response to received messages in this executor */
    protected final ThreadPoolExecutor executor;
    
    /**
     * **************** Constructors of M-tree *****************
     */
    /**
     * Creates original M-tree algorithm.
     *
     * @param algorithmName the name of this algorithm
     * @param nodeID the sub-identification of this algorithm's dispatcher for the higher level
     * @param port the TCP/UDP port on which this distributed algorithm communicates
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     */
    @Algorithm.AlgorithmConstructor(description = "Distributed M-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects"})
    public DistributedMTree(String algorithmName, int nodeID, int port, long intNodeCap, long leafCap) throws AlgorithmMethodException, InstantiationException, IOException {
        super(algorithmName, port + 1);
        
        this.executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        
        this.receiverActivate = new ReceiverImpl();
        
        messageDisp.registerReceiver(receiverActivate);
        cluster = new HashMap<>();
        
        alg = new MTree(intNodeCap, leafCap);
        algId = nodeID;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("DistributedMTree: ");
        sb.append("node=").append(getName()).append("\n");
        sb.append(alg);
        return sb.toString();
    }
    
    @Override
    public void start() throws CantStartException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public final void join(String hosts) throws UnknownHostException, IOException {
        // Store list of nodes in the cluster
        log.log(Level.INFO, "Cluster nodes: {0}", hosts);
        String[] lst = hosts.split("[ ,]+");
        for (int hostID = 0; hostID < lst.length; hostID++) {
            String[] parts = lst[hostID].split(":");
            NetworkNode node = new NetworkNode(parts[0], Integer.valueOf(parts[1]) + 1);
            cluster.put(hostID, node);
        }
        // Send knock-knock messages to get ready...
        boolean allReady = true;
        for (int attempt = 0; attempt < 5; attempt++) {
            log.log(Level.INFO, "Cluster join attempt...");
            List<ReplyReceiver> replies = new ArrayList<>();
            for (Map.Entry<Integer,NetworkNode> e : cluster.entrySet()) {
                log.log(Level.INFO, "   Sending joining message to node: {0}", lst[e.getKey()]);
                replies.add(messageDisp.sendMessageWaitReply(new MessageActivate(null), e.getValue()));
            }
            for (int hostID = 0; hostID < replies.size(); hostID++) {
                try {
                    ReplyReceiver recv = replies.get(hostID);
                    recv.getReplies(3000);
                    if (!recv.isFinished()) {
                        allReady = false;
                        log.log(Level.WARNING, "   Joining node for partition {0} failed.", hostID);
                    }
                } catch (InterruptedException ex) {
                    log.log(Level.WARNING, "   Joining node for partition {0} interrupted.", hostID);
                }
            }
            if (allReady)
                break;
        }
        // Result is...
        if (allReady)
            log.log(Level.INFO, "Cluster ready.");
        else
            log.log(Level.SEVERE, "Cluster cannot be established!");
    }

    private int getAddr(LocalAbstractObject obj) {
        int h = obj.getLocatorURI().hashCode();
        h &= ~(1 << (Integer.SIZE-1));         // Clear the highest bit (the cluster can hardly by as large as 2^32-1 computers)
        return h % cluster.size();
    }
    
    /** Single object insertion
     * @param oper operation
     * @param msgNotUsed not used
     */
    public void insert(InsertOperation oper, DistAlgRequestMessage msgNotUsed) {
        if (msgNotUsed != null) 
            return;
        NetworkNode node = cluster.get(getAddr(oper.getInsertedObject()));
        try {
            messageDisp.sendMessageWaitReply(createRequestMessage(oper), node);
            oper.endOperation();
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Insertion failed due to communication error.", ex);
            oper.endOperation(OperationErrorCode.UNKNOWN_ERROR);
        }
    }

    /** Bulk object insertion
     * @param oper operation
     * @param msgNotUsed not used
     */
    public void insert(BulkInsertOperation oper, DistAlgRequestMessage msgNotUsed) {
        if (msgNotUsed != null) 
            return;
        boolean allDone = true;
        // Partition data
        Map<Integer, List<LocalAbstractObject>> split = new HashMap<>(cluster.size());
        for (int i = 0; i < cluster.size(); i++)
            split.put(i, new ArrayList<LocalAbstractObject>());
        for (LocalAbstractObject obj : oper.getInsertedObjects()) {
            int hostID = getAddr(obj);
            split.get(hostID).add(obj);
        }
        // Store data in the cluster
        Map<Integer,ReplyReceiver<? extends ReplyMessage>> replies = new HashMap<>();
        for (int i = 0; i < cluster.size(); i++) {
            if (split.get(i).isEmpty())
                continue;
            DistAlgRequestMessage m = createRequestMessage(new BulkInsertOperation(split.get(i)));
            try {
                replies.put(i, messageDisp.sendMessageWaitReply(m, DistAlgReplyMessage.class, false, cluster.get(i)));
            } catch (IOException ex) {
                log.log(Level.SEVERE, "Insertion failed due to communication error.", ex);
                allDone = false;
                break;
            }
        }
        // Wait for response
        for (Map.Entry<Integer, ReplyReceiver<? extends ReplyMessage>> e : replies.entrySet()) {
            Integer k = e.getKey();
            ReplyReceiver<? extends ReplyMessage> recv = e.getValue();
            try {
                List<? extends ReplyMessage> r = recv.getReplies();
                if (recv.isFinished()) {
                    mergeOperationsFromReplies(oper, (Collection<? extends DistAlgReplyMessage>) r);
                    mergeStatisticsFromReplies(getOperationStatistics(), r);
                } else {
                    log.log(Level.WARNING, "Procesing on node for partition {0} failed.", k);
                    allDone = false;
                }
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, "Processing on node for partition {0} interrupted.", k);
            }
        }
        if (allDone) {
            oper.endOperation();
            //log.log(Level.INFO, "Distributed M-tree: Insertion finished. Objects inserted {0}", oper.getInsertedObjects().size());
        } else {
            oper.endOperation(OperationErrorCode.UNKNOWN_ERROR);
        }
    }

    /** Range or kNN search
     * @param oper query operation
     * @param msgNotUsed not used
     */
    public void search(RankingSingleQueryOperation oper, DistAlgRequestMessage msgNotUsed) {
        if (msgNotUsed != null) 
            return;
        try {
            ReplyReceiver<? extends ReplyMessage> recv = messageDisp.sendMessageWaitReply(createRequestMessage(oper), DistAlgReplyMessage.class, false, cluster.values());
            List<? extends ReplyMessage> replies = recv.getReplies();
            if (recv.isFinished()) {
                mergeOperationsFromReplies(oper, (Collection<? extends DistAlgReplyMessage>) replies);
                mergeStatisticsFromReplies(getOperationStatistics(), replies);
                oper.endOperation();
                //log.log(Level.INFO, "Distributed M-tree: Query finished. {0}", oper);
            } else {
                oper.endOperation(OperationErrorCode.UNKNOWN_ERROR);
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Searching failed due to communication error.", ex);
        } catch (InterruptedException ex) {
            log.log(Level.SEVERE, "Searching interrupted.", ex);
        }
    }
    
    private abstract class Processor<T extends Message> implements Runnable {
        protected final T msg;

        public Processor(T msg) {
            this.msg = msg;
        }
    }
    
    private class ActivationProcessor extends Processor<MessageActivate> {
        public ActivationProcessor(MessageActivate msg) {
            super(msg);
        }
        
        @Override
        public void run() {
            log.log(Level.INFO, "Activation message from {0}", msg.getSender());
            try {
                messageDisp.replyMessage(new MessageActivateResponse(MessageActivateResponse.ACTIVATED, msg));
            } catch (IOException ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
    }

    private class OperationProcessor extends Processor<DistAlgRequestMessage> {
        public OperationProcessor(DistAlgRequestMessage msg) {
            super(msg);
        }
        
        @Override
        public void run() {
            DistAlgRequestMessage req = (DistAlgRequestMessage)msg;
            log.log(Level.INFO, "Algorithm request message from {0}", req.getSender());
            try {
                try {
                    alg.executeOperation(req.getOperation());
                    req.getOperation().endOperation();
                    //log.log(Level.INFO, "M-tree processed operation: {0}", req.getOperation().toString());
                    if (req.getOperation() instanceof BulkInsertOperation)
                        alg.markObjectsWithBucketIds();
                } catch (AlgorithmMethodException ex) {
                    log.log(Level.INFO, "M-tree failed while processing operation!", ex);
                } catch (NoSuchMethodException ex) {
                    log.log(Level.INFO, "M-tree cannot process operation!", ex);
                }
                messageDisp.replyMessage(new DistAlgReplyMessage(req));
            } catch (IOException ex) {
                log.log(Level.SEVERE, null, ex);
            }
        }
    }

    private class ReceiverImpl implements Receiver {
        @Override
        public boolean acceptMessage(Message msg, boolean allowSuperclass) {
            if (msg instanceof MessageActivate) {
                executor.submit(new ActivationProcessor((MessageActivate)msg));
                return true;
                
            } else if (msg instanceof DistAlgRequestMessage) {
                executor.submit(new OperationProcessor((DistAlgRequestMessage)msg));
                return true;
            }
            // Next receiver will process msg
            return false;
        }
    }

}
