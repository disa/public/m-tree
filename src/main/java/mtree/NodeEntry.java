package mtree;

import mtree.utils.ParentFilter;
import java.io.Serializable;
import messif.objects.LocalAbstractObject;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class NodeEntry implements Serializable {
    
    /** node in which the node entry is stored */
    private InternalNode n;
    
    /** stored object */
    private LocalAbstractObject o;
    
    /** subtree radius */
    private float r = 0f;
    
    /** subtree of this node */
    private Node subtree = null;
    
    /** array for filtering */
    private HREntry[] hr;
    
    static final long serialVersionUID = 1L;
    
    
    /****************** Constructors ******************/
    
    public NodeEntry(InternalNode n, LocalAbstractObject o, Node subtree, HREntry[] hr) {
        this.n = n;
        this.o = o;
        this.subtree = subtree;
        this.hr = hr;
    }
    
    
    /****************** Methods ******************/
    
    /**
     *  Returns the node in which this node entry is stored.
     *
     *  @return the node in which this node entry is stored
     */
    public InternalNode getNode() {
        return n;
    }
    
    /**
     *  Sets the node in which this node entry is stored.
     */
    public void setNode(InternalNode n) {
        this.n = n;
    }
    
    /**
     *  Returns the pivot connected with this node entry.
     *
     *  @return the pivot connected with this node entry
     */
    public LocalAbstractObject getObject() {
        return o;
    }
    
    /**
     *  Returns the distance to the parent pivot.
     *
     *  @return the distance to the parent pivot ({@link LocalAbstractObject#UNKNOWN_DISTANCE UNKNOWN_DISTANCE} if this node entry has no parent)
     */
    public float getParentDist() {
        return o.getDistanceFilter(ParentFilter.class).getParentDist();
    }
    
    /**
     *  Returns the radius of this node entry covering all descendants.
     *
     *  @return the radius of this node entry covering all descendants
     */
    public float getRadius() {
        return r;
    }
    
    /**
     *  Sets the radius of this node entry covering all descendants.
     */
    public void setRadius(float r) {
        this.r = r;
    }
    
    /**
     *  Returns the subtree of this node entry.
     *
     *  @return the subtree of this node entry
     */
    public Node getSubtree() {
        return subtree;
    }
    
    /**
     *  Sets the subtree of this node entry.
     */
    public void setSubtree(Node subtree) {
        this.subtree = subtree;
    }
    
    /**
     *  Returns the HR array of precomputed distances.
     *
     *  @return the HR array of precomputed distances
     */
    public HREntry[] getHR() {
        return hr;
    }
    
    /**
     *  Computes and sets the distance between this node entry and its parent
     *  pivot.
     *
     *  @return the distance between this node entry and its parent pivot
     */
    public float computeAndSetDistanceToParent() {
        NodeEntry pne = getNode().getParentNodeEntry();
        
        float d = LocalAbstractObject.UNKNOWN_DISTANCE;
        if (pne != null)
            d = o.getDistance(pne.getObject());
        
        o.getDistanceFilter(ParentFilter.class).setParentDist(d);
        return d;
    }
    
    /**
     *  Static class used for HR array of precomputed distances.
     */
    public static class HREntry implements Serializable {
        
        private float min;
        private float max;
        static final long serialVersionUID = 1L;
        
        
        /****************** Constructors ******************/
        
        /** Create an empty cut-region (boundaries that cannot intersect any other region) */
        public HREntry() {
            this(Float.MAX_VALUE, 0f);
        }
        
        public HREntry(float min, float max) {
            this.min = min;
            this.max = max;
        }
        
        
        /****************** Methods ******************/
        
        public float getMin() {
            return min;
        }
        
        public void setMin(float min) {
            this.min = min;
        }
        
        public float getMax() {
            return max;
        }
        
        public void setMax(float max) {
            this.max = max;
        }

        @Override
        public String toString() {
            return "HREntry{" + "min=" + min + ", max=" + max + '}';
        }
    }
    
}
