package mtree;

import java.io.BufferedReader;
import messif.objects.keys.BucketIdObjectKey;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.algorithms.Algorithm;
import messif.algorithms.Algorithm.AlgorithmConstructor;
import messif.algorithms.AlgorithmMethodException;
import messif.buckets.BucketDispatcher;
import messif.buckets.BucketErrorCode;
import messif.buckets.BucketStorageException;
import messif.buckets.CapacityFullException;
import messif.buckets.LocalBucket;
import messif.buckets.impl.MemoryStorageBucket;
import messif.buckets.impl.AlgorithmStorageBucket;
import messif.buckets.split.SplitPolicy;
import messif.buckets.split.SplittableAlgorithm;
import messif.buckets.split.SplittableAlgorithm.SplittableAlgorithmResult;
import messif.objects.AbstractObject;
import messif.objects.BallRegion;
import messif.objects.util.AbstractObjectList;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFixedArrayFilter;
import messif.objects.keys.BucketInfoObjectKey;
import messif.objects.util.RankedAbstractObject;
import messif.objects.util.StreamGenericAbstractObjectIterator;
import messif.operations.AnswerType;
import messif.operations.Approximate;
import messif.operations.query.ApproxKNNQueryOperation;
import messif.operations.data.DeleteOperation;
import messif.operations.query.GetAllObjectsQueryOperation;
import messif.operations.query.GetObjectsByLocatorsOperation;
import messif.operations.query.IncrementalNNQueryOperation;
import messif.operations.data.InsertOperation;
import messif.operations.OperationErrorCode;
import messif.operations.data.BulkInsertOperation;
import messif.operations.data.PartitionedBulkInsertOperation;
import messif.operations.query.DumpObjectsToFilesOperation;
import messif.operations.query.DumpStructureOperation;
import messif.operations.query.GetObjectByLocatorOperation;
import messif.operations.query.RangeQueryOperation;
import messif.operations.query.KNNQueryOperation;
import messif.operations.query.PrintAllObjectsOperation;
import messif.statistics.OperationStatistics;
import messif.statistics.StatisticCounter;
import messif.statistics.StatisticObject;
import messif.statistics.StatisticRefCounter;
import messif.statistics.Statistics;
import messif.utility.reflection.ConstructorInstantiator;
import messif.utility.reflection.MethodInstantiator;
import messif.utility.reflection.NoSuchInstantiatorException;
import mtree.utils.Insert;
import mtree.utils.ParentFilter;
import mtree.utils.Search;
import mtree.utils.Split;

/**
 * M-tree's fork
 * Changes:
 *    = node capacity is in number of objects now!!!
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class MTree extends Algorithm implements Serializable, SplittableAlgorithm, Cloneable {
    static final long serialVersionUID = 4L;

    /** maximum internal node capacity in objects */
    protected final long intNodeCap;
    /** maximum leaf capacity in objects */
    protected final long leafCap;
    // fixed array of pivots
    protected final LocalAbstractObject[] pivots;
    protected final int npd, nhr;
    // root of the tree
    protected Node root;
    // dispatcher of all leaf buckets
    protected BucketDispatcher bucketDispatcher = null;
    // indicates whether a new object is inserted by multiway algorithm
    protected boolean multiwayInsertion = false;
    // radius for multiway insertion of a new object
    protected float insRadius = 0f;
    // maximum number of objects which are selected by split node method to build a minimum spanning tree
    protected int maxSpanningTree = 0;
    // indicates the using of M-tree algorithm in the buckets
    protected final boolean isAlgorithmInBucket;
    // maximum internal node capacity of the tree stored in every bucket (when using of an algorithm in the buckets)
    protected final long intNodeCapInBucketAlgorithm;
    // maximum leaf capacity of the tree stored in every bucket (when using of an algorithm in the buckets)
    protected final long leafCapInBucketAlgorithm;
    // map keeps IDs of operations for the incremental nearest neighbor search algorithm
    protected Map<Object, SearchQueue<Serializable>> incManager = new HashMap<>();
    // in two-phased testing indicates whether the second phase is turned on (when using usefulness and testMode)
    private static transient boolean testMode = false;
    
    private static final String STAT_NAME_LEAVES_VISITED                        = "Node.Leaf.Visited";
    private static final StatisticCounter statVisitedLeaves = StatisticCounter.getStatistics(STAT_NAME_LEAVES_VISITED);
    private static final String STAT_NAME_LEAVES_VISITED_ORDER                  = "Node.Leaf.Visited.Order";
    private static final StatisticRefCounter statLeafNodeVisitedOrder = StatisticRefCounter.getStatistics(STAT_NAME_LEAVES_VISITED_ORDER);
    private static final String STAT_NAME_LEAVES_VISITED_DISTANCE_PIVOT         = "Node.Leaf.Visited.DistanceQueryPivot";
    private static final String STAT_NAME_LEAVES_VISITED_DISTANCE_LB            = "Node.Leaf.Visited.DistanceLowerBound";
    private static final String STAT_NAME_LEAVES_VISITED_DISTANCE_UB            = "Node.Leaf.Visited.DistanceUpperBound";

    private static final String STAT_NAME_ALL_LEAVES_DISTANCE_PIVOT         = "Tree.Leaf.DistanceQueryPivot";
    private static final String STAT_NAME_ALL_LEAVES_DISTANCE_LB            = "Tree.Leaf.DistanceLowerBound";
    private static final String STAT_NAME_ALL_LEAVES_DISTANCE_UB            = "Tree.Leaf.DistanceUpperBound";

    private static final String STAT_NAME_ALL_NODES_DISTANCE_PIVOT         = "Tree.Node.DistanceQueryPivot";
    
    private static final String STAT_NAME_QUERY_TO_ALL_OBJECTS_PER_BUCKET         = "Distance.ObjectsToQuery.";
    private static final String STAT_NAME_QUERY_TO_PIVOTS_PER_BUCKET              = "Distance.PivotsToQuery.";

    /** Maximum seach queue length */
    private static final String STAT_NAME_SEARCH_QUEUE_LENGTH                   = "SearchQueue.Length";
    /** How many times the queues head was replaced by a more specific element that the right removed one. */
    private static final String STAT_NAME_SEARCH_QUEUE_DEPTH_FIRST              = "SearchQueue.DepthFirstCount";
    
    private static final String STAT_NAME_OPTIM_INDEX                           = "OptimIndex";
    private static final String STAT_NAME_OPTIM_INDEX_ON_RESULT                 = "OptimIndex.OnResult";
    private static final String STAT_NAME_OBJS_SUM                              = "OptimIndex.ObjectsSum";
    private static final StatisticCounter statOptimIndexObjSum = StatisticCounter.getStatistics(STAT_NAME_OBJS_SUM);
    private static final String STAT_NAME_OBJS_PER_BUCKET                       = "OptimIndex.ObjectsPerBucket";
    private static final StatisticRefCounter statOptimIndexObjCntPerBucket = StatisticRefCounter.getStatistics(STAT_NAME_OBJS_PER_BUCKET);
    private static final String STAT_NAME_NEW_OBJS_PER_BUCKET                   = "OptimIndex.NewObjectsPerBucket";
    private static final StatisticRefCounter statOptimIndexNewObjPerBucket = StatisticRefCounter.getStatistics(STAT_NAME_NEW_OBJS_PER_BUCKET);

    private static final String STAT_NAME_NECESSARY_BUCKETS                     = "OptimIndex.NecessaryBuckets";
    private static final String STAT_NAME_QUERY_FREQUENCY                       = "OptimIndex.QueryFrequency";
    
    private static final String STAT_NAME_ANSWER_OBJS_PER_BUCKET                = "OptimIndex.Answer.ObjectsPerBucketOrderedByDistance";
    private static final String STAT_NAME_ANSWER_BUCKETS_ORDERED_BY_VISIT       = "OptimIndex.Answer.VisitedBuckets.OrderedByVisit";
    private static final String STAT_NAME_ANSWER_BUCKETS_ORDERED_BY_VISIT_MAX   = "OptimIndex.Answer.VisitedBuckets.OrderedByVisit.Max";
    private static final String STAT_NAME_ANSWER_BUCKETS_DISTANCE_PIVOT         = "OptimIndex.Answer.VisitedBuckets.DistanceQueryPivot";
    private static final String STAT_NAME_ANSWER_BUCKETS_DISTANCE_LB            = "OptimIndex.Answer.VisitedBuckets.DistanceLowerBound";
    private static final String STAT_NAME_ANSWER_BUCKETS_DISTANCE_UB            = "OptimIndex.Answer.VisitedBuckets.DistanceUpperBound";
    private static final String STAT_NAME_ANSWER_BUCKETS_CONTAINS_QUERY         = "OptimIndex.Answer.VisitedBuckets.ContainsQuery";
    
    private static final String STAT_NAME_ANSWER_DISTANCE_TO_KNN                = "OptimIndex.Answer.VisitedBuckets.DistanceToKNN";
    private static StatisticRefCounter statDistanceToKNNCandidate = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_DISTANCE_TO_KNN);
    
    private static final String STAT_NAME_ANSWER_CLOSEST_BUCKET                 = "OptimIndex.Answer.ClosestBucket";
    private static final String STAT_NAME_ANSWER_CLOSEST_BUCKET_ORDER_VISITED   = "OptimIndex.Answer.ClosestBucket.OrderVisited";
    private static final String STAT_NAME_ANSWER_CLOSEST_BUCKET_DIST_TO_KNN     = "OptimIndex.Answer.ClosestBucket.DistanceToKNN";
    private static final String STAT_NAME_ANSWER_FIRST_BUCKET                   = "OptimIndex.Answer.FirstVisitedBucket";
    private static final String STAT_NAME_ANSWER_FIRST_BUCKET_DIST_TO_KNN       = "OptimIndex.Answer.FirstVisitedBucket.DistanceToKNN";
    private static final String STAT_NAME_ANSWER_FIRST_BUCKET_EXP_RATE          = "OptimIndex.Answer.FirstVisitedBucket.ExpRate";
    private static final String STAT_NAME_ANSWER_SECOND_BUCKET                   = "OptimIndex.Answer.SecondVisitedBucket";
    private static final String STAT_NAME_ANSWER_SECOND_BUCKET_DIST_TO_KNN       = "OptimIndex.Answer.SecondVisitedBucket.DistanceToKNN";
    private static final String STAT_NAME_ANSWER_SECOND_BUCKET_EXP_RATE          = "OptimIndex.Answer.SecondVisitedBucket.ExpRate";

    
    /**
     * **************** Constructors of M-tree *****************
     */
    /**
     * Creates original M-tree algorithm with {@link MemoryStorageBucket}.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     */
    @AlgorithmConstructor(description = "M-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects"})
    public MTree(long intNodeCap, long leafCap) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, 0);
    }

    /**
     * Creates original M-tree algorithm with a specific bucket storage.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     */
    @AlgorithmConstructor(description = "M-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "storage class for buckets", "storage class parameters"})
    public MTree(long intNodeCap, long leafCap, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, new LocalAbstractObject[0], 0, 0, 0, 20, false, 0, 0, bucketClass, bucketClassParams);
    }

    /**
     * Creates M-tree algorithm with multi-way insertion extension.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     * @param insRadius multi-way insertion radius (use 0 to disable)
     */
    @AlgorithmConstructor(description = "M-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "multi-way insertion radius"})
    public MTree(long intNodeCap, long leafCap, float insRadius) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, insRadius, 20, false, 0, 0);
    }

    /**
     * Creates M-tree algorithm with multi-way insertion extension.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     * @param insRadius multi-way insertion radius (use 0 to disable)
     * @param maxSpanningTree maximum number of objects creating spanning tree
     * @param isAlgorithmInBucket using of M-tree algorithm in the buckets
     * @param intNodeCapInBucketAlgorithm maximum internal node capacity of the
     * tree stored in every bucket
     * @param leafCapInBucketAlgorithm maximum leaf capacity of the tree stored
     * in every bucket
     */
    @AlgorithmConstructor(description = "M-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "multi-way insertion radius", "maximum number of objects creating spanning tree", "using of M-tree algorithm in the buckets", "maximum internal node capacity of the tree stored in every bucket", "maximum leaf capacity of the tree stored in every bucket"})
    public MTree(long intNodeCap, long leafCap, float insRadius, int maxSpanningTree, boolean isAlgorithmInBucket, long intNodeCapInBucketAlgorithm, long leafCapInBucketAlgorithm) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, new LocalAbstractObject[0], 0, 0, insRadius, maxSpanningTree, isAlgorithmInBucket, intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm);
    }

    /**
     * **************** Constructors of PM-tree *****************
     */
    /**
     * Creates Pivoting M-tree algorithm.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     * @param pivotCount number of pivots to read
     * @param pivotIterator object iterator for pivots
     * @param npd number of pivots used in leaves
     * @param nhr number of pivots used in internal nodes
     */
    @AlgorithmConstructor(description = "PM-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "number of pivots to read", "object iterator for pivots", "npd", "nhr"})
    public MTree(long intNodeCap, long leafCap, int pivotCount, Iterator<LocalAbstractObject> pivotIterator, int npd, int nhr) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, pivotCount, pivotIterator, npd, nhr, 0, 20, false, 0, 0);
    }

    /**
     * Creates Pivoting M-tree algorithm with multi-way insertion.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     * @param pivotCount number of pivots to read
     * @param pivotIterator object iterator for pivots
     * @param npd number of pivots used in leaves
     * @param nhr number of pivots used in internal nodes
     * @param insRadius multi-way insertion radius (use 0 to disable)
     * @param maxSpanningTree maximum number of objects creating spanning tree
     * @param isAlgorithmInBucket using of M-tree algorithm in the buckets
     * @param intNodeCapInBucketAlgorithm maximum internal node capacity of the
     * tree stored in every bucket
     * @param leafCapInBucketAlgorithm maximum leaf capacity of the tree stored
     * in every bucket
     */
    @AlgorithmConstructor(description = "PM-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "number of pivots to read", "object iterator for pivots", "npd", "nhr", "multi-way insertion radius", "maximum number of objects creating spanning tree", "using of M-tree algorithm in the buckets", "maximum internal node capacity of the tree stored in every bucket", "maximum leaf capacity of the tree stored in every bucket"})
    public MTree(long intNodeCap, long leafCap, int pivotCount, Iterator<LocalAbstractObject> pivotIterator, int npd, int nhr, float insRadius, int maxSpanningTree, boolean isAlgorithmInBucket, long intNodeCapInBucketAlgorithm, long leafCapInBucketAlgorithm) throws InstantiationException, AlgorithmMethodException {
        this(intNodeCap, leafCap, new AbstractObjectList<LocalAbstractObject>(pivotIterator, pivotCount).toArray(new LocalAbstractObject[pivotCount]), npd, nhr, insRadius, maxSpanningTree, isAlgorithmInBucket, intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm);
    }

    /**
     * Creates Pivoting M-tree algorithm with multi-way insertion.
     */
    @AlgorithmConstructor(description = "PM-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "pivots array", "npd", "nhr", "multi-way insertion radius", "maximum number of objects creating spanning tree", "using of M-tree algorithm in the buckets", "maximum internal node capacity of the tree stored in every bucket", "maximum leaf capacity of the tree stored in every bucket"})
    public MTree(long intNodeCap, long leafCap, LocalAbstractObject[] pivots, int npd, int nhr, float insRadius, int maxSpanningTree, boolean isAlgorithmInBucket, long intNodeCapInBucketAlgorithm, long leafCapInBucketAlgorithm) throws AlgorithmMethodException, InstantiationException {
        this(intNodeCap, leafCap, pivots, npd, nhr, insRadius, maxSpanningTree, isAlgorithmInBucket, intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm, MemoryStorageBucket.class, null);
    }

    /**
     * Creates Pivoting M-tree algorithm with multi-way insertion.
     */
    @AlgorithmConstructor(description = "PM-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "number of pivots to read", "object iterator for pivots", "npd", "nhr", "multi-way insertion radius", "maximum number of objects creating spanning tree", "using of M-tree algorithm in the buckets", "maximum internal node capacity of the tree stored in every bucket", "maximum leaf capacity of the tree stored in every bucket", "storage class for buckets", "storage class parameters"})
    public MTree(long intNodeCap, long leafCap, int pivotCount, AbstractObjectIterator<LocalAbstractObject> pivotIterator, int npd, int nhr, float insRadius, int maxSpanningTree, boolean isAlgorithmInBucket, long intNodeCapInBucketAlgorithm, long leafCapInBucketAlgorithm, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) throws InstantiationException, AlgorithmMethodException {
        this(intNodeCap, leafCap, new AbstractObjectList<LocalAbstractObject>(pivotIterator, pivotCount).toArray(new LocalAbstractObject[pivotCount]), npd, nhr, insRadius, maxSpanningTree, isAlgorithmInBucket, intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm, bucketClass, bucketClassParams);
    }

    /**
     * Creates Pivoting M-tree algorithm that can be encapsulated in a bucket.
     *
     * @param intNodeCap internal node capacity
     * @param leafCap leaf capacity
     * @param pivots array of pivots
     * @param npd number of pivots used in leaves
     * @param nhr number of pivots used in internal nodes
     * @param insRadius multi-way insertion radius (use 0 to disable)
     * @param maxSpanningTree maximum number of objects creating spanning tree
     * @param isAlgorithmInBucket using of M-tree algorithm in the buckets
     * @param intNodeCapInBucketAlgorithm maximum internal node capacity of the
     * tree stored in every bucket
     * @param leafCapInBucketAlgorithm maximum leaf capacity of the tree stored
     * in every bucket
     */
    @AlgorithmConstructor(description = "PM-tree", arguments = {"internal node capacity in objects", "leaf capacity in objects", "pivots array", "npd", "nhr", "multi-way insertion radius", "maximum number of objects creating spanning tree", "using of M-tree algorithm in the buckets", "maximum internal node capacity of the tree stored in every bucket", "maximum leaf capacity of the tree stored in every bucket", "storage class for buckets", "storage class parameters"})
    public MTree(long intNodeCap, long leafCap, LocalAbstractObject[] pivots, int npd, int nhr, float insRadius, int maxSpanningTree, boolean isAlgorithmInBucket, long intNodeCapInBucketAlgorithm, long leafCapInBucketAlgorithm, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) throws AlgorithmMethodException, InstantiationException {
        super("M-tree");
        this.intNodeCap = intNodeCap;
        this.leafCap = leafCap;
        if (pivots == null) {
            this.pivots = new LocalAbstractObject[0];
        } else {
            this.pivots = pivots;
        }
        this.npd = npd;
        this.nhr = nhr;
        if (Math.max(npd, nhr) > this.pivots.length) {
            throw new AlgorithmMethodException("The number of pivots is less than npd or nhr!");
        }
        this.insRadius = insRadius;
        this.maxSpanningTree = maxSpanningTree;
        this.isAlgorithmInBucket = isAlgorithmInBucket;
        this.intNodeCapInBucketAlgorithm = intNodeCapInBucketAlgorithm;
        this.leafCapInBucketAlgorithm = leafCapInBucketAlgorithm;
        this.createRootAndBucketDispatcher(isAlgorithmInBucket, bucketClass, bucketClassParams);
    }

    /**
     * **************** Methods *****************
     */
    
    /** Sets the number of items used in splitting a node. 
     * @param maxSpanningTree number of items to take to make a node split. Pass zero to use all at any time.
     */
    public void setMaxSpanningTree(int maxSpanningTree) {
        this.maxSpanningTree = maxSpanningTree;
    }

    /**
     * Returns the root of the tree.
     *
     * @return the root of the tree
     */
    public Node getRoot() {
        return root;
    }

    /**
     * Returns the copy of fixed array of pivots.
     *
     * @return the copy of fixed array of pivots
     */
    public LocalAbstractObject[] getPivots() {
        return pivots.clone();
    }

    /**
     * Returns the bucket dispatcher of the tree.
     *
     * @return the bucket dispatcher of the tree
     */
    public BucketDispatcher getBucketDispatcher() {
        return bucketDispatcher;
    }

    /**
     * Sets the type of inserting algorithm (singleway or multiway insertion).
     *
     * @param multiwayInsertion indicates whether new objects will be inserted
     * by multiway algorithm
     */
    public void setMultiwayInsertion(boolean multiwayInsertion) {
        this.multiwayInsertion = multiwayInsertion;
    }

    /**
     * Returns a list of all leaves of the tree.
     *
     * @return a list of all leaves of the tree
     */
    public List<LeafNode> getAllLeaves() {
        List<LeafNode> leaves = new ArrayList<>();
        Search.getAllLeaves(root, leaves);
        return leaves;
    }

    /**
     * Returns a list of all buckets of the tree.
     *
     * @return a list of all buckets of the tree
     */
    public Collection<LocalBucket> getAllBuckets() {
        Collection<LocalBucket> buckets;
        if (!isAlgorithmInBucket) {
            buckets = bucketDispatcher.getAllBuckets();
        } else {
            buckets = new ArrayList<>();
            Search.getAllBuckets(root, buckets);
        }
        return buckets;
    }
    
    private LeafNode findByBucketId(int bucketId, Node current) {
        if (current.isLeaf())
            return (bucketId == ((LeafNode)current).getBucket().getBucketID()) ? (LeafNode)current : null;
    
        // Depth-first search
        for (NodeEntry ne : ((InternalNode) current).getNodeEntryList()) {
            LeafNode found = findByBucketId(bucketId, ne.getSubtree());
            if (found != null) {
                return found; 
            }
        }
        return null;
    }

    private void getNodesByLevel(Map<Integer, List<Node>> nodesByLevel) {
        Search.getNodesByLevel(root, nodesByLevel);
    }

    /**
     * Creates new empty M-tree with same a structure (parameters) as current
     * this, but without stored objects.
     *
     * @return new empty M-tree with same a structure (parameters) as current
     * this, but without stored objects
     */
    public MTree cloneWithoutObjects() {
        try {
            MTree mtree = new MTree(intNodeCap, leafCap, pivots, npd, nhr, insRadius, maxSpanningTree, isAlgorithmInBucket, intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm, bucketDispatcher.getDefaultBucketClass(), bucketDispatcher.getDefaultBucketClassParams());
            mtree.setMultiwayInsertion(multiwayInsertion);
            return mtree;
        } catch (AlgorithmMethodException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the class of objects indexed by this algorithm. This methods
     * returns the class of the first object stored in the first bucket.
     *
     * @return the class of objects indexed by this algorithm
     */
    @Override
    public Class<? extends LocalAbstractObject> getObjectClass() {
        Iterator<LocalBucket> bucketIterator = bucketDispatcher.getAllBuckets().iterator();
        if (!bucketIterator.hasNext()) {
            return super.getObjectClass();
        }
        LocalBucket bucket = bucketIterator.next();
        Iterator<LocalAbstractObject> bucketObjects = bucket.getAllObjects();
        if (!bucketObjects.hasNext()) {
            return super.getObjectClass();
        }
        return bucketObjects.next().getClass();
    }

    /**
     * **************** Internal methods *****************
     */
    /**
     * Creates a root of the tree. Creates and sets a bucket dispatcher of the
     * tree in the case that none algorithm in the buckets of the tree is used.
     */
    private void createRootAndBucketDispatcher(boolean isAlgorithmInBucket, Class<? extends LocalBucket> bucketClass, Map<String, Object> bucketClassParams) {
        try {
            if (isAlgorithmInBucket) {
                root = new LeafNode(null, this.createAlgorithmStorageBucket());
            } else {
                bucketDispatcher = new BucketDispatcher(Integer.MAX_VALUE, Long.MAX_VALUE, leafCap, 0, false, bucketClass, bucketClassParams);
                root = new LeafNode(null, bucketDispatcher.createBucket());
            }
        } catch (BucketStorageException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Creates the M-tree storage bucket.
     *
     * @return the M-tree storage bucket
     */
    private AlgorithmStorageBucket createAlgorithmStorageBucket() {
        try {
            MTree alg = new MTree(intNodeCapInBucketAlgorithm, leafCapInBucketAlgorithm, pivots, npd, nhr, insRadius, maxSpanningTree, false, 0, 0, bucketDispatcher.getDefaultBucketClass(), bucketDispatcher.getDefaultBucketClassParams());
            alg.setMultiwayInsertion(multiwayInsertion);
            return new AlgorithmStorageBucket(alg, Long.MAX_VALUE, leafCap, 0, false);
        } catch (AlgorithmMethodException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the new root of the tree. The local buckets stored in the bucket
     * dispatcher of the original tree are replaced by buckets of the new tree.
     * The parent distance of all root objects is set to
     * LocalAbstractObject#UNKNOWN_DISTANCE.
     */
    private void setRoot(Node n, BucketDispatcher originalBucketDispatcher) throws BucketStorageException {

        if (!isAlgorithmInBucket) {
            bucketDispatcher = new BucketDispatcher(Integer.MAX_VALUE, Long.MAX_VALUE, leafCap, 0, true, originalBucketDispatcher.getDefaultBucketClass(), originalBucketDispatcher.getDefaultBucketClassParams());

            List<LocalBucket> buckets = new ArrayList<>();
            Search.getAllBuckets(n, buckets);

            for (LocalBucket bucket : buckets) {
                originalBucketDispatcher.moveBucket(bucket.getBucketID(), bucketDispatcher);
            }
            //bucketDispatcher.addExistingBucket(bucket);

        }

        // Setting new root of the tree.
        root = n;
        root.createRootNode();

        // Setting the parent distances of root objects to UNKNOWN_DISTANCE.
        AbstractObjectIterator<LocalAbstractObject> it = n.getObjects();
        while (it.hasNext()) {
            it.next().getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
        }
    }

    private void splitNode(Node n/*, PrecomputedDistancesFixedArrayFilter hrDists*/) throws CloneNotSupportedException {
        splitNode_SlimDown(n);//, hrDists);
//        splitNode_mM_RAD(n);
    }
    
    /**
     * Splits the given node into two nodes (if it is necessary splitting
     * algorithm is applied recursively) according to Slim-down-algorithm
     * policy.
     * 
     * @param hrDists the precomputed distance of newly added object to the node that caused a bucket to overflow
     * 
     * CAVEAT: Must not be called directly! It is called from {@link #splitNode(mtree.Node, messif.objects.PrecomputedDistancesFixedArrayFilter) } only!
     */
    private void splitNode_SlimDown(Node n) throws CloneNotSupportedException {
                                    // , PrecomputedDistancesFixedArrayFilter hrDists

        // List of all objects which will be inserted after split node into first node.
        List<LocalAbstractObject> objectList1;

        // List of all objects which will be inserted after split node into second node.
        List<LocalAbstractObject> objectList2;

        // Map is used for quicker split node in the case of splitting an internal node.
        Map<LocalAbstractObject, NodeEntry> nodeEntryMap = null;
        if (!n.isLeaf()) {
            nodeEntryMap = new HashMap<>();
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                nodeEntryMap.put(ne.getObject(), ne);
            }
        }

        // Objects stored in the node n will be split into two nodes.
        int objCnt = n.getObjectCount();
        //LocalAbstractObject[] objects = new LocalAbstractObject[poc];     // DiskBlockBucket -- there is a trouble with reporting different number of objects than the iterator getObjects() does!
        List<LocalAbstractObject> objects = new ArrayList<>(objCnt);
        AbstractObjectIterator<LocalAbstractObject> objectIt = n.getObjects();
        while (objectIt.hasNext())
            objects.add(objectIt.next());

        // Indicates whether the spanning tree is built from all objects in the node.
        boolean isSpanningTreeComplete = !(maxSpanningTree >= 2 && maxSpanningTree < objCnt);
        if (!isSpanningTreeComplete) {
            objCnt = maxSpanningTree;  // Use the first "maxSpanningTree" items!!!
        }

        TreeSet<Split.Edge> edgeSet = createEdgeSet(objCnt, objects);

        // Sorted set of edges which forms the minimum spanning tree.
        TreeSet<Split.Edge> spanSet = new TreeSet<>(Split.Edge.comparatorByDecreasingDistance());

        Map<LocalAbstractObject, List<Split.Edge>> objectMap = createObjectMap(objCnt, objects);

        // Choosing edges creating the minimum spanning tree. These edges are
        // deleted from edgeSet and added into objectMap and spanSet.
        Iterator<Split.Edge> edgeIt = edgeSet.iterator();
        while (edgeIt.hasNext() && spanSet.size() < objCnt - 1) {
            Split.Edge e = edgeIt.next();
            LocalAbstractObject o1 = e.getObject1(), o2 = e.getObject2();
            if (!Split.formsOneComponent(objectMap, o1, o2)) {
                objectMap.get(o1).add(e);
                objectMap.get(o2).add(e);
                spanSet.add(e);
                edgeIt.remove();        // And remove it from the list of all edges
            }
        }
//        for (Split.Edge e : spanSet) {    // Done above
//            edgeSet.remove(e);
//        }

        // Locating the edge which will split node by the minimum spanning tree into 2 components.
        assert spanSet.size() == objCnt - 1;
        //Split.Edge le = spanSet.last();
        Split.Edge le = Split.findBestSpanEdge(objectMap, spanSet);
        objectMap.get(le.getObject1()).remove(le);
        objectMap.get(le.getObject2()).remove(le);
        spanSet.remove(le);
        // Splitting all objects into 2 sets representing objects of new nodes.
        objectList1 = Split.findAllObjectsInComponent(objectMap, le.getObject1());
        objectList2 = Split.findAllObjectsInComponent(objectMap, le.getObject2());
        
        boolean unbalancedSplit = false;
        if (objectList1 == null || objectList2 == null || Math.abs(objectList1.size() - objectList2.size()) > ((objectList1.size() + objectList2.size()) / 5)) {
            System.err.println("Splitting a node " + n + " produces highly uneven partitions: " + ((objectList1 == null) ? -1 : objectList1.size())
                    + ":" + ((objectList2 == null) ? -1 : objectList2.size()));
            unbalancedSplit = true;
        }

        // Insertion of all edges into objectMap that do not connect the two
        // disjunctive sets of objects (objectList1 and objectList2).
        for (Split.Edge e : edgeSet) {
            if ((objectList1.contains(e.getObject1()) && objectList1.contains(e.getObject2()))
                    || (objectList2.contains(e.getObject1()) && objectList2.contains(e.getObject2()))) {
                objectMap.get(e.getObject1()).add(e);
                objectMap.get(e.getObject2()).add(e);
            }
        }

        // Looking for pivots p1 and p2 for sets of objects objectList1 and
        // objectList2 and setting their covering radii. Filling of distances
        // between pivots and other objects in an appropriate node.
        LocalAbstractObject p1 = Split.findPivot(objectMap, objectList1);
        p1.getDistanceFilter(ParentFilter.class).setParentDist(0f);
//        float r1 = Split.fillDistances(objectMap, p1);
        float r1 = Split.setParentDistances(p1, objectList1);
        LocalAbstractObject p2 = Split.findPivot(objectMap, objectList2);
        p2.getDistanceFilter(ParentFilter.class).setParentDist(0f);
//        float r2 = Split.fillDistances(objectMap, p2);
        float r2 = Split.setParentDistances(p2, objectList2);

        // If the spanning tree isn't built from all objects in the node n then
        // the rest of objects is divided into two nodes n1 and n2.
        if (!isSpanningTreeComplete) {
            for (int i = objCnt; i < objects.size(); i++) {
                LocalAbstractObject o = objects.get(i);
                float d1 = o.getDistance(p1);
                float d2 = o.getDistance(p2);
                if (d1 <= d2) {
                    o.getDistanceFilter(ParentFilter.class).setParentDist(d1);
                    objectList1.add(o);
                    r1 = Math.max(d1, r1);
                } else {
                    o.getDistanceFilter(ParentFilter.class).setParentDist(d2);
                    objectList2.add(o);
                    r2 = Math.max(d2, r2);
                }
            }
            if (unbalancedSplit) 
                System.err.println("Splitting a node " + n + " produces highly uneven partitions - complete split: " + objectList1.size() + ":" + objectList2.size());
        }

        // Internal node entry pointing to the subtree n1 and n2, respectively.
        NodeEntry ne1 = null;
        NodeEntry ne2 = null;

        // If the node n is the root then a new root with descendants n1 and n2
        // is created. Else nodes n1 and n2 are added into the parent node of
        // the node n.
        p1 = p1.clone();
        p2 = p2.clone();
        InternalNode pn = n.getParentNode();
        boolean isFree = true;
        if (pn == null) {
            root = new InternalNode(n.getLevel() + 1, null, intNodeCap);
            p1.getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
            ne1 = ((InternalNode) root).createNodeEntry(p1, null, nhr);
            p2.getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
            ne2 = ((InternalNode) root).createNodeEntry(p2, null, nhr);
        } else {
            NodeEntry pne = n.getParentNodeEntry();
            pn.removeNodeEntry(pne);

            ne1 = pn.createNodeEntry(p1, null, nhr);
            ne1.computeAndSetDistanceToParent();
            isFree = pn.isFreeCapacity(p2);
            ne2 = pn.createNodeEntry(p2, null, nhr);
            ne2.computeAndSetDistanceToParent();
        }

        // Original node n splits up two new nodes n1 and n2.
        Node n1 = null;
        Node n2 = null;
        if (n.isLeaf()) {
            try {
                if (isAlgorithmInBucket) {
                    n1 = new LeafNode(ne1, this.createAlgorithmStorageBucket());
                    n2 = new LeafNode(ne2, this.createAlgorithmStorageBucket());
                } else {
                    LocalBucket origBucket = ((LeafNode) n).getBucket();
//                    origBucket.deleteAllObjects();
//                    n1 = new LeafNode(ne1, origBucket);
                    bucketDispatcher.removeBucket(origBucket.getBucketID(), true);
                    n1 = new LeafNode(ne1, bucketDispatcher.createBucket());
                    n2 = new LeafNode(ne2, bucketDispatcher.createBucket());
                }
                ne1.setSubtree(n1);
                ne2.setSubtree(n2);

                ((LeafNode) n1).getBucket().addObjects(objectList1);
                ((LeafNode) n2).getBucket().addObjects(objectList2);
            } catch (BucketStorageException ex) {
                ex.printStackTrace();
            }

            ((LeafNode) n1).updateDistances(r1);
            ((LeafNode) n2).updateDistances(r2);
        } else {
            n1 = new InternalNode(n.getLevel(), ne1, intNodeCap);
            n2 = new InternalNode(n.getLevel(), ne2, intNodeCap);
            ne1.setSubtree(n1);
            ne2.setSubtree(n2);

            for (LocalAbstractObject o : objectList1) {
                ((InternalNode) n1).createNodeEntry(nodeEntryMap.get(o));
            }
            for (LocalAbstractObject o : objectList2) {
                ((InternalNode) n2).createNodeEntry(nodeEntryMap.get(o));
            }

            n1.updateDistances();
            n2.updateDistances();
        }

        // If the capacity of the parent node of the node n is exceeded,
        // then the parent node has to be split.
        if (!isFree) {
            splitNode_SlimDown(pn);
        }

        // Splitting a node with the exceeded capacity. (handles completely disbalanced node split???)
        if (n1.isCapacityExceeded()) {
            splitNode_SlimDown(n1);
        }
        if (n2.isCapacityExceeded()) {
            splitNode_SlimDown(n2);
        }
    }

    private Map<LocalAbstractObject, List<Split.Edge>> createObjectMap(int objCnt, List<LocalAbstractObject> objects) {
        // Map of objects with an appropriate list of edges to other objects.
        Map<LocalAbstractObject, List<Split.Edge>> objectMap = new HashMap<>();
        for (int i = 0; i < objCnt; i++) {
            objectMap.put(objects.get(i), new ArrayList<Split.Edge>());
        }
        return objectMap;
    }

    /**
     * Sorted set of all edges (by size from the shortest to the longest).
     * An edge is created between a pair of objects skipping symmetric pairs and self-object pairs.
     */
    private TreeSet<Split.Edge> createEdgeSet(int objCnt, List<LocalAbstractObject> objects) {
        TreeSet<Split.Edge> edgeSet = new TreeSet<>();
        for (int i = 0; i < objCnt - 1; i++) {
            for (int j = i + 1; j < objCnt; j++) {
                edgeSet.add(new Split.Edge(objects.get(i), objects.get(j)));
            }
        }
        return edgeSet;
    }

    /**
     * Splits the given node into two nodes (if it is necessary splitting
     * algorithm is applied recursively) according to splitNode_mM_RAD policy.

     * CAVEAT: Must not be called directly! It is called from {@link #splitNode(mtree.Node, messif.objects.PrecomputedDistancesFixedArrayFilter) } only!
     */
    private void splitNode_mM_RAD(Node n) throws CloneNotSupportedException {

        // List of all objects which will be inserted after split node into first node.
        List<LocalAbstractObject> objectList1 = new ArrayList<LocalAbstractObject>();

        // List of all objects which will be inserted after split node into second node.
        List<LocalAbstractObject> objectList2 = new ArrayList<LocalAbstractObject>();

        // Map is used for quicker split node in the case of splitting internal node.
        Map<LocalAbstractObject, NodeEntry> nodeEntryMap = null;
        if (!n.isLeaf()) {
            nodeEntryMap = new HashMap<LocalAbstractObject, NodeEntry>();
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                nodeEntryMap.put(ne.getObject(), ne);
            }
        }

        // Objects stored in the node n will be split into two nodes.
        int oc = n.getObjectCount();
        LocalAbstractObject[] objects = new LocalAbstractObject[oc];
        AbstractObjectIterator<LocalAbstractObject> objectIt = n.getObjects();
        int pos = 0;
        while (objectIt.hasNext()) {
            objects[pos] = objectIt.next();
            pos++;
        }

        // Indicates whether the spanning tree is built from all objects in the node.
        int spanSize = (maxSpanningTree >= 2 && maxSpanningTree < oc) ? maxSpanningTree : oc;

        // Computing distances between each pair of objects.
        float[][] dists = new float[spanSize][spanSize];
        for (int i = 0; i < spanSize - 1; i++) {
            for (int j = i + 1; j < spanSize; j++) {
                dists[i][j] = objects[i].getDistance(objects[j]);
                dists[j][i] = dists[i][j];
            }
        }
        for (int i = 0; i < spanSize; i++) {
            dists[i][i] = 0f;
        }

        // Choosing pivots.
        int p1index = -1;
        int p2index = -1;
        float difference = Float.MAX_VALUE;
        for (int i = 0; i < spanSize - 1; i++) {
            for (int j = i + 1; j < spanSize; j++) {
                float rp1 = 0f;
                float rp2 = 0f;
                for (int k = 0; k < spanSize; k++) {
                    if (dists[i][k] < dists[j][k]) {
                        rp1 = Math.max(rp1, dists[i][k]);
                    } else {
                        rp2 = Math.max(rp2, dists[j][k]);
                    }
                }
                float pdifference = Math.max(rp1, rp2);
                if (difference > pdifference) {
                    difference = pdifference;
                    p1index = i;
                    p2index = j;
                }
            }
        }

        // Redistributing objects to objectList1 and objectList2 according to
        // the distance to the nearer pivot.
        LocalAbstractObject p1 = objects[p1index];
        LocalAbstractObject p2 = objects[p2index];
        float r1 = 0f;
        float r2 = 0f;
        for (int i = 0; i < spanSize; i++) {
            if ((i == p1index || dists[p1index][i] < dists[p2index][i]) && (i != p2index)) {
                objectList1.add(objects[i]);
                objects[i].getDistanceFilter(ParentFilter.class).setParentDist(dists[p1index][i]);
                r1 = Math.max(r1, dists[p1index][i]);
            } else {
                objectList2.add(objects[i]);
                objects[i].getDistanceFilter(ParentFilter.class).setParentDist(dists[p2index][i]);
                r2 = Math.max(r2, dists[p2index][i]);
            }
        }

        for (int i = spanSize; i < oc; i++) {
            float d1 = objects[i].getDistance(objects[p1index]);
            float d2 = objects[i].getDistance(objects[p2index]);
            if (d1 < d2) {
                objectList1.add(objects[i]);
                objects[i].getDistanceFilter(ParentFilter.class).setParentDist(d1);
                r1 = Math.max(r1, d1);
            } else {
                objectList2.add(objects[i]);
                objects[i].getDistanceFilter(ParentFilter.class).setParentDist(d2);
                r2 = Math.max(r2, d2);
            }
        }

        // Internal node entry pointing to the subtree n1 and n2, respectively.
        NodeEntry ne1 = null;
        NodeEntry ne2 = null;

        // If the node n is the root then a new root with descendants n1 and n2
        // is created. Else nodes n1 and n2 are added into the parent node of
        // the node n.
        p1 = p1.clone();
        p2 = p2.clone();
        InternalNode pn = n.getParentNode();
        boolean isFree = true;
        if (pn == null) {
            //root = new InternalNode(n.getLevel() + 1, null, caps[n.getLevel() + 1]);
            root = new InternalNode(n.getLevel() + 1, null, intNodeCap);
            p1.getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
            ne1 = ((InternalNode) root).createNodeEntry(p1, null, nhr);
            p2.getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
            ne2 = ((InternalNode) root).createNodeEntry(p2, null, nhr);
        } else {
            NodeEntry pne = n.getParentNodeEntry();
            pn.removeNodeEntry(pne);

            ne1 = pn.createNodeEntry(p1, null, nhr);
            ne1.computeAndSetDistanceToParent();
            isFree = pn.isFreeCapacity(p2);
            ne2 = pn.createNodeEntry(p2, null, nhr);
            ne2.computeAndSetDistanceToParent();
        }

        // Original node n splits up two new nodes n1 and n2.
        Node n1 = null;
        Node n2 = null;
        if (n.isLeaf()) {
            try {
                if (isAlgorithmInBucket) {
                    n1 = new LeafNode(ne1, this.createAlgorithmStorageBucket());
                    n2 = new LeafNode(ne2, this.createAlgorithmStorageBucket());
                } else {
                    bucketDispatcher.removeBucket(((LeafNode) n).getBucket().getBucketID());
                    n1 = new LeafNode(ne1, bucketDispatcher.createBucket());
                    n2 = new LeafNode(ne2, bucketDispatcher.createBucket());
                }
                ne1.setSubtree(n1);
                ne2.setSubtree(n2);

                for (LocalAbstractObject o : objectList1) {
                    ((LeafNode) n1).getBucket().addObject(o);
                }
                for (LocalAbstractObject o : objectList2) {
                    ((LeafNode) n2).getBucket().addObject(o);
                }
            } catch (BucketStorageException ex) {
                ex.printStackTrace();
            }

            ((LeafNode) n1).updateDistances(r1);
            ((LeafNode) n2).updateDistances(r2);
        } else {
            n1 = new InternalNode(n.getLevel(), ne1, intNodeCap);
            n2 = new InternalNode(n.getLevel(), ne2, intNodeCap);
            ne1.setSubtree(n1);
            ne2.setSubtree(n2);

            for (LocalAbstractObject o : objectList1) {
                ((InternalNode) n1).createNodeEntry(nodeEntryMap.get(o));
            }
            for (LocalAbstractObject o : objectList2) {
                ((InternalNode) n2).createNodeEntry(nodeEntryMap.get(o));
            }

            n1.updateDistances();
            n2.updateDistances();
        }

        // If the capacity of the parent node of the node n is exceeded the
        // parent node has to be split.
        if (!isFree) {
            splitNode_mM_RAD(pn);
        }

        // Splitting a node with the exceeded capacity.
        if (n1.isCapacityExceeded()) {
            splitNode_mM_RAD(n1);
        }
        if (n2.isCapacityExceeded()) {
            splitNode_mM_RAD(n2);
        }
    }

    /**
     * Removes all irrelevant objects from this tree according to the given
     * split node policy.
     */
    public void clonningSplit(SplitPolicy sp, boolean include) throws BucketStorageException {

        // Setting unknown parent distance.
        Split.setParentDistances(sp, LocalAbstractObject.UNKNOWN_DISTANCE);

        // Indicates whether the objects in slim nodes should be reinserted into
        // the tree after splitting the tree.
        boolean reinsertSlimNodesObjects = true;
        //boolean reinsertSlimNodesObjects = false;

        MTree mtree1 = this;

        // All internal nodes which descendants are leaves.
        List<InternalNode> nodes = Search.findLastInternalNodes(mtree1.getRoot());

        // Determining and computing the missing parameters of the split node policy.
        Split.completeSplitPolicy(sp, mtree1);

        // If the tree has only one node (a leaf node) then only the precomputed
        // distances to pivots can be used for filtering.
        if (mtree1.getRoot().isLeaf()) {
            AbstractObjectIterator<LocalAbstractObject> it = mtree1.getRoot().getObjects();
            while (it.hasNext()) {
                int matchObject = sp.match(it.next());
                if ((include && matchObject == 1) || (!include && matchObject == 0)) {
                    it.remove();
                }
            }
        } else {
            Set<LocalAbstractObject> slimNodesObjects = new HashSet<LocalAbstractObject>();

            // Filtering particular subtrees.
            for (InternalNode n : nodes) {
                Iterator<NodeEntry> nodeEntryIterator = n.getNodeEntryList().iterator();
                //for (NodeEntry ne: n.getNodeEntryList()) {
                while (nodeEntryIterator.hasNext()) {
                    NodeEntry ne = nodeEntryIterator.next();

                    // Filtering the whole subtree.
                    BallRegion br = new BallRegion(ne.getObject(), ne.getRadius());
                    int matchBallRegion = sp.match(br);
                    if (matchBallRegion != -1) {
                        if ((include && matchBallRegion == 1) || (!include && matchBallRegion == 0)) {
                            n.removeNodeEntry(ne, nodeEntryIterator);
                            n.updateDistances();
                            Node newRoot = Insert.deleteSlimNodes(n);
                            if (newRoot != null) {
                                mtree1.setRoot(newRoot, mtree1.bucketDispatcher);
                            }
                        }
                    } else {

                        // Filtering particular objects.
                        Split.setParentDistances(sp);
                        Node subtree = ne.getSubtree();
                        AbstractObjectIterator<LocalAbstractObject> it = subtree.getObjects();
                        while (it.hasNext()) {
                            int matchObject = sp.match(it.next());
                            if ((include && matchObject == 1) || (!include && matchObject == 0)) {
                                it.remove();
                            }
                        }
                        subtree.updateDistances();

                        // Slim nodes objects are reinserted and the given node is deleted.
                        if (subtree.getObjectCount() <= 1) {

                            LocalAbstractObject objectToReinsert = null;
                            if (reinsertSlimNodesObjects && subtree.getObjectCount() == 1) {
                                objectToReinsert = subtree.getObjects().next();
                                //((LeafNode) subtree).deleteObject(objectToReinsert);
                                ((LeafNode) subtree).getBucket().deleteAllObjects();
                            }

                            // Deleting slim nodes.
                            Node newRoot = Insert.deleteSlimNodes(subtree, nodeEntryIterator);
                            if (newRoot != null) {
                                mtree1.setRoot(newRoot, mtree1.bucketDispatcher);
                            }

                            if (objectToReinsert != null) {
                                slimNodesObjects.add(objectToReinsert);
                            }
                        }
                    }
                }
            }

            // Reinserting objects from slim nodes.
            if (reinsertSlimNodesObjects) {
                for (LocalAbstractObject o : slimNodesObjects) {
                    mtree1.addObject(o);
                }
            }
        }
    }

    /**
     * Splits this tree into two trees according to the given split node policy
     * policy.
     */
    public void deletingSplit(SplitPolicy sp, SplittableAlgorithmResult result) throws BucketStorageException, InstantiationException {

        // Setting unknown parent distance.
        Split.setParentDistances(sp, LocalAbstractObject.UNKNOWN_DISTANCE);

        // Indicates whether the objects in slim nodes should be reinserted into
        // the tree after splitting the tree.
        boolean reinsertSlimNodesObjects = true;

        // Creating a new tree with the same parameters as the current tree.
        MTree mtree1 = this;
        MTree mtree2 = mtree1.cloneWithoutObjects();

        // All internal nodes which descendants are leaves.
        List<InternalNode> nodes = Search.findLastInternalNodes(mtree1.getRoot());

        // Determining and computing the missing parameters of the split node policy.
        Split.completeSplitPolicy(sp, mtree1);

        // If the tree has only one node (a leaf node) then only the precomputed
        // distances to pivots can be used for filtering.
        if (mtree1.getRoot().isLeaf()) {
            AbstractObjectIterator<LocalAbstractObject> it = mtree1.getRoot().getObjects();
            while (it.hasNext()) {
                LocalAbstractObject o = it.next();
                if (sp.match(o) == 1) {
                    result.markMovedObject(mtree2, o);
                    it.remove();
                    mtree2.addObject(o);
                }
            }
        } else {
            Set<LocalAbstractObject> slimNodesObjects = new HashSet<LocalAbstractObject>();

            // Filtering particular subtrees.
            for (InternalNode n : nodes) {
                Iterator<NodeEntry> nodeEntryIterator = n.getNodeEntryList().iterator();
                //for (NodeEntry ne: n.getNodeEntryList()) {
                while (nodeEntryIterator.hasNext()) {
                    NodeEntry ne = nodeEntryIterator.next();

                    // Filtering the whole subtree.
                    BallRegion br = new BallRegion(ne.getObject(), ne.getRadius());
                    int matchBallRegion = sp.match(br);
                    if (matchBallRegion != -1) {
                        if (matchBallRegion == 1) {
                            AbstractObjectIterator<LocalAbstractObject> it = ne.getSubtree().getObjects();
                            while (it.hasNext()) {
                                mtree2.addObject(it.next());
                            }
                            n.removeNodeEntry(ne, nodeEntryIterator);
                            n.updateDistances();
                            Node newRoot = Insert.deleteSlimNodes(n);
                            if (newRoot != null) {
                                mtree1.setRoot(newRoot, mtree1.bucketDispatcher);
                            }
                        }
                    } else {

                        // Filtering particular objects.
                        Split.setParentDistances(sp);
                        Node subtree = ne.getSubtree();
                        AbstractObjectIterator<LocalAbstractObject> it = subtree.getObjects();
                        while (it.hasNext()) {
                            LocalAbstractObject o = it.next();
                            if (sp.match(o) == 1) {
                                mtree2.addObject(o);
                                it.remove();
                            }
                        }
                        subtree.updateDistances();

                        // Slim nodes objects are reinserted and the given node is deleted.
                        if (subtree.getObjectCount() <= 1) {

                            LocalAbstractObject objectToReinsert = null;
                            if (reinsertSlimNodesObjects && subtree.getObjectCount() == 1) {
                                objectToReinsert = subtree.getObjects().next();
                                //((LeafNode) subtree).deleteObject(objectToReinsert);
                                ((LeafNode) subtree).getBucket().deleteAllObjects();
                            }

                            // Deleting slim nodes.
                            Node newRoot = Insert.deleteSlimNodes(subtree, nodeEntryIterator);
                            if (newRoot != null) {
                                mtree1.setRoot(newRoot, mtree1.bucketDispatcher);
                            }

                            if (objectToReinsert != null) {
                                slimNodesObjects.add(objectToReinsert);
                            }
                        }
                    }
                }
            }

            // Computing moved objects and bytes.
            AbstractObjectIterator<LocalAbstractObject> it = mtree2.getAllObjects();
            while (it.hasNext()) {
                result.markMovedObject(mtree2, it.next());
            }

            // Reinserting objects from slim nodes.
            if (reinsertSlimNodesObjects) {
                for (LocalAbstractObject o : slimNodesObjects) {
                    mtree1.addObject(o);
                }
            }
        }
    }

    /**
     * **************** Operations *****************
     */
    /**
     * Looks for all objects stored in the tree.
     */
    public void getAllObjectsQueryOperation(GetAllObjectsQueryOperation qo) {
        for (LocalBucket bucket : getAllBuckets()) {
            bucket.processQuery(qo);
        }
        qo.endOperation();
    }

    /**
     * Random-access query for SAPIR API. Implemented as (index) searching all
     * buckets.
     */
    public void getObjectsByLocatorsOperation(GetObjectsByLocatorsOperation qo) {
        for (LocalBucket bucket : getAllBuckets()) {
            bucket.processQuery(qo);
        }
        qo.endOperation();
    }

    /**
     * Random-access query for SAPIR API. Implemented as (index) searching all
     * buckets.
     */
    public void getObjectByLocatorOperation(GetObjectByLocatorOperation qo) {
        for (LocalBucket bucket : getAllBuckets()) {
            if (bucket.processQuery(qo) == 1) {
                break;
            }
        }
        qo.endOperation();
    }

    /**
     * Deletes the specific object from the tree.
     *
     * @param delOper holds objects to delete from the tree
     * @return <tt>false</tt> if the tree does not contain the specific object
     */
    public boolean deleteObject(DeleteOperation delOper) {

        List<LeafNode> leaves = getAllLeaves();
        for (LeafNode leaf : leaves) {
            LocalBucket bucket = leaf.getBucket();

            // Deleting the object from a bucket.
            if (leaf.deleteObjects(delOper) > 0) {

                // Slim nodes objects are reinserted and the given node is deleted.
                if (bucket.getObjectCount() <= 1) {

                    LocalAbstractObject objectToReinsert = null;
                    if (bucket.getObjectCount() == 1) {
                        objectToReinsert = bucket.getAllObjects().next();
                        try {
                            bucket.deleteAllObjects();
                        } catch (BucketStorageException ex) {
                            ex.printStackTrace();
                        }
                    }

                    // Deleting slim nodes.
                    Node n = Insert.deleteSlimNodes(leaf);
                    if (n != null) {
                        try {
                            this.setRoot(n, bucketDispatcher);
                        } catch (BucketStorageException e) {
                            e.printStackTrace();
                        }
                    }

                    if (objectToReinsert != null) {
                        this.addObject(objectToReinsert);
                    }
                }

                return true;
            }
        }
        return false;
    }

    /**
     * Range search.
     *
     * Looks for all objects incident to the region R(q, radius). The response
     * is returned in the operation's response list.
     *
     * @param rqo Range query operation which carries the query object and
     * radius as well as the response list.
     * @return returns <code>true</code>
     */
    public boolean rangeSearch(RangeQueryOperation rqo) {

        // Computes distances between the query object and all pivots.
        Search.setPrecompDistances(rqo.getQueryObject(), pivots);

//        bindStatsOptimIndex();
        
        int accessedNodes = rangeSearch(root, LocalAbstractObject.UNKNOWN_DISTANCE, rqo);
        rqo.setParameter("AccessedNodes", accessedNodes);
        
        computeOptimIndex();
        
        rqo.endOperation();
        return true;
    }

    /** Compute 'I' -- the seach structure effectiveness index */
    private void computeOptimIndex() {
        float C = leafCap;
        float l = OperationStatistics.getOpStatisticCounter(STAT_NAME_LEAVES_VISITED).get();    // visited leaves
        
        // THE INDEX for all counts of objects found during query processing
        float sum = OperationStatistics.getOpStatisticCounter(STAT_NAME_OBJS_SUM).get();
        float idx = (float)Math.sqrt(sum / (l * C * C)) * 100;  // in percent
        OperationStatistics.getOpStatistics(STAT_NAME_OPTIM_INDEX, StatisticObject.class).set(idx);
        
        // THE INDEX for objects in the final result per bucket only
        StatisticRefCounter opObjsPerBucket = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_OBJS_PER_BUCKET);
        sum = 0;
        for (StatisticCounter s : opObjsPerBucket.getValue().values()) {
            long inanswer = s.get();
            sum += inanswer * inanswer;
        }
        idx = (float)Math.sqrt(sum / (l * C * C)) * 100;  // in percent
        OperationStatistics.getOpStatistics(STAT_NAME_OPTIM_INDEX_ON_RESULT, StatisticObject.class).set(idx);
    }

    private void bindStatsOptimIndex() {
        if (!Statistics.isEnabledGlobally())
            return;
        
        statOptimIndexObjCntPerBucket.reset();
        StatisticRefCounter opRef = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_OBJS_PER_BUCKET);
        opRef.bindTo(statOptimIndexObjCntPerBucket);
        
        statOptimIndexNewObjPerBucket.reset();
        opRef = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_NEW_OBJS_PER_BUCKET);
        opRef.bindTo(statOptimIndexNewObjPerBucket);

        statVisitedLeaves.reset();
        StatisticCounter opCnt = OperationStatistics.getOpStatisticCounter(STAT_NAME_LEAVES_VISITED);
        opCnt.bindTo(statVisitedLeaves);

        StatisticCounter opSum = OperationStatistics.getOpStatisticCounter(STAT_NAME_OBJS_SUM);
        opSum.bindTo(statOptimIndexObjSum);
        
        statLeafNodeVisitedOrder.reset();
        statLeafNodeVisitedOrder.turnToInsertOrdered();
        StatisticRefCounter opOrd = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_ORDER);
        opOrd.turnToInsertOrdered();
        opOrd.bindTo(statLeafNodeVisitedOrder);
        
        statDistanceToKNNCandidate.reset();
        statDistanceToKNNCandidate.turnToInsertOrdered();
        opOrd = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_DISTANCE_TO_KNN);
        opOrd.turnToInsertOrdered();
        opOrd.bindTo(statDistanceToKNNCandidate);
    }
    
    private void computeResultObjectsPerBucketStat(KNNQueryOperation kNNOper) {
        StatisticRefCounter opObjsPerBucket = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_OBJS_PER_BUCKET);
        opObjsPerBucket.turnToInsertOrdered();
        StatisticRefCounter opBucketOrder = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_BUCKETS_ORDERED_BY_VISIT);
        opBucketOrder.turnToInsertOrdered();
//        StatisticRefCounter opBucketContainsQ = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_BUCKETS_CONTAINS_QUERY);
//        opBucketContainsQ.turnToInsertOrdered();
        
        StatisticRefCounter opBucketVisitOrder = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_ORDER);

        // Count the number of objects in the operation answer for each bucket
        int answerSize = kNNOper.getAnswerCount();
        if (kNNOper instanceof Approximate) {           // Set the limit on the size of "correct" results of approximate query
            int answerPerc = kNNOper.getParameter("usefulnessOmitPerc", Integer.class, 100);
            answerSize = (int)Math.ceil((float)answerSize * (float)answerPerc / 100f);
        }
        int objCnt = 0;
        for (Iterator<RankedAbstractObject> iterator = kNNOper.getAnswer(); iterator.hasNext();) {
            AbstractObject next = iterator.next().getObject();
            objCnt++;
            if (objCnt == answerSize)       // Stop when all non-omitted objects were checked
                break;
            BucketIdObjectKey key = next.getObjectKey(BucketIdObjectKey.class);
            if (key != null) {
                if (!testMode)                              // Increment object's usefulness (ICI) only if we should do so, but do gather statistics!
                    key.incUsefulness();
                opObjsPerBucket.add(key.getBucketId());
            }
        }
        
        // Update ICI values (usefulness) in nodes...
        if (!testMode) {
//            int freq = (kNNOper.getParameter("useFrequency", Boolean.class, false)) ? getQueryFrequency(kNNOper.getQueryObject().getLocatorURI()) : 1;
            int freq = kNNOper.getParameter("queryFrequency", Integer.class, 1);
            OperationStatistics.getOpStatisticCounter(STAT_NAME_QUERY_FREQUENCY).set(freq);

            // Type of usefulness!!!!!!!
            String usefulnessType = kNNOper.getParameter("usefulnessType", String.class, "queryCount");
            
            if ("queryCount".equalsIgnoreCase(usefulnessType)) {        
                // ***** Node's usefulness == number of queries that contain an object of this node's subtree in the answer
                Set<Node> accessedNodes = new HashSet<>();
                for (Object key : opObjsPerBucket.getKeys()) {
                    int bucketId = (int)key;
                    LeafNode l = findByBucketId(bucketId, root);

                    //l.incNodeUsefulness();                        // Increment leaf node only!
                    //l.incNodeUsefulnessRecursivelyUpToRoot();       // Increment all internal nodes on the way to root too!
                    //l.nodeUsefulness--;     // Increment only non-leaf level... (must be used together with incNodeUsefulnessRecursivelyUpToRoot()

                    // Gather accessed nodes (and increment usefulnes only once)
                    for (Node p = l; p != null; p = p.getParentNode())
                        accessedNodes.add(p);
                }
                for (Node n : accessedNodes)
                    n.incNodeUsefulness(freq);
            } else if ("objectRatio".equalsIgnoreCase(usefulnessType)) {
                // ***** Node's usefulness == sum of the ratio of objects taken from this node's subtree to the answer size for each query processed.
                Map<LeafNode,Integer> accessedNodes = new HashMap<>();
                for (Object key : opObjsPerBucket.getKeys()) {
                    int bucketId = (int)key;
                    LeafNode l = findByBucketId(bucketId, root);
                    Integer cnt = accessedNodes.get(l);
                    if (cnt == null)
                        cnt = 1;
                    else
                        cnt++;
                    accessedNodes.put(l, cnt);
                }
                // For each leaf, update it and all its parents...
                for (Map.Entry<LeafNode,Integer> e : accessedNodes.entrySet()) {
                    float ratio = ((float)freq) * ((float)e.getValue()) / ((float)answerSize);
                    for (Node p = e.getKey(); p != null; p = p.getParentNode())
                        p.incNodeUsefulness(ratio);
                }
            } else {
                log.log(Level.SEVERE, "Unknown usefulness type '{0}'!!!", usefulnessType);
            }
        }
        
        // Get the indexes of visiting buckets for the necessary buckets
        long visitedOrderMax = -1;
        for (Object key : statLeafNodeVisitedOrder.getKeys()) {
            if (opObjsPerBucket.containsKey(key)) {
                opBucketOrder.set(key, statLeafNodeVisitedOrder.get(key));
                if (statLeafNodeVisitedOrder.get(key) > visitedOrderMax)
                    visitedOrderMax = statLeafNodeVisitedOrder.get(key);
                
//                // Compute distance between query and pivot of this bucket
//                LeafNode bucket = findByBucketId((int)key, root);
//                float dist = kNNOper.getQueryObject().getDistance(bucket.getParentNodeEntry().getObject());
//                if (dist > bucket.getParentNodeEntry().getRadius())
//                    dist = -dist;
//                opBucketContainsQ.set(key, (long)(dist * 1000f));
            }
        }
        OperationStatistics.getOpStatisticCounter(STAT_NAME_ANSWER_BUCKETS_ORDERED_BY_VISIT_MAX).set(visitedOrderMax);
        // Compute lower/upper bound for visited buckets
        computeStatsVisitedBuckets(statLeafNodeVisitedOrder.getKeys(), 999999 /*visitedOrderMax*/, kNNOper);
        
        // Set total number of necessary buckets (only the buckets used to get this query result)
        OperationStatistics.getOpStatisticCounter(STAT_NAME_NECESSARY_BUCKETS).set(opObjsPerBucket.getKeyCount());
        
        // Get pivot-query distance and lower/upper bounds for all necessary buckets
        StatisticRefCounter statDistQueryPivot = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_BUCKETS_DISTANCE_PIVOT);
        statDistQueryPivot.turnToInsertOrdered();
        StatisticRefCounter statDistLowerBound = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_BUCKETS_DISTANCE_LB);
        statDistLowerBound.turnToInsertOrdered();
        StatisticRefCounter statDistUpperBound = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ANSWER_BUCKETS_DISTANCE_UB);
        statDistUpperBound.turnToInsertOrdered();
        LocalAbstractObject queryObject = kNNOper.getQueryObject();
        for (Object key : opBucketOrder.getKeys()) {
            int bucketId = (int)key;
            LeafNode l = findByBucketId(bucketId, root);
            NodeEntry n = l.getParentNodeEntry();
            float d = queryObject.getDistance(n.getObject());
            statDistQueryPivot.set(key, (long)(d * 1000f));
            statDistLowerBound.set(key, (long)((d - n.getRadius()) * 1000f));
            statDistUpperBound.set(key, (long)((d + n.getRadius()) * 1000f));
        }
        
        // Compute the distance to the 30th nearest neighbor in the bucket where the first object (in the final answer) was located
        try {
            if (kNNOper.getAnswerCount() > 0) {
                BucketIdObjectKey objKey = kNNOper.getAnswer().next().getObject().getObjectKey(BucketIdObjectKey.class);
                if (objKey != null) {
                    int closestBucketId = objKey.getBucketId();
                    LocalBucket closestBucket = bucketDispatcher.getBucket(closestBucketId);
                    KNNQueryOperation closestBucketKNNOper = new KNNQueryOperation(kNNOper.getQueryObject().clone(false), kNNOper.getK()); //(KNNQueryOperation)kNNOper.clone(false);
                    closestBucketKNNOper.evaluate(closestBucket.getAllObjects());
                    OperationStatistics.getOpStatisticCounter(STAT_NAME_ANSWER_CLOSEST_BUCKET).set(closestBucketId);
                    OperationStatistics.getOpStatisticCounter(STAT_NAME_ANSWER_CLOSEST_BUCKET_ORDER_VISITED).set(opBucketVisitOrder.get(closestBucketId));
                    OperationStatistics.getOpStatistics(STAT_NAME_ANSWER_CLOSEST_BUCKET_DIST_TO_KNN, StatisticObject.class).set(closestBucketKNNOper.getAnswerDistance());
                }
            }
        } catch(CloneNotSupportedException ex) {
            System.err.println("Failed to clone kNN operation! Class is " + kNNOper.getClass().getName());
        }
        
        // Compute the distance to the 30th nearest neighbor in the bucket which was visited as first (and the expansion rate)
        try {
            // Get the lowest value (1) out of the operation stats containing [bucketID,order] pairs.
            int firstBucketId = -1;
            int secondBucketId = -1;
            StatisticRefCounter opOrd = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_ORDER);
            for (Map.Entry<Object, StatisticCounter> e : opOrd.getValue().entrySet()) {
                StatisticCounter sc = e.getValue();
                if (sc.get() == 1)
                    firstBucketId = (Integer)e.getKey();
                else if (sc.get() == 2)
                    secondBucketId = (Integer)e.getKey();
            }
            
            // First bucket stats
            if (firstBucketId != -1) {
                LocalBucket firstBucket = bucketDispatcher.getBucket(firstBucketId);
                KNNQueryOperation firstBucketKNNOper = new KNNQueryOperation(kNNOper.getQueryObject().clone(false), kNNOper.getK() * 2); //(KNNQueryOperation)kNNOper.clone(false);
                firstBucketKNNOper.evaluate(firstBucket.getAllObjects());
                OperationStatistics.getOpStatisticCounter(STAT_NAME_ANSWER_FIRST_BUCKET).set(firstBucketId);
                float distK = (firstBucketKNNOper.getAnswerCount() >= kNNOper.getK()) 
                                    ? firstBucketKNNOper.getAnswer(kNNOper.getK()-1, 1).next().getDistance()
                                    : firstBucketKNNOper.getAnswerDistance();
                OperationStatistics.getOpStatistics(STAT_NAME_ANSWER_FIRST_BUCKET_DIST_TO_KNN, StatisticObject.class).set(distK); // firstBucketKNNOper.getAnswerDistance());
                float expRate = ((firstBucketKNNOper.isAnswerFull()) ? firstBucketKNNOper.getAnswerDistance() : 0) / distK;
                OperationStatistics.getOpStatistics(STAT_NAME_ANSWER_FIRST_BUCKET_EXP_RATE, StatisticObject.class).set(expRate);
            }
            
            // Second bucket stats
            if (secondBucketId != -1) {
                LocalBucket secondBucket = bucketDispatcher.getBucket(secondBucketId);
                KNNQueryOperation secondBucketKNNOper = new KNNQueryOperation(kNNOper.getQueryObject().clone(false), kNNOper.getK() * 2); //(KNNQueryOperation)kNNOper.clone(false);
                secondBucketKNNOper.evaluate(secondBucket.getAllObjects());
                OperationStatistics.getOpStatisticCounter(STAT_NAME_ANSWER_SECOND_BUCKET).set(secondBucketId);
                float distK = (secondBucketKNNOper.getAnswerCount() >= kNNOper.getK()) 
                                    ? secondBucketKNNOper.getAnswer(kNNOper.getK()-1, 1).next().getDistance()
                                    : secondBucketKNNOper.getAnswerDistance();
                OperationStatistics.getOpStatistics(STAT_NAME_ANSWER_SECOND_BUCKET_DIST_TO_KNN, StatisticObject.class).set(distK); // firstBucketKNNOper.getAnswerDistance());
                float expRate = ((secondBucketKNNOper.isAnswerFull()) ? secondBucketKNNOper.getAnswerDistance() : 0) / distK;
                OperationStatistics.getOpStatistics(STAT_NAME_ANSWER_SECOND_BUCKET_EXP_RATE, StatisticObject.class).set(expRate);
            }
            
        } catch(CloneNotSupportedException ex) {
            System.err.println("Failed to clone kNN operation! Class is " + kNNOper.getClass().getName());
        }
    }
    
    private static Map<String,Integer> queryFrequency = null;
    private static int queryPadding = 0;
    
    private int getQueryFrequency(String queryObjectId) {
        if (queryFrequency == null) {
            // Initialize the map...
            initQueryFrequency("GA1000entries.csv", 9);
        }
        Integer freq = queryFrequency.get(queryObjectId);
        if (freq == null) {
            String padded = padId(queryObjectId, queryPadding);
            freq = queryFrequency.get(padded);
            if (freq == null) {
                log.log(Level.WARNING, "Failed to get frequency of query:{0}", queryObjectId);
                freq = 1;
            }
        }
        return freq;
    }
    
    /** Initialize query freqencies from a file.
     * @param frequencyFile file path
     * @param paddingLen length of resultin object key after possible padding with zeroes on the left. If zero is passed, no padding is done.
     */
    public void initQueryFrequency(String frequencyFile, int paddingLen) {
        queryFrequency = new TreeMap<>();
        queryPadding = paddingLen;
        String line = null;
        try {
            BufferedReader scanner = new BufferedReader(new FileReader(frequencyFile));
            while ( (line = scanner.readLine()) != null) {
                String[] matches = line.split("[,\t]");
                if (paddingLen > 0)
                    matches[0] = padId(matches[0], paddingLen);
                queryFrequency.put(matches[0], Integer.parseInt(matches[1], 10));
                //int qId = Integer.parseInt(matches[0], 10);
                //queryFrequency.put(String.format("%09d", qId), Integer.parseInt(matches[1], 10));
            }
        } catch (IOException ex) {
            System.err.println("Failed to read file GA1000entries.csv! " + ex.getMessage());
        } catch (NumberFormatException ex1) {
            System.err.println("Failed to parse line: " + line + " Error: " + ex1.getMessage());
        }
    }

    private String padId(String id, int paddingLen) {
        StringBuilder sb = new StringBuilder();
        for (int l = paddingLen - id.length(); l > 0; l--)
            sb.append('0');
        sb.append(id);
        return sb.toString();
    }
    
    
    public void createTrainingTestingSubsets() throws FileNotFoundException, IOException {
        // Initialize map
        getQueryFrequency("xxxxxx");
        // Make list of all respecting frequency
        List<String> all = new ArrayList<>(10000);
        for (Map.Entry<String, Integer> entrySet : queryFrequency.entrySet()) {
            String key = entrySet.getKey();
            int cnt = entrySet.getValue();
            for (int i = 0; i < cnt; i++)
                all.add(key);
        }
        // Shuffle it
        for (int s = 0; s < all.size(); s++) {
            int i1 = (int)(Math.random() * (double)all.size());
            int i2 = (int)(Math.random() * (double)all.size());
            if (i1 != i2) {
                String tmp = all.get(i1);
                all.set(i1, all.get(i2));
                all.set(i2, tmp);
            }
        }
        // Take first as training...
        int trainingSize = (int)((float)queryFrequency.size() * 0.7f);
        Set<String> training = new TreeSet<>();
        Set<String> testing = new TreeSet<>();
        Set<String> curr = training;    // Fill the training set first
        for (String k : all) {
            // Skip non-unique query IDs
            if (training.contains(k) || testing.contains(k))
                continue;
            curr.add(k);
            // Add the rest to the testing set
            if (curr == training && curr.size() == trainingSize)
                curr = testing;
        }
        // Print training, then testing...
        final byte[] newLine = "\r\n".getBytes();
        OutputStream out = new FileOutputStream("query-ids-training.txt");
        for (String k : training) {
            out.write(k.getBytes());
            out.write(newLine);
        }
        out.close();
        out = new FileOutputStream("query-ids-testing.txt");
        for (String k : testing) {
            out.write(k.getBytes());
            out.write(newLine);
        }
        out.close();
    }
    
    private void computeStatsVisitedBuckets(Set<Object> keys, long visitedOrderMax, KNNQueryOperation kNNOper) {
        StatisticRefCounter opBucketVisitedDistanceToQ = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_DISTANCE_PIVOT);
        opBucketVisitedDistanceToQ.turnToInsertOrdered();
        StatisticRefCounter opBucketVisitedDistanceLB = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_DISTANCE_LB);
        opBucketVisitedDistanceLB.turnToInsertOrdered();
        StatisticRefCounter opBucketVisitedDistanceUB = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_LEAVES_VISITED_DISTANCE_UB);
        opBucketVisitedDistanceUB.turnToInsertOrdered();
        
        int cnt = 0;
        for (Object key : keys) {
            int bId = (int)key;
            // Compute distance between query and pivot of this bucket
            LeafNode bucket = findByBucketId(bId, root);
            NodeEntry n = bucket.getParentNodeEntry();
            float d = kNNOper.getQueryObject().getDistance(n.getObject());
            opBucketVisitedDistanceToQ.set(key, (long)(d * 1000f));
            opBucketVisitedDistanceLB.set(key, (long)((d - n.getRadius()) * 1000f));
            opBucketVisitedDistanceUB.set(key, (long)((d + n.getRadius()) * 1000f));
            
            if (++cnt > visitedOrderMax)        // Do it only for all accessed buckets up to the necessary one plus one more.
                break;
        }
        
        StatisticRefCounter opBucketAllDistanceToQ = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ALL_LEAVES_DISTANCE_PIVOT);
        StatisticRefCounter opBucketAllDistanceLB = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ALL_LEAVES_DISTANCE_LB);
        StatisticRefCounter opBucketAllDistanceUB = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ALL_LEAVES_DISTANCE_UB);
        // Add already computed bounds...
        for (Object k : opBucketVisitedDistanceToQ.getKeys()) {
            opBucketAllDistanceToQ.set(k, opBucketVisitedDistanceToQ.get(k));
            opBucketAllDistanceLB.set(k, opBucketVisitedDistanceLB.get(k));
            opBucketAllDistanceUB.set(k, opBucketVisitedDistanceUB.get(k));
        }
        // Get the rest buckets...
        for (LeafNode l : getAllLeaves()) {
            int key = l.getBucket().getBucketID();
            if (opBucketVisitedDistanceToQ.containsKey(key))     // Skip already computed buckets
                continue;
            
            NodeEntry n = l.getParentNodeEntry();
            float d = kNNOper.getQueryObject().getDistance(n.getObject());
            opBucketAllDistanceToQ.set(key, (long)(d * 1000f));
            opBucketAllDistanceLB.set(key, (long)((d - n.getRadius()) * 1000f));
            opBucketAllDistanceUB.set(key, (long)((d + n.getRadius()) * 1000f));
        }
        
        // Pivot distance for all internal nodes
        StatisticRefCounter opAllNodesDistanceToQ = OperationStatistics.getOpStatisticRefCounter(STAT_NAME_ALL_NODES_DISTANCE_PIVOT);
        Map<Integer, List<Node>> nodesByLevel = new TreeMap<>();
        getNodesByLevel(nodesByLevel);
        for (Map.Entry<Integer, List<Node>> e : nodesByLevel.entrySet()) {
            Integer lvl = e.getKey();
            List<Node> nodes = e.getValue();
            for (Node n : nodes) {
                if (n.isLeaf())
                    break;
                String key = lvl + "-" + n.nodeIdPerLevel;
                if (opAllNodesDistanceToQ.containsKey(key))
                    continue;
                float d;
                if (n.getParentNode() == null)
                    d = -1f;
                else
                    d = kNNOper.getQueryObject().getDistance(((InternalNode)n).getParentNodeEntry().getObject());
                opAllNodesDistanceToQ.set(key, (long)(d * 1000f));
            }
        }
    }

    /**
     * Recursive method for rangeSearch.
     * @return number of nodes accessed (colliding with the query's region)
     */
    private int rangeSearch(Node n, float qParentDist, RangeQueryOperation rqo) {

        LocalAbstractObject q = rqo.getQueryObject();
        float r = rqo.getRadius();

        // If the given node is a leaf then it is searched for incident objects
        // which are added to the list. Else all descendants are recursively
        // searched.
        if (n.isLeaf()) {
            q.getDistanceFilter(ParentFilter.class).setParentDist(qParentDist);
            LocalBucket bucket = ((LeafNode) n).getBucket();
            int added = bucket.processQuery(rqo);       // uses all precomputed distance filters by default
            statVisitedLeaves.add();
            statOptimIndexObjSum.add(added * added);
            statOptimIndexObjCntPerBucket.add(bucket.getBucketID(), added);
            statOptimIndexNewObjPerBucket.add(bucket.getBucketID(), added);
            return 1;
        } else {

            // All subtrees of the given node are searched.
            int acc = 1;
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                float oParentDist = ne.getParentDist();

                if (!Search.filterNodeEntry(ne, oParentDist, q, qParentDist, r)) {
                    float qPivotDist = (oParentDist == 0f && qParentDist != LocalAbstractObject.UNKNOWN_DISTANCE) 
                                        ? qParentDist : q.getDistance(ne.getObject());
                    if (qPivotDist <= ne.getRadius() + r) {
                        acc += rangeSearch(ne.getSubtree(), qPivotDist, rqo);
                    }
                }
            }
            return acc;
        }
    }

    /**
     * k-NN search operation Performs the k-nearest neighbor search operation
     * with given KNNQueryOperation object. The answer is held in the
     * KNNQueryOperation object.
     *
     * @param kNNOper kNN query operation which carries the query object and k
     * as well as the response list.
     * @return returns <code>true</code>
     */
    public boolean kNN(KNNQueryOperation kNNOper) throws ClassNotFoundException, NoSuchInstantiatorException, InvocationTargetException {
        return kNNsearch(kNNOper, null);
    }
    
    private boolean kNNsearch(KNNQueryOperation kNNOper, ApproxState approxState) throws ClassNotFoundException, NoSuchInstantiatorException, InvocationTargetException {
//        OperationStatistics.getLocalThreadStatistics().resetStatistics();
//        OperationStatistics.getLocalThreadStatistics().registerBoundAllStats("DistanceComputations.*");
        final StatisticCounter gDC = StatisticCounter.getStatistics("DistanceComputations");
        final StatisticCounter gDCSaved = StatisticCounter.getStatistics("DistanceComputations.Savings");
        long gDCState = gDC.get();
        long gDCSavedState = gDCSaved.get();
        StatisticCounter opBucketsVisited = OperationStatistics.getOpStatisticCounter(STAT_NAME_LEAVES_VISITED);
        opBucketsVisited.reset();
        opBucketsVisited.bindTo(statVisitedLeaves);
        
        // Computes distances between the query object and all pivots.
        Search.setPrecompDistances(kNNOper.getQueryObject(), pivots);

        // Map of distances between query object and pivots (routing objects) in the tree.
        Map<LocalAbstractObject, Float> pd = new HashMap<LocalAbstractObject, Float>();

        // Sorted queue of the most promising subtrees.
        String comparatorClass = kNNOper.getParameter("queueOrdering", String.class);
        SearchQueue.EntryComparator<Node> comparator;
        if (comparatorClass != null) {
            comparator = (SearchQueue.EntryComparator<Node>) ConstructorInstantiator.createInstanceWithStringArgs(Class.forName(comparatorClass), new Object[] {}, 0, -1, null);
        } else {
            Boolean useUsefulness = kNNOper.getParameter("usefulness", Boolean.class);      // For compatibility...
            if (useUsefulness != null && useUsefulness)
                comparator = new SearchQueue.EntryComparatorByUsefulnessOnPivotQueryDistance<Node>();
            else
                comparator = new SearchQueue.EntryComparatorByLowerBoundNatural<Node>();
        }
        // Base of logarithm used in comparator
        Float logBase = kNNOper.getParameter("queueOrderingLogBase", Float.class);
        if (logBase != null) {
            try {
                MethodInstantiator.callMethod(comparator, "setLogBase", false, true, null, logBase);
                Logger.getLogger(MTree.class.getName()).log(Level.INFO, "Base of logarithm in SearchQueue comparator set to: {0}", logBase);
            } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
                // Ignore this because the method may not exist, so it is OK.
            }
        }
        // Power of distance
        Float distPower = kNNOper.getParameter("queueOrderingDistPower", Float.class);
        if (distPower != null) {
            try {
                MethodInstantiator.callMethod(comparator, "setDistancePower", false, true, null, distPower);
                Logger.getLogger(MTree.class.getName()).log(Level.INFO, "Power of distance in SearchQueue comparator set to: {0}", distPower);
            } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
                // Ignore this because the method may not exist, so it is OK.
            }
        }
        // Normalization distance used in comparator
        Float normDist = kNNOper.getParameter("queueOrderingNormDist", Float.class);
        if (normDist != null) {
            try {
                MethodInstantiator.callMethod(comparator, "setNormalizationDist", false, true, null, normDist);
                Logger.getLogger(MTree.class.getName()).log(Level.INFO, "Normalization distance in SearchQueue comparator set to: {0}", normDist);
            } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
                // Ignore this because the method may not exist, so it is OK.
            }
        }
        // Base of logarithm used in comparator
        Float lineSlope = kNNOper.getParameter("queueOrderingLineSlope", Float.class);
        if (lineSlope != null) {
            try {
                MethodInstantiator.callMethod(comparator, "setSlope", false, true, null, lineSlope);
                Logger.getLogger(MTree.class.getName()).log(Level.INFO, "Line slope in SearchQueue comparator set to: {0}", lineSlope);
            } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
                // Ignore this because the method may not exist, so it is OK.
            }
        }
        // Power of distance
        Float lineShift = kNNOper.getParameter("queueOrderingLineShift", Float.class);
        if (lineShift != null) {
            try {
                MethodInstantiator.callMethod(comparator, "setShift", false, true, null, lineShift);
                Logger.getLogger(MTree.class.getName()).log(Level.INFO, "Line shift in SearchQueue comparator set to: {0}", lineShift);
            } catch (InvocationTargetException | NoSuchInstantiatorException ex) {
                // Ignore this because the method may not exist, so it is OK.
            }
        }
        // Query frequency
        boolean useFreq = (kNNOper.getParameter("useFrequency", Boolean.class, false));
        if (useFreq) 
            kNNOper.setParameter("queryFrequency", getQueryFrequency(kNNOper.getQueryObject().getLocatorURI()));
        
        SearchQueue<Node> queue = new SearchQueue<Node>(comparator);

        boolean doBreadthFirst = kNNOper.getParameter("breadthFirst", Boolean.class, false);        // Default is the standard way == depth-first search with priority
        
//        bindStatsOptimIndex();
        
        // Initialize queue with root
        queue.add(root, root.getLevel(), 0f, 0f, 0f);
        // Looking for the most promising objects stored in the nearest leaves.
        float radius = kNNOper.getAnswerThreshold();
        
        if (doBreadthFirst) {
            // New: breadth-first search
            for (int l = root.getLevel(); l > 0; l--) {
                List<InternalNode> lst = new ArrayList<>(queue.getSize());
                while (!queue.isEmpty())
                    lst.add((InternalNode)queue.remove());
                for (InternalNode n : lst)
                    processNode(n, queue, pd, kNNOper.getQueryObject(), radius);
            }
            // Process leaf nodes...
            while (!queue.isEmpty()) {
                float headLB = queue.getHeadLowerBound();
                if (headLB > radius)
                    break;
                if (approxState != null && approxState.stop(headLB))
                    break;
                
                Node node = queue.remove();
                processLeafNode((LeafNode) node, pd, kNNOper, opBucketsVisited);
                radius = kNNOper.getAnswerThreshold();
                if (approxState != null)
                    approxState.update((LeafNode) node);
            }
        } else {
            StatisticCounter queueDepthFirst = OperationStatistics.getOpStatisticCounter(STAT_NAME_SEARCH_QUEUE_DEPTH_FIRST);
            StatisticCounter queueLength = OperationStatistics.getOpStatisticCounter(STAT_NAME_SEARCH_QUEUE_LENGTH);
                    
            while (!queue.isEmpty()) {
                float headLB = queue.getHeadLowerBound();
                if (headLB > radius)
                    break;
                if (approxState != null && approxState.stop(headLB))
                    break;
                
                Node node = queue.remove();

                if (nhr > 0) {
                    NodeEntry pne = node.getParentNodeEntry();
                    if (pne != null && Search.filterNodeEntryUsingPivots(pne, kNNOper.getQueryObject(), radius))
                        continue;
                }

                if (!node.isLeaf()) {
                    processNode((InternalNode) node, queue, pd, kNNOper.getQueryObject(), radius);
                    // Test whether the queue head changed to a more specific element and update statistics
                    int qs = queue.getSize();
                    if (qs > 0 && queue.peek().getLevel() < node.getLevel()) {
                        queueLength.max(qs);
                        queueDepthFirst.add();
                    }
                } else {
                    processLeafNode((LeafNode) node, pd, kNNOper, opBucketsVisited);
                    radius = kNNOper.getAnswerThreshold();
                    if (approxState != null)
                        approxState.update((LeafNode) node);
                }
            }
        }
        
        kNNOper.setParameter("DistanceComputations", gDC.get() - gDCState);
        kNNOper.setParameter("DistanceComputations.Savings", gDCSaved.get() - gDCSavedState);
        kNNOper.setParameter("Buckets.Visited", opBucketsVisited.get());

//        computeResultObjectsPerBucketStat(kNNOper);

//        computeOptimIndex();

//        checkGroundTruth(kNNOper);
        
        kNNOper.endOperation();
        return true;
    }

    /**
     * Process a leaf node from M-Tree queue. Leaf's associated bucket is
     * processed by the specified query operation.
     *
     * @param node the leaf node to be processed
     * @param pd precomputed distances to specific objects
     * @param kNNOper the kNN operation processed on the leaf
     */
    protected void processLeafNode(LeafNode node, Map<LocalAbstractObject, Float> pd, KNNQueryOperation kNNOper, StatisticCounter opBucketsVisited) {
        NodeEntry pne = node.getParentNodeEntry();
        float qParentDist = (pne == null) ? LocalAbstractObject.UNKNOWN_DISTANCE : pd.get(pne.getObject());
        kNNOper.getQueryObject().getDistanceFilter(ParentFilter.class).setParentDist(qParentDist);
        
        String[] preState = getObjectLocators(kNNOper);
        statVisitedLeaves.add();
        int added = node.getBucket().processQuery(kNNOper);
//        final AbstractObjectIterator<LocalAbstractObject> it = node.getBucket().getAllObjects();
//        while (it.hasNext())
//            kNNOper.addToAnswer(it.next());
//        int added = 0;
        String[] postState = getObjectLocators(kNNOper);
        final int[] indexesOfNew = getNewObjects(preState, postState);
        
        final int bucketID = node.getBucket().getBucketID();
        final long bucketAccessOrderNo = opBucketsVisited.get();
        markAnswerObjectsWithBucketAccessNo(kNNOper, indexesOfNew, bucketID, (int)bucketAccessOrderNo);
        
        statOptimIndexObjSum.add(added * added);
        statOptimIndexObjCntPerBucket.add(bucketID, added);
        statOptimIndexNewObjPerBucket.add(bucketID, indexesOfNew.length);
        statLeafNodeVisitedOrder.add(bucketID, bucketAccessOrderNo);  // Set the ordinal number of accessing this bucket (statVisitedLeaves is zeroed before starting each operation)
        statDistanceToKNNCandidate.add(bucketID, (long)(kNNOper.getAnswerDistance()*1000f));   // Distance to k-th nearest candidate
    }

    private String[] getObjectLocators(KNNQueryOperation kNNOper) {
        String[] locs = new String[kNNOper.getAnswerCount()];
        int i = 0;
        for (Iterator<RankedAbstractObject> iterator = kNNOper.getAnswer(); iterator.hasNext();) {
            locs[i++] = iterator.next().getObject().getLocatorURI();
        }
        return locs;
    }
    
    /** @return list of indexes in the post array of locators that correspond to the newly added objects */
    private int[] getNewObjects(String[] pre, String[] post) {
        int[] idxs = new int[post.length];
        int diff = 0;
        int iPre = 0, iPost = 0;
        for (String po : post) {
            if (iPre < pre.length && po.equals(pre[iPre]))
                iPre++;
            else {
                idxs[diff] = iPost;
                diff++;
            }
            iPost++;
        }
        if (diff != idxs.length) {
            int[] idxsRes = new int[diff];
            System.arraycopy(idxs, 0, idxsRes, 0, diff);
            return idxsRes;
        } else {
            return idxs;
        }
    }
    
    private void markAnswerObjectsWithBucketAccessNo(KNNQueryOperation kNNOper, int[] indexesOfNew, int bucketID, int bucketAccessOrderNo) {
        try {
            int i = 0, j = 0;
            for (Iterator<RankedAbstractObject> iterator = kNNOper.getAnswer(); iterator.hasNext() && j < indexesOfNew.length; i++) {
                if (i == indexesOfNew[j]) {
                    j++;
                    AbstractObject o = iterator.next().getObject();
                    o.setObjectKey(new BucketInfoObjectKey(o.getLocatorURI(), bucketID, bucketAccessOrderNo));
                } else {
                    AbstractObject o = iterator.next().getObject();
                    if (!(o.getObjectKey() instanceof BucketInfoObjectKey)) {
                        o.setObjectKey(new BucketInfoObjectKey(o.getLocatorURI(), -1, -1));
                    }
                }
            }
        } catch (ClassCastException e) {    // OK, we do not set anything
        }
    }

    /**
     * Process a leaf node from M-Tree queue. Leaf's associated objects (from
     * bucket) are added to queue.
     *
     * @param node the leaf node to be processed
     * @param queue the queue to add the objects to
     * @param queryObject the query object used for computing distances
     */
    protected void processLeafNodeIncKNN(LeafNode node, SearchQueue queue, LocalAbstractObject queryObject) {
        AbstractObjectIterator<LocalAbstractObject> objectIt = node.getObjects();
        while (objectIt.hasNext()) {
            LocalAbstractObject object = objectIt.next();
            queue.add(object, queryObject.getDistance(object));
        }
    }

    /**
     * Process an internal node from M-Tree queue. The queue is updated with
     * node's internal entries that can't be filtered out.
     *
     * @param node the internal node to be processed
     * @param queue the queue into which to add nodes
     * @param pd precomputed distances to specific objects
     * @param queryObject the query object used for filtering
     * @param radius the radius used for filtering
     */
    protected void processNode(InternalNode node, SearchQueue queue, Map<LocalAbstractObject, Float> pd, LocalAbstractObject queryObject, float radius) {
        for (NodeEntry ne : node.getNodeEntryList()) {
            NodeEntry pne = ne.getNode().getParentNodeEntry();
            float qParentDist = (pne == null) ? LocalAbstractObject.UNKNOWN_DISTANCE : pd.get(pne.getObject());
            float oParentDist = ne.getParentDist();
            
            // The subtree bounded by node entry ne can be filtered.
            if (!Search.filterNodeEntry(ne, oParentDist, queryObject, qParentDist, radius)) {
                float qPivotDist = (oParentDist == 0f && qParentDist != LocalAbstractObject.UNKNOWN_DISTANCE) 
                                        ? qParentDist : queryObject.getDistance(ne.getObject());
                pd.put(ne.getObject(), qPivotDist);
                if (qPivotDist <= ne.getRadius() + radius) {
                    // Regular M-tree
                    float lb = qPivotDist - ne.getRadius();     // max(lb,0) is applied in queue comparator.
                    float ub = qPivotDist + ne.getRadius();
                    
                    // State tighter lower and upper bounds for PM-tree (LB is max, UB is not min - it is much more complex (intersection of rings must be identified))
                    if (nhr > 0) { 
                        float[] hrQ = queryObject.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
                        NodeEntry.HREntry[] hr = ne.getHR();
                        for (int i = 0; i < hr.length; i++) {
                            float lbI, ubI = hrQ[i] + hr[i].getMax();
                            if (hr[i].getMin() <= hrQ[i] && hrQ[i] <= hr[i].getMax()) {
                                lbI = 0;
                                //ubI = hrQ[i] + hr[i].getMax(); //nonsense: Math.max(hrQ[i] - hr[i].getMin(), hr[i].getMax() - distQ[i]);
                            } else if (hrQ[i] < hr[i].getMin()) {     // Q is closet than the ring
                                lbI = hr[i].getMin() - hrQ[i];
                                //ubI = hrQ[i] + hr[i].getMax(); //nonsense: hr[i].getMax() - distQ[i];
                            } else {                                    // Q is farer than the ring
                                lbI = hrQ[i] - hr[i].getMax();
                                //ubI = hrQ[i] + hr[i].getMax(); //nonsense: distQ[i] - hr[i].getMin();
                            }
                            if (lbI > lb)
                                lb = lbI;
                            if (ubI < ub)
                                ub = ubI;
                        }
                    }
                    
                    queue.add(ne.getSubtree(), ne.getNode().getLevel(), lb, qPivotDist, ub);
                }
            }
        }
    }

    /**
     * Incremental nearest neighbor search.
     *
     * Incrementally returns the nearest objects to the given object. The
     * objects are returned in the operation's response list. This 
     * implementation respects
     * {@link messif.operations.query.IncrementalNNQueryOperation#minNN the minimum number of nearest neighbors}
     * specified in the operation.
     *
     * @param incOper Incremental nearest neighbor query operation which carries
     * the query object
     * @return returns <code>true</code>
     */
    public boolean incrementalNN(IncrementalNNQueryOperation incOper) {
        // Computes distances between the query object and all pivots.
        Search.setPrecompDistances(incOper.getQueryObject(), pivots);

        SearchQueue<Serializable> queue = incManager.get(incOper.getOperationID());

        // Add a new queue if this operation has just started
        if (queue == null) {
            queue = new SearchQueue<Serializable>();
            queue.add(root, root.getLevel(), 0f, 0f, 0f);
            incManager.put(incOper.getOperationID(), queue);
        }

        // Map of distances between query object and pivots in the tree.
        Map<LocalAbstractObject, Float> pd = new HashMap<LocalAbstractObject, Float>();

        // Process query
        synchronized (queue) {
            // Look for objects which are the nearest to object q.
            while (!queue.isEmpty() && !incOper.isFilledEnough()) {
                // Get queue head
                float headDistance = queue.getHeadLowerBound();
                Object object = queue.remove();

                // Decide what to do with the retrieved object
                if (object instanceof LocalAbstractObject) {
                    //FIXME: This is wrong, the search queue should maintain the additional distances as well
                    incOper.addToAnswer((LocalAbstractObject) object, headDistance, null);
                } else if (object instanceof LeafNode) {
                    processLeafNodeIncKNN((LeafNode) object, queue, incOper.getQueryObject());
                } else { // InternalNode
                    processNode((InternalNode) object, queue, pd, incOper.getQueryObject(), LocalAbstractObject.MAX_DISTANCE);
                }
            }

            // Set the error code of the operation and if all objects have been sent to the operation, remove the operation from incManager.
            if (queue.isEmpty()) {
                incManager.remove(incOper.getOperationID());
                incOper.endOperation(OperationErrorCode.RESPONSE_RETURNED);
            } else {
                incOper.endOperation(OperationErrorCode.HAS_NEXT);
            }
        }

        return true;
    }

    /**
     * Approximates the evaluation of k nearest neighbors query.
     *
     * Looks for the k-nearest objects to the given object. Note that the
     * searching is approximate therefor all the nearest objects needn't be
     * returned.
     *
     * The neighbors (objects) are returned in the response in the operation.
     *
     * @param kNNOper the operation which specifies the query object and the
     * number of neighbors to retrieve
     * @return returns <code>true</code>
     */
    public boolean approxKNN(ApproxKNNQueryOperation kNNOper) throws ClassNotFoundException, NoSuchInstantiatorException, InvocationTargetException {
        return kNNsearch(kNNOper, ApproxState.create(kNNOper, this));
//        // Computes distances between the query object and all pivots.
//        Search.setPrecompDistances(kNNOper.getQueryObject(), pivots);
//
//        // Number of inspected objects in leaves.
//        int objectCount = 0;
//
//        // Number of objects to be explored (i.e. the percentage of this tree objects specified in the operation)
//        int objectToExplore;
//        if (kNNOper.getLocalSearchType() == ApproxKNNQueryOperation.LocalSearchType.PERCENTAGE) {
//            objectToExplore = Math.round(this.getObjectCount() * (kNNOper.getLocalSearchParam() / 100f));
//        } else {
//            objectToExplore = kNNOper.getLocalSearchParam();
//        }
//
//        // Map of distances between query object and pivots in the tree.
//        Map<LocalAbstractObject, Float> pd = new HashMap<LocalAbstractObject, Float>();
//
//        // Sorted queue of the most promising subtrees.
//        SearchQueue<Node> queue = new SearchQueue<Node>();
//
//        // Initialize queue with root
//        queue.add(root, root.getLevel(), 0f, 0f, 0f);
//
//        // Looking for the most promising objects stored in the nearest leaves.
//        float radius = kNNOper.getAnswerThreshold();
//        while (!queue.isEmpty() && queue.getHeadLowerBound() <= radius) {
//            // Approximation stop condition
//            if (objectCount >= objectToExplore && (kNNOper.getRadiusGuaranteed() == LocalAbstractObject.UNKNOWN_DISTANCE || queue.getHeadLowerBound() > 0f)) {
//                break;
//            }
//            Node node = queue.remove();
//
//            if (!node.isLeaf()) {
//                processNode((InternalNode) node, queue, pd, kNNOper.getQueryObject(), radius);
//            } else {
//                LeafNode leafNode = (LeafNode) node;
//                processLeafNode(leafNode, pd, kNNOper);
//                objectCount += leafNode.getBucket().getObjectCount();
//                radius = kNNOper.getAnswerThreshold();
//            }
//        }
//
//        return true;
    }

    /** Executes KNN queries in parallel */
    public void operationExecuteKNNParallel(StreamGenericAbstractObjectIterator<LocalAbstractObject> queries, int cnt, int k, int threads) {
        if (threads < 2)
            throw new IllegalArgumentException("At least 2 threads for concurrent execution must be used.");
        
        Statistics.disableGlobally();
        ExecutorService pool = Executors.newFixedThreadPool(threads);
        Queue<Future<KNNQueryOperation>> running = new ArrayDeque<>(2 * threads);
        while (queries.hasNext() && cnt > 0) {
            if (running.size() < 2*threads) {
                cnt--;
                running.add(pool.submit(new KnnWorker(queries.next(), k)));
            } else {
                if (!processFinished(running));
                    try { Thread.sleep(100); } catch (InterruptedException ex) {}
            }
        }
        pool.shutdown();
        while (!running.isEmpty()) {
            if (!processFinished(running))
                try { pool.awaitTermination(1, TimeUnit.SECONDS); } catch (InterruptedException ex) { }
        }
        Statistics.enableGlobally();
    }
    
    private boolean processFinished(Queue<Future<KNNQueryOperation>> running) {
        final Future<KNNQueryOperation> status = running.peek();
        if (status == null || !status.isDone())
            return false;       // Nothing has finished.
        final Future<KNNQueryOperation> toProcess = running.poll();
        if (toProcess != status)
            throw new RuntimeException("Head of process queue has changed!?!?");       // Eh????
        // Show operation result
        final KNNQueryOperation oper;
        try {
            oper = toProcess.get();
            System.out.println(oper);
            System.out.print(OperationStatistics.getLocalThreadStatistics().printStatistics("Accessed.*|Answer.*|Node.Leaf.Visited|DistanceComputations.*|BlockReads|OperationTime"));
            // Display output
            Iterator<?> answerIterator = oper.getAnswer();
            while (answerIterator.hasNext())
                System.out.println(answerIterator.next());
            return true;
        } catch (InterruptedException ex) {
            Logger.getLogger(MTree.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(MTree.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private class KnnWorker implements Callable<KNNQueryOperation> {
        private KNNQueryOperation oper;
        
        public KnnWorker(LocalAbstractObject queryObj, int k) {
            oper = new KNNQueryOperation(queryObj, k, AnswerType.ORIGINAL_OBJECTS);
        }
        
        @Override
        public KNNQueryOperation call() throws Exception {
            MTree.this.executeOperation(oper);
            return oper;
        }        
    }
    
    /**
     * Inserts a new object into the tree. A leaf into which the new object is
     * inserted is chosen by inserting singleway or multiway algorithm (it
     * depends on multiwayInsertion variable).
     *
     * @return <tt>false</tt> if the size of inserting object is greater than
     * the capacity of an internal node or a leaf
     */
    public boolean insert(InsertOperation insOper) {
        // Call the internal addObject method -- it has to be there because of the BucketInterface.
        BucketErrorCode errCode = addObject(insOper.getInsertedObject());
        insOper.endOperation(errCode);
        return (BucketErrorCode.OBJECT_INSERTED.equals(errCode));
    }

    /**
     * Inserts new objects by sequential inserting individual objects.
     */
    public void insert(BulkInsertOperation insOper) {
        BucketErrorCode err = BucketErrorCode.OBJECT_INSERTED;
        for (LocalAbstractObject o : insOper.getInsertedObjects()) {
            BucketErrorCode res = addObject(o);
            if (res != BucketErrorCode.OBJECT_INSERTED)
                err = res;
        }
        insOper.endOperation(err);
    }
    
    /**
     * Creates M-tree from bottom up with the passed partitions as individual buckets.
     * @param insOper partitined data
     */
    public void insertWithGivenPartitioning(PartitionedBulkInsertOperation insOper) {
        if (getObjectCount() != 0) {
            insOper.endOperation(new OperationErrorCode("M-tree must be empty!"));
            return;
        }

        // Does not (fully) support PM-tree yet!!!!
        
        try {
            // Remove old root and set the new one
            bucketDispatcher.removeBucket(((LeafNode)root).getBucket().getBucketID(), true);
            root = new InternalNode(1, null, intNodeCap);

            // Create all leaf nodes
            for (int partId = 0; partId < insOper.getArgumentCount(); partId++) {
                final List<LocalAbstractObject> lst = insOper.getInsertedObjects(partId);
                mtree.utils.Search.setPrecompDistances(lst, pivots, pivots.length);//, nhr);        // Init just HR external pivots?

                TreeSet<Split.Edge> edgeSet = createEdgeSet(lst.size(), lst);
                Map<LocalAbstractObject, List<Split.Edge>> objectMap = createObjectMap(lst.size(), lst);
                for (Iterator<Split.Edge> it = edgeSet.iterator(); it.hasNext();) {
                    Split.Edge e = it.next();
                    objectMap.get(e.getObject1()).add(e);
                    objectMap.get(e.getObject2()).add(e);
                }
                edgeSet = null;     // free mem

                // Looking for leaf's pivots and setting covering radius. Filling of distances
                // between pivots and other objects.
                LocalAbstractObject pvt = Split.findPivot(objectMap, lst);
                pvt.getDistanceFilter(ParentFilter.class).setParentDist(0f);
                float rad = Split.setParentDistances(pvt, lst);

                // Copy the pivot and use it in the parent node
                pvt = pvt.clone();
                pvt.getDistanceFilter(ParentFilter.class).setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
                NodeEntry ne = ((InternalNode)root).createNodeEntry(pvt, null, nhr);
                LeafNode leaf = new LeafNode(ne, bucketDispatcher.createBucket());
                ne.setSubtree(leaf);
                ne.setRadius(rad);
                leaf.getBucket().addObjects(lst);            
            }

            // Split root if overflowing
            if (((InternalNode)root).isCapacityExceeded()) {
                splitNode_SlimDown(root);
            }

            insOper.endOperation();

        } catch (CloneNotSupportedException e) {
            insOper.endOperation(BucketErrorCode.OBJECT_REFUSED);
        } catch (BucketStorageException ex) {
            insOper.endOperation(BucketErrorCode.OBJECT_REFUSED);
        }        
    }
    
    public void markObjectsWithBucketIds() {
        List<LeafNode> lvs = getAllLeaves();
        for (LeafNode lv : lvs) {
            int id = lv.getBucket().getBucketID();
            for (AbstractObjectIterator<LocalAbstractObject> iterator = lv.getBucket().getAllObjects(); iterator.hasNext();) {
                LocalAbstractObject next = iterator.next();
                next.setObjectKey(new BucketIdObjectKey(next.getLocatorURI(), id));
            }
        }
    }
    
    /** Check consistency of (Pivoting) M-tree.
     * Checks performed:
     * 1/ PM-tree - length of pivot arrays must equal to npd and nhr, respectively.
     * 2/ M-tree - covering radii
     * @return <code>true</code> if no problem found; otherwise false and the problems are printed to {@link System#err}.
     */
    public boolean checkConsistency() {
        try {
            boolean ok = checkConsistencyNode(root);
            System.err.println("Mtree.checkConsistencyNode: " + (ok ? "passed" : "FAILED"));
            return ok;
        } catch (CloneNotSupportedException ex) {
            System.err.println("Mtree.checkConsistencyNode: FAILED - " + ex.getMessage());
            ex.printStackTrace(System.err);
            return false;
        }
    }
    
    private boolean checkConsistencyNode(Node n) throws CloneNotSupportedException {
        boolean result = true;
        if (n instanceof LeafNode) {
            boolean isPMtree = false;
            LeafNode leaf = (LeafNode)n;
            final NodeEntry parent = leaf.getParentNodeEntry();
            AbstractObjectIterator<LocalAbstractObject> it = leaf.getObjects();
            while (it.hasNext()) {
                final LocalAbstractObject o = it.next();
                final LocalAbstractObject oNoFilters = o.clone(false);
                
                float distToParent = o.getDistanceFilter(ParentFilter.class).getParentDist();
                float distToParentReal = (parent != null) ? oNoFilters.getDistance(parent.getObject()) : distToParent;
                if (distToParentReal != distToParent) {
                    System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                            + " has object id #" + o.getLocatorURI() + " whose stored parent dist " + distToParent
                            + " is diffrent from the real distsance " + distToParentReal);
                    result = false;
                }
                if (parent != null && distToParent > parent.getRadius()) {
                    System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                            + " has object id #" + o.getLocatorURI() + " whose parent dist " + distToParent
                            + " is larger than covering radiues " + parent.getRadius());
                    result = false;
                }

                PrecomputedDistancesFixedArrayFilter flt = o.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class);
                if (flt != null) {      // PM-tree
                    isPMtree = true;
                    if (flt.getPrecompDistSize() != npd) {
                        System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                                + " has " + flt.getPrecompDistSize() + " precomputed distances! Expected: " + npd);
                        result = false;
                    }
                }
            }
            // Check NPD of PM-tree
            for (int i = 0; i < npd; i++) {
                float min = Float.MAX_VALUE, max = Float.MIN_VALUE;
                
                it = leaf.getObjects();
                while (it.hasNext()) {
                    final LocalAbstractObject o = it.next();
                    final LocalAbstractObject oNoFilters = o.clone(false);
                    PrecomputedDistancesFixedArrayFilter flt = o.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class);
                    if (flt == null) {
                        System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                                + " has an object " + o.getLocatorURI() + " that does not have precomputed distances to pivots!");
                        result = false;
                        continue;
                    }
                    float distOP = flt.getPrecompDist(i);
                    float distOPreal = oNoFilters.getDistance(pivots[i]);
                    if (distOP != distOPreal) {
                        System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                                + " has an object " + o.getLocatorURI() + " that has different precomputed distances to"
                                + " the pivot at index " + i + ": " + distOP + " Expected dist: " + distOPreal);
                        result = false;
                    }
                    if (distOPreal < min)
                        min = distOPreal;
                    if (distOPreal > max)
                        max = distOPreal;
                }
                
                if (parent != null) {
                    NodeEntry.HREntry[] parentRings = parent.getHR();
                    if (i < nhr) {
                        if (parentRings[i].getMin() != min) {
                            System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                                    + " has min precomputed distance to the pivot at index " + i + ": " + parentRings[i].getMin() + " Expected dist: " + min);
                            result = false;
                        }
                        if (parentRings[i].getMax() != max) {
                            System.err.println("Mtree.checkConsistencyNode: Leaf with bucket id #" + leaf.getBucket().getBucketID()
                                    + " has max precomputed distance to the pivot at index " + i + ": " + parentRings[i].getMax() + " Expected dist: " + max);
                            result = false;
                        }
                    }
                }
            }
            
        } else {
            // Internal node
            InternalNode node = (InternalNode)n;
            final NodeEntry parent = node.getParentNodeEntry();
            for (int i = 0; i < node.getNodeEntryList().size(); i++) {
                NodeEntry e = node.getNodeEntry(i);
                if (e.getHR() != null && e.getHR().length != nhr) {
                    System.err.println("Mtree.checkConsistencyNode: Internal node on level " + n.getLevel() + ", entry index " + i
                            + " has " + e.getHR().length + " precomputed distances! Expected: " + nhr);
                    result = false;
                }
                if (parent != null) {
                    float distToParent = e.getParentDist();
                    float distToParentReal = e.getObject().clone(false).getDistance(parent.getObject());
                    if (distToParentReal != distToParent) {
                        System.err.println("Mtree.checkConsistencyNode: " + node.toString()
                                + " has an entry at index " + i + " whose stored parent dist " + distToParent
                                + " is diffrent from the real distsance " + distToParentReal);
                        result = false;
                    }
                    if (distToParent + e.getRadius() > parent.getRadius()) {
                        System.err.println("Mtree.checkConsistencyNode: " + node.toString()
                                + " has an entry at index " + i + " whose parent dist " + distToParent
                                + " plus radius " + e.getRadius() + " is larger than covering radiues " + parent.getRadius());
                        result = false;
                    }
                    
                }
            }
            // Check all children's HR array to be covered by this node.
            if (parent != null) {
                final NodeEntry.HREntry[] hrsParent = parent.getHR();
                for (int i = 0; i < nhr; i++) {
                    float min = Float.MAX_VALUE, max = Float.MIN_VALUE;
                    for (int chld = 0; chld < node.getNodeEntryList().size(); chld++) {
                        NodeEntry e = node.getNodeEntry(chld);
                        final NodeEntry.HREntry[] hrs = e.getHR();
                        if (hrsParent[i].getMin() > hrs[i].getMin()) {
                            System.err.println("Mtree.checkConsistencyNode: Node with pivot " + e.getObject().getLocatorURI()
                                    + " has min precomputed distance to the pivot at index " + i + ": " + hrs[i].getMin() + ". But it is smaller than the parent's min: " + hrsParent[i].getMin());
                            result = false;
                        }
                        if (hrsParent[i].getMax() < hrs[i].getMax()) {
                            System.err.println("Mtree.checkConsistencyNode: Node with pivot " + e.getObject().getLocatorURI()
                                    + " has max precomputed distance to the pivot at index " + i + ": " + hrs[i].getMax() + ". But it is greater than the parent's max: " + hrsParent[i].getMax());
                            result = false;
                        }
                        if (min > hrs[i].getMin())
                            min = hrs[i].getMin();
                        if (max < hrs[i].getMax())
                            max = hrs[i].getMax();
                    }
                    if (hrsParent[i].getMin() != min) {
                        System.err.println("Mtree.checkConsistencyNode: Node with pivot " + parent.getObject().getLocatorURI()
                                + " has min precomputed distance to the pivot at index " + i + ": " + hrsParent[i].getMin() + ". But the minimum from its children is: " + min);
                        result = false;
                    }
                    if (hrsParent[i].getMax() != max) {
                        System.err.println("Mtree.checkConsistencyNode: Node with pivot " + parent.getObject().getLocatorURI()
                                + " has max precomputed distance to the pivot at index " + i + ": " + hrsParent[i].getMax() + ". But the maixmum from its children is: " + max);
                        result = false;
                    }
                }
            }
            
            // Recurse to all children
            for (int i = 0; i < node.getNodeEntryList().size(); i++) {
                NodeEntry e = node.getNodeEntry(i);
                boolean res = checkConsistencyNode(e.getSubtree());     // separate lines are necessary to do recursion even if result is false.
                result = result && res;
            }
        }
        return result;
    }
    
    /** Result of ground truth queries */
    private transient Map<String, List<RankedAbstractObject>> groundTruth;
    
    public void prepareGroundTruth(int maxK, LocalAbstractObject query) {
        if (groundTruth == null)
            groundTruth = Collections.synchronizedMap(new HashMap<>());
        
        KNNQueryOperation op = new KNNQueryOperation(query, maxK, AnswerType.ORIGINAL_OBJECTS);

        //  alg.executeOperation(op);
        // Rather use sequential scan
        for (LocalBucket bucket : getAllBuckets())
            bucket.processQuery(op);
        // Store the answer
        List<RankedAbstractObject> ans = new ArrayList<>(op.getAnswerCount());
        final Iterator<RankedAbstractObject> ansIt = op.getAnswer();
        while (ansIt.hasNext())
            ans.add(ansIt.next());
        groundTruth.put(op.getQueryObject().getLocatorURI(), ans);
    }
    
    public void prepareGroundTruthInBatch(int maxK, StreamGenericAbstractObjectIterator<LocalAbstractObject> queries, int queryCnt) {
        if (groundTruth == null)
            groundTruth = Collections.synchronizedMap(new HashMap<>());
        
        List<KNNQueryOperation> kNNOperations = new ArrayList<>();
        while (queries.hasNext() && queryCnt-- > 0) {
            kNNOperations.add(new KNNQueryOperation(queries.next(), maxK, AnswerType.ORIGINAL_OBJECTS));
        }
        
        kNNOperations.parallelStream()
                .forEach((KNNQueryOperation op) -> {
                    //  alg.executeOperation(op);
                    // Rather use sequential scan
                    for (LocalBucket bucket : getAllBuckets())
                        bucket.processQuery(op);
                    // Store the answer
                    List<RankedAbstractObject> ans = new ArrayList<>(op.getAnswerCount());
                    final Iterator<RankedAbstractObject> ansIt = op.getAnswer();
                    while (ansIt.hasNext())
                        ans.add(ansIt.next());
                    groundTruth.put(op.getQueryObject().getLocatorURI(), ans);
                });
    }
    
    private void checkGroundTruth(KNNQueryOperation kNNOper) {
        if (groundTruth == null)
            return;
        List<RankedAbstractObject> gt = groundTruth.get(kNNOper.getQueryObject().getLocatorURI());
        if (gt == null)
            return;
        
        final Iterator<RankedAbstractObject> it = kNNOper.getAnswer();
        StringBuilder sb = new StringBuilder();
        int i = 0, iGT = 0, iGTmatch = 0, matches = 0;
        RankedAbstractObject oGT = null;
        WHILEANSWER:
        while (it.hasNext()) {
            final RankedAbstractObject o = it.next();
            i++;
            
            if (iGT >= kNNOper.getAnswerCount()) {        // No more GT objects, report all non matching...
                if (sb.length() > 0)
                    sb.append(", ");
                sb.append("ans[").append(i).append(":").append(o.getObject().getLocatorURI()).append(",").append(o.getDistance());
                if (oGT == null)
                    sb.append("]-gt[]");
                else
                    sb.append("]>gt[").append(iGT).append(":").append(oGT.getObject().getLocatorURI()).append(",").append(oGT.getDistance()).append("]");
                oGT = null;     // Report GT object once at maximum
                continue;
            }
            
            // GT is always better, so try to find o in GT
            for (; iGT < gt.size(); iGT++) {
                oGT = gt.get(iGT);
                if (o.getDistance() == oGT.getDistance()) {
                    matches++;
                    if (iGTmatch != iGT) {
                        if (sb.length() > 0)
                            sb.append(", ");
                        sb.append("ans[").append(i).append(":").append(o.getObject().getLocatorURI()).append(",").append(o.getDistance())
                                .append("]=gt[").append(iGT+1).append(":").append(oGT.getObject().getLocatorURI()).append(",").append(oGT.getDistance()).append("]");
                    }
                    iGTmatch = ++iGT;   // Move to next on match and remember it
                    oGT = null;     // Report GT object once at maximum
                    continue WHILEANSWER;  // OK, continue in checking next object in answer
                }
            }
            if (iGT >= gt.size()) {  // Not a single GT object matches
                if (sb.length() > 0)
                    sb.append(", ");
                sb.append("ans[").append(i).append(":").append(o.getObject().getLocatorURI()).append(",").append(o.getDistance());
                if (oGT == null)
                    sb.append("]-gt[]");
                else
                    sb.append("]>gt[").append(iGT).append(":").append(oGT.getObject().getLocatorURI()).append(",").append(oGT.getDistance()).append("]");
            }
        }
        System.err.print("Mtree.checkGroundTruth: kNN(" + kNNOper.getQueryObject().getLocatorURI() + "," + kNNOper.getK() + "): ");
        System.err.print("Recall " + ((float)matches / (float)kNNOper.getAnswerCount()));
        if (sb.length() > 0) {
            System.err.print(" ");
            System.err.println(sb);
        } else {
            System.err.println();
        }
    }
    
    
    /**
     * Second phaze of testing while gathering usefulness
     *   FALSE - DEFAULT; increases usefulness during queries; uses compareToByDistance
     *   TRUE - does not increase usefullnes during queries; uses compareToByUsefulness
     * @param testModeOn
     */
    public void setTestMode(boolean testModeOn) {
        testMode = testModeOn;
    }

    /**
     * Select objects from each bucket of this M-tree at random.
     * 
     * Two independent data-sets can be created, i.e. without any overlap between them.
     * @param learnData file to write all selected object as string
     * @param learnCnt number of objects to take from each bucket
     * @param testData file to write all selected object as string (may be null)
     * @param testCnt number of objects to take from each bucket (may be zero)
     * @throws java.io.IOException output files cannot be opened
     */
    public void selectRandomFromLeaves(String learnData, int learnCnt, String testData, int testCnt) throws IOException, CloneNotSupportedException {
        FileOutputStream fileOutLearn = new FileOutputStream(learnData);
        FileOutputStream fileOutTest = (testData != null) ? new FileOutputStream(testData) : null;

        log.log(Level.INFO, "Getting random objects from leaves. Learn {0} - {1}, test {2} - {3}", new Object[]{learnData, learnCnt, testData, testCnt});
        
        List<LeafNode> lvs = getAllLeaves();
        for (LeafNode lv : lvs) {
            log.log(Level.INFO, "Processing bucket {0}, occupation {1}", new Object[] {lv.getBucket().getBucketID(), lv.getBucket().getObjectCount()});
            AbstractObjectList<LocalAbstractObject> objs = lv.getBucket().getAllObjects().getRandomObjects(learnCnt + testCnt, true);
            int toLearn = learnCnt;
            FileOutputStream out = fileOutLearn;
            for (LocalAbstractObject obj : objs) {
                if (obj == null)
                    break;
                obj.clone(false).write(out);
                --toLearn;
                if (toLearn == 0) {
                    toLearn = testCnt;
                    out = fileOutTest;
                }
            }
        }
        fileOutLearn.close();
        if (fileOutTest != null)
            fileOutTest.close();
    }
    
    /**
     * **************** Implemented interface SplittableAlgorithm
     * ****************
     */
    /**
     * Splits this tree into two trees according to the given split node policy.
     *
     * @param whoStays identification of a partition whose objects stay in this
     * bucket.
     */
    public void split(SplitPolicy sp, SplittableAlgorithmResult result, int whoStays) throws BucketStorageException {

        // Setting unknown parent distance.
        Split.setParentDistances(sp, LocalAbstractObject.UNKNOWN_DISTANCE);

        // Indicates whether the objects in slim nodes should be reinserted into
        // the tree after splitting the tree.
        boolean reinsertSlimNodesObjects = true;

        // Creating a new tree with the same parameters as the current tree.
        MTree mtree1 = this;
        MTree mtree2 = mtree1.cloneWithoutObjects();

        // All internal nodes which descendants are leaves.
        List<InternalNode> nodes = Search.findLastInternalNodes(mtree1.getRoot());

        // Determining and computing the missing parameters of the split node policy.
        Split.completeSplitPolicy(sp, mtree1);

        // If the tree has only one node (a leaf node) then only the precomputed
        // distances to pivots can be used for filtering.
        if (mtree1.getRoot().isLeaf()) {
            AbstractObjectIterator<LocalAbstractObject> it = mtree1.getRoot().getObjects();
            while (it.hasNext()) {
                LocalAbstractObject o = it.next();
                if (sp.match(o) != whoStays) {
                    try {
                        result.markMovedObject(mtree2, o);
                    } catch (InstantiationException e) {
                        throw new CapacityFullException(e.getMessage());
                    }
                    it.remove();
                    mtree2.addObject(o);
                }
            }
        } else {
            InternalNode n1 = new InternalNode(1, null, intNodeCap);
            InternalNode n2 = new InternalNode(1, null, intNodeCap);
            Set<LocalAbstractObject> slimNodesObjects1 = new HashSet<LocalAbstractObject>();
            Set<LocalAbstractObject> slimNodesObjects2 = new HashSet<LocalAbstractObject>();

            // Filtering particular subtrees.
            for (InternalNode n : nodes) {
                for (NodeEntry ne : n.getNodeEntryList()) {

                    // Filtering the whole subtree.
                    BallRegion br = new BallRegion(ne.getObject(), ne.getRadius());
                    int matchBallRegion = sp.match(br);
                    if (matchBallRegion != -1) {
                        if (matchBallRegion == whoStays) {
                            n1.createNodeEntry(ne);
                        } else {
                            n2.createNodeEntry(ne);
                        }
                    } else {

                        // Filtering particular objects.
                        Split.setParentDistances(sp);
                        Set<LocalAbstractObject> objectSet1 = new HashSet<LocalAbstractObject>();
                        Set<LocalAbstractObject> objectSet2 = new HashSet<LocalAbstractObject>();
                        AbstractObjectIterator<LocalAbstractObject> it = ne.getSubtree().getObjects();
                        while (it.hasNext()) {
                            LocalAbstractObject o = it.next();
                            if (sp.match(o) == whoStays) {
                                objectSet1.add(o);
                            } else {
                                objectSet2.add(o);
                            }
                        }
                        try {

                            // Creating leaves which have to contain two objects at least
                            // in the case of reinserting objects from slim nodes.
                            if (reinsertSlimNodesObjects && objectSet1.size() <= 1) {
                                slimNodesObjects1.addAll(objectSet1);
                            } else {
                                try {
                                    NodeEntry ne1 = n1.createNodeEntry(ne.getObject().clone(true), null, nhr);
                                    LeafNode leaf1 = new LeafNode(ne1, bucketDispatcher.createBucket());
                                    ne1.setSubtree(leaf1);
                                    leaf1.getBucket().addObjects(objectSet1.iterator());
                                    leaf1.updateDistances();
                                } catch (CloneNotSupportedException e) {
                                    throw new CapacityFullException("Can't clone object while splitting: " + e.getMessage());
                                }
                            }
                            if (reinsertSlimNodesObjects && objectSet2.size() <= 1) {
                                slimNodesObjects2.addAll(objectSet2);
                            } else {
                                try {
                                    NodeEntry ne2 = n2.createNodeEntry(ne.getObject().clone(true), null, nhr);
                                    LeafNode leaf2 = new LeafNode(ne2, bucketDispatcher.createBucket());
                                    ne2.setSubtree(leaf2);
                                    leaf2.getBucket().addObjects(objectSet2.iterator());
                                    leaf2.updateDistances();
                                } catch (CloneNotSupportedException e) {
                                    throw new CapacityFullException("Can't clone object while splitting: " + e.getMessage());
                                }
                            }
                        } catch (CapacityFullException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }

            // Computing moved objects and bytes.
            for (NodeEntry ne : n2.getNodeEntryList()) {
                AbstractObjectIterator<LocalAbstractObject> it = ne.getSubtree().getObjects();
                while (it.hasNext()) {
                    try {
                        result.markMovedObject(mtree2, it.next());
                    } catch (InstantiationException e) {
                        throw new CapacityFullException(e.getMessage());
                    }
                }
            }

            // Setting new roots of the trees.
            if (n2.getObjectCount() > 0) {
                mtree2.setRoot(n2, bucketDispatcher);
            }
            if (n1.getObjectCount() > 0) {
                mtree1.setRoot(n1, bucketDispatcher);
            } else {
                mtree1 = mtree1.cloneWithoutObjects();
            }

            // Splitting a node with the exceeded capacity.
            if (n1.isCapacityExceeded()) {
                try {
                    mtree1.splitNode(n1);
                } catch (CloneNotSupportedException e) {
                    throw new CapacityFullException("Can't clone object while splitting: " + e.getMessage());
                }
            }
            if (n2.isCapacityExceeded()) {
                try {
                    mtree2.splitNode(n2);
                } catch (CloneNotSupportedException e) {
                    throw new CapacityFullException("Can't clone object while splitting: " + e.getMessage());
                }
            }

            // Reinserting objects from slim nodes.
            if (reinsertSlimNodesObjects) {
                for (LocalAbstractObject o : slimNodesObjects1) {
                    mtree1.addObject(o);
                }
                for (LocalAbstractObject o : slimNodesObjects2) {
                    try {
                        result.markMovedObject(mtree2, o);
                        mtree2.addObject(o);
                    } catch (InstantiationException e) {
                        throw new CapacityFullException(e.getMessage());
                    }
                }
            }
        }
    }

    /**
     * **************** Implemented interface BucketInterface ****************
     */
    /**
     * Inserts a new object into the tree. A leaf into which the new object is
     * inserted is chosen by the single-way or multi-way insertion algorithm.
     * The algo selection depends on the multiwayInsertion variable.
     *
     * @return bucket error code after inserting the object
     */
    public BucketErrorCode addObject(LocalAbstractObject object) {
        mtree.utils.Search.setPrecompDistances(object, pivots);//, nhr);        // Init just HR external pivots
//        if (object.getSize() > Math.min(intNodeCap, leafCap)) {
//            return BucketErrorCode.OBJECT_REFUSED;
//        }

        // Best leaf for inserting new object q.
        LeafNode leaf = null;

        // Using singleway or multiway insertion algorithm.
        if (!multiwayInsertion) {
            leaf = Insert.findNearestLeafList(object, root);    // parent filter is set inside
        } else {

            // Looking for a best leaf for insertion of the object q by multiway
            // insertion algorithm.
            List<LeafNode> leaves = Search.findLeavesByQuery(root, object, insRadius);
            if (leaves.isEmpty()) {
                leaf = Insert.findNearestLeafList(object, root);
            } else {
                float min = Float.MAX_VALUE;
                for (LeafNode n : leaves) {
                    float d = object.getDistance(n.getParentNodeEntry().getObject());
                    if (d < min) {
                        leaf = n;
                        min = d;
                    }
                }
                object.getDistanceFilter(ParentFilter.class).setParentDist(min);
            }
        }

        boolean overflowing = false;
        PrecomputedDistancesFixedArrayFilter hrDists = null;
        try {
            LocalBucket bucket = leaf.getBucket();
            hrDists = mtree.utils.Search.clearPrecompDistances(object, npd);        // Make sure just PD external pivots are initialized (if ndp < nhr)
            bucket.addObject(object);
            overflowing = bucket.isSoftCapacityExceeded();
        } catch (BucketStorageException ex) {
            ex.printStackTrace();
        }

        // If the capacity of the leaf is exceeded after inserting new object, then the leaf has to be split.
        if (overflowing) {
            try {
                splitNode(leaf);//, hrDists);
            } catch (CloneNotSupportedException e) {
                return BucketErrorCode.OBJECT_REFUSED;
            }
        } else {
            // Adjustment of the radius and distances of HR array.
            leaf.updateDistances(Math.max(object.getDistanceFilter(ParentFilter.class).getParentDist(), leaf.getParentRadius()), hrDists);
        }

        return BucketErrorCode.OBJECT_INSERTED;
    }

    /**
     * Returns the number of objects stored in the tree.
     *
     * @return the number of objects stored in the tree
     */
    public int getObjectCount() {
        int objectCount = 0;
        for (LocalBucket bucket : this.getAllBuckets()) {
            objectCount += bucket.getObjectCount();
        }
        return objectCount;
    }

    /**
     * Looks for all objects stored in the tree.
     *
     * @return object iterator of all objects stored in the tree
     */
    public AbstractObjectIterator<LocalAbstractObject> getAllObjects() {
        AbstractObjectList<LocalAbstractObject> retVal = new AbstractObjectList<LocalAbstractObject>();
        for (LocalBucket bucket : getAllBuckets()) {
            retVal.addAll(bucket.getAllObjects());
        }
        return retVal.iterator();
    }

    /**
     * **************** Clonning *****************
     */
    /**
     * Clones the tree (along with all stored objects) exloiting the process of
     * serialization.
     */
    @Override
    public MTree clone() {
        String fileName = "M-tree_clone";
        this.saveToFile(fileName);
        return MTree.loadFromFile(fileName);
    }

    /**
     * **************** Serialization *****************
     */
    /**
     * Saves the tree into a file specified by filename.
     *
     * @param fileName the file name into which the tree is saved
     */
    public void saveToFile(String fileName) {
        try {
            OutputStream outfile = new FileOutputStream(fileName);
            ObjectOutputStream outstream = new ObjectOutputStream(outfile);
            outstream.writeObject(this);
            outstream.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Loads the M-tree from a file specified by filename.
     *
     * @param fileName the file name from which the tree is loaded
     * @return the loaded M-tree from file specified by filename
     */
    public static MTree loadFromFile(String fileName) {
        try {
            InputStream infile = new FileInputStream(fileName);
            ObjectInputStream instream = new ObjectInputStream(infile);
            return (MTree) instream.readObject();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * **************** Statistics *****************
     */
    /**
     * Prints the statistics of the tree (the number of internal nodes, the
     * number of leaves, the number of all objects and the number of slim nodes
     * - nodes with only one stored object).
     */
    public void printStatistics() {
        Map<String, Integer> stat = new HashMap<>();
        stat.put("Internal nodes", 0);
        stat.put("Slim internal nodes", 0);
        stat.put("Leaves", 0);
        stat.put("Slim leaves", 0);
        stat.put("Object count", 0);
        stat.put("Pivots", 0);
        this.gatherStatistics(root, stat);

        // Computing filling of leaves.
        int min = Integer.MAX_VALUE;
        int max = 0;
        int sum  = 0;
        List<LeafNode> leaves = getAllLeaves();
        Map<Integer,Integer> leavesOccupationByBycketId = new TreeMap<>();
        for (LeafNode leaf : leaves) {
            int cnt = leaf.getObjectCount();
            leavesOccupationByBycketId.put(leaf.getBucket().getBucketID(), cnt);
            min = Math.min(min, cnt);
            max = Math.max(max, cnt);
            sum  += cnt;
        }

        // Printing statistics
        System.out.println("M-tree statistics:");
        for (String key : stat.keySet()) {
            System.out.println("  " + key + ": " + stat.get(key));
        }
        System.out.println("  Node capacity: " + intNodeCap + ", Leaf capacity: " + leafCap + ", Height: " + (root.getLevel()+1));
        System.out.println("  PM-tree Pivot count: " + pivots.length + " - nhr=" + nhr + ", npd=" + npd);
        System.out.println("  Split done using " + ((maxSpanningTree < 2) ? "all" : String.valueOf(maxSpanningTree)) + " entries.");
        System.out.println("  Filling of leaves (min: " + min + " , max: " + max + ", avg: " + (float) sum  / (float) leaves.size() + "):");
        System.out.print("Unsorted leaf-node occupations (bucket ID = occupation): ["); 
        for (Map.Entry<Integer, Integer> e : leavesOccupationByBycketId.entrySet()) {
            System.out.print(e.getKey());
            System.out.print("=");
            System.out.print(e.getValue());
            System.out.print(", ");
        }
        System.out.println("]");

        Integer[] sorted = new Integer[leaves.size()];
        Arrays.sort(leavesOccupationByBycketId.values().toArray(sorted), new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.print("Sorted leaf-node occupations: "); System.out.print(Arrays.toString(sorted)); System.out.println();
        
        Map<Integer,List<Node>> nodesByLevel = new TreeMap<>();
        getNodesByLevel(nodesByLevel);
        StringBuilder usefulness = new StringBuilder("Usefulness of nodes per level (nodeID[-BucketId]=usefulness):");  // Usability of nodes...
        StringBuilder occupations = new StringBuilder("Node occupation per level (nodeID=usefulness):");
        int nodeId = 1;
        for (Map.Entry<Integer, List<Node>> e : nodesByLevel.entrySet()) {
            Integer key = e.getKey();
            List<Node> lst = e.getValue();
            usefulness.append("\n     Level ").append(key).append(" nodes: [");
            occupations.append("\n     Level ").append(key).append(" nodes: [");
            for (Node n : lst) {
                usefulness.append(nodeId);
                occupations.append(nodeId);
                if (n.isLeaf()) {
                    usefulness.append("-B").append(((LeafNode)n).getBucket().getBucketID());
                }
                usefulness.append('=').append(n.getNodeUsefulness()).append(", ");
                occupations.append('=').append(n.getObjectCount()).append(", ");
                nodeId++;
            }
            usefulness.append(']');
            occupations.append(']');
        }
        
        System.out.println(occupations.toString());
        System.out.println(usefulness.toString());
        
        // Covering radius for all leaf nodes...
        Map<Integer,Float> leavesCoveringRadii = new TreeMap<>();
        for (LeafNode leaf : leaves) {
            leavesCoveringRadii.put(leaf.getBucket().getBucketID(), leaf.getParentRadius());
        }
        System.out.print("Covering radii of leaf nodes (bucketID=radius): ["); 
        for (Map.Entry<Integer, Float> e : leavesCoveringRadii.entrySet()) {
            System.out.print(e.getKey());
            System.out.print("=");
            System.out.print(e.getValue());
            System.out.print(", ");
        }
        System.out.println("]");
        
        // Covering radii for all internal nodes
        System.out.print("Covering radii of internal nodes (lvl-nodeID=radius): ["); 
        for (Map.Entry<Integer, List<Node>> e : nodesByLevel.entrySet()) {
            Integer level = e.getKey();
            List<Node> nodes = e.getValue();
            for (Node n : nodes) {
                if (n.isLeaf())
                    break;
                System.out.print(level);
                System.out.print("-");
                System.out.print(n.nodeIdPerLevel);
                System.out.print("=");
                System.out.print(n.getParentRadius());
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

    /**
     * Recursive method for printStatistics.
     */
    private void gatherStatistics(Node n, Map<String, Integer> stat) {
        int objectCount = n.getObjectCount();
        if (n.isLeaf()) {
            stat.put("Leaves", stat.get("Leaves") + 1);
            stat.put("Object count", stat.get("Object count") + objectCount);
            if (objectCount == 1) {
                stat.put("Slim leaves", stat.get("Slim leaves") + 1);
            }
        } else {
            stat.put("Internal nodes", stat.get("Internal nodes") + 1);
            stat.put("Pivots", stat.get("Pivots") + n.getObjectCount());
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                this.gatherStatistics(ne.getSubtree(), stat);
            }
            if (objectCount == 1) {
                stat.put("Slim internal nodes", stat.get("Slim internal nodes") + 1);
            }
        }
    }

    /**
     * Prints a structure of the whole tree (all nodes with their objects).
     */
    public void printTree() {
        print(root);
    }

    /**
     * Recursive method for printTree.
     */
    protected void print(Node n) {
        if (n.isLeaf()) {
            if (isAlgorithmInBucket) {
                System.out.print("Leaf: M-tree algorithm");
            } else {
                System.out.print("Leaf: | ");
                AbstractObjectIterator<LocalAbstractObject> objectIt = n.getObjects();
                while (objectIt.hasNext()) {
                    LocalAbstractObject o = objectIt.next();
                    System.out.print(o + ", d=" + o.getDistanceFilter(ParentFilter.class).getParentDist() + " | ");
                }
                System.out.println();
            }
        } else {
            System.out.print("Node (level=" + n.getLevel() + "): | ");
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                System.out.print(ne.getObject() + ", d=" + ne.getParentDist() + ", r=" + ne.getRadius() + " | ");
            }
            System.out.println();
            for (NodeEntry ne : ((InternalNode) n).getNodeEntryList()) {
                print(ne.getSubtree());
            }
        }
    }

    /**
     * **************** Overrided class Object *****************
     */
    @Override
    public String toString() {
        StringBuilder rtv = new StringBuilder("M-tree (");
        rtv.append(this.getObjectCount());
        rtv.append(" stored objects)");
        return rtv.toString();
    }

    @Override
    public void finalize() throws Throwable {
        bucketDispatcher.finalize();
        super.finalize();
    }

    
    //**********************************************************
    // TESTING METHODS....
    //**********************************************************
    
    public void printExpansionRates(int threadCnt) {
        // Run all computations in concurrent threads...
        ThreadPoolExecutor executor = (ThreadPoolExecutor)Executors.newFixedThreadPool(threadCnt);
        Map<Integer,Future<Float>> rates = new TreeMap<>();
        for (LocalBucket bucket : getAllBuckets()) {
            final LocalBucket b = bucket;
            Future<Float> res = executor.submit(new ExpansionRateCallable(b, 30));
            rates.put(bucket.getBucketID(), res);
        }
        
        // Wait for the jobs to complete
        executor.shutdown();        
        try {
            while (!executor.awaitTermination(10, TimeUnit.MINUTES)) {
                System.err.println("Expansion rates computation status: " + executor.getCompletedTaskCount() + "/" + executor.getTaskCount());
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MTree.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Print the results
        System.out.print("ExpansionRates (bucketID=rate): ["); 
        for (Map.Entry<Integer, Future<Float>> e : rates.entrySet()) {
            System.out.print(e.getKey());
            System.out.print("=");
            try {
                System.out.print(e.getValue().get().floatValue());
            } catch (InterruptedException | ExecutionException ex) { }
            System.out.print(", ");
        }
        System.out.println("]");
    }

    private class ExpansionRateCallable implements Callable<Float> {
        private LocalBucket bucket;
        private int k;
        
        public ExpansionRateCallable(LocalBucket b, int k) {
            this.bucket = b;
            this.k = k;
        }

        @Override
        public Float call() throws Exception {
            float maxRate = 0;
            for (AbstractObjectIterator<LocalAbstractObject> iterator = bucket.getAllObjects(); iterator.hasNext();) {
                LocalAbstractObject q = iterator.next();
                KNNQueryOperation oper = new KNNQueryOperation(q, k*2, AnswerType.ORIGINAL_OBJECTS);
                oper.evaluate(bucket.getAllObjects());
                float rate;
                if (oper.getAnswerCount() < k*2)
                    rate = 0;
                else
                    rate = oper.getAnswerDistance() / oper.getAnswer(k-1, 1).next().getDistance();
                if (rate > maxRate)
                    maxRate = rate;
            }
            return maxRate;
        }
    }
    
    public void assignNodeIds() {
        List<Node> curLvl = new ArrayList<>();
        curLvl.add(root);
        for (int l = root.getLevel(); l >= 0; l--) {
            List<Node> nextLvl = new ArrayList<>();
            int id = 0;
            for (Node n : curLvl) {
                n.nodeIdPerLevel = id++;
                if (n instanceof InternalNode) {
                    InternalNode in = (InternalNode)n;
                    for (NodeEntry e : in.getNodeEntryList())
                        nextLvl.add(e.getSubtree());
                }
            }
            curLvl = nextLvl;
        }
    }
    
    public void computeDistanceFromQueriesToDB(LocalAbstractObject queryObject) {
        String qid = queryObject.getLocatorURI();
        
        StatisticRefCounter opQtoAllObjects = StatisticRefCounter.getStatistics(STAT_NAME_QUERY_TO_ALL_OBJECTS_PER_BUCKET + qid);
        opQtoAllObjects.clear();
        opQtoAllObjects.turnToInsertOrdered();
        StatisticRefCounter opQtoPivots = StatisticRefCounter.getStatistics(STAT_NAME_QUERY_TO_PIVOTS_PER_BUCKET + qid);
        opQtoPivots.clear();
        opQtoPivots.turnToInsertOrdered();
        
        for (LeafNode n : getAllLeaves()) {
            LocalBucket b = n.getBucket();
            int bid = b.getBucketID();
            // Bucket pivot...
            long distQP = (long)(queryObject.getDistance(n.getParentNodeEntry().getObject()) * 1000L);
            opQtoPivots.add(String.format("distQP-%d", bid), distQP);
            opQtoPivots.add(String.format("coverRad-%d", bid), (long)(n.getParentNodeEntry().getRadius() * 1000L));
            // Bucket objects...
            AbstractObjectIterator<LocalAbstractObject> it = b.getAllObjects();
            int objIdx = 0;
            while (it.hasNext()) {
                LocalAbstractObject o = it.next();
                long d = (long)(queryObject.getDistance(o) * 1000L);
                opQtoAllObjects.add(String.format("distQO-%d-%d", bid, objIdx), d);
                objIdx++;
            }
        }
        System.out.println(opQtoAllObjects.toString());
        System.out.println(opQtoPivots.toString());
    }
    
    public void dumpObjects(PrintAllObjectsOperation operation) throws IOException {
        operation.printAll(getAllObjects());
        operation.endOperation();
    }
    
    /** Dump pivots and covering radii to files.
     * This file constains pivot, its identification from the root (dot-separated numbers), covering radius, e.g.:
     * ID4219042 1 4.5
     * This stands for the pivots locator; it is in the root node as the first pivot and the covering radius is 4.5
     * ID23423 1.2.3 2.2
     * This is pivot on the third level from the root (root-1st subtree -> node-2nd subtree -> 3rd pivot), and the covering radius of 2.2
     * 
     * If descriptor should be dumped, it is added as the next line(s) following the above mentioned line.
     */
    public void dumpStructureToFiles(DumpStructureOperation operation) {
        List<String> path = new ArrayList<>();
        dumpStructureToFiles(operation, root, path);
        operation.endOperation();
    }
    
    private void dumpStructureToFiles(DumpStructureOperation operation, Node n, List<String> path) {
        if (n.isLeaf())
            return;
        
        List<NodeEntry> entries = ((InternalNode)n).getNodeEntryList();
        path.add("foo");
        for (int i = 0; i < entries.size(); i++) {
            final NodeEntry e = entries.get(i);
            path.set(path.size() - 1, Integer.toString(i+1));
            operation.addToAnswer(path.size(), path, e.getObject(), e.getRadius());
            dumpStructureToFiles(operation, e.getSubtree(), path);
        }
        path.remove(path.size() - 1);
    }

    public void dumpToFiles(DumpObjectsToFilesOperation operation) {
        List<String> path = new ArrayList<>();
        if (Boolean.TRUE.equals(operation.getArgument(2)))  // == queryByPivotBalls
            dumpToFilesByPivots(operation, root, path);
        else
            dumpToFilesByLeafNodes(operation, root, path);
        operation.endOperation();
    }

    private void dumpToFilesByLeafNodes(DumpObjectsToFilesOperation operation, Node n, List<String> path) {
        if (n.isLeaf()) {
            operation.addToAnswer(path.size(), path, n.getObjects());
        } else {
            List<NodeEntry> entries = ((InternalNode)n).getNodeEntryList();
            path.add("foo");
            for (int i = 0; i < entries.size(); i++) {
                path.set(path.size() - 1, Integer.toString(i+1));
                dumpToFilesByLeafNodes(operation, entries.get(i).getSubtree(), path);
            }
            path.remove(path.size() - 1);
        }
    }

    private void dumpToFilesByPivots(DumpObjectsToFilesOperation operation, Node n, List<String> path) {
        if (n.isLeaf())
            return;
        
        List<NodeEntry> entries = ((InternalNode)n).getNodeEntryList();
        path.add("foo");
        for (int i = 0; i < entries.size(); i++) {
            path.set(path.size() - 1, Integer.toString(i+1));
            final NodeEntry e = entries.get(i);
            dumpToFilesByPivotsQuery(operation, path, 
                    new RangeQueryOperation(e.getObject(), 
                            e.getRadius() * (float)operation.getArgument(3), // * pivotBallRadiusMultiple
                            AnswerType.ORIGINAL_OBJECTS));
            dumpToFilesByPivots(operation, e.getSubtree(), path);
        }
        path.remove(path.size() - 1);
    }
    
    private void dumpToFilesByPivotsQuery(DumpObjectsToFilesOperation operation, List<String> path, RangeQueryOperation query) {
        try {
            this.executeOperation(query);
            operation.addToAnswer(path.size(), path, query.getAnswerObjects());
        } catch (AlgorithmMethodException ex) {
            Logger.getLogger(MTree.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(MTree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /** Computes fat factor of the tree.
     * 
     * @return fat-factor value
     */
    public float getFatFactor() {
        long accessed = 0; // I_c
        
        final AbstractObjectIterator<LocalAbstractObject> iterObjs = getAllObjects();
        
        try {
            while (iterObjs.hasNext()) {
                LocalAbstractObject o = iterObjs.next();

                RangeQueryOperation op = new RangeQueryOperation(o, 0f, AnswerType.ORIGINAL_OBJECTS);
                execute(false, op);
                
                accessed += (long)op.getParameter("AccessedNodes", Integer.class);
            }
        } catch (AlgorithmMethodException | NoSuchMethodException ex) {
        }

        final long objects = getObjectCount();
        final int height = root.getLevel()+1;
        final int totalNodes = root.getNodeCount();
        
        return (float)(accessed - height * objects) / (float)(objects * (totalNodes - height));
    }
}
