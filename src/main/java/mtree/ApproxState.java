/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtree;

import messif.objects.LocalAbstractObject;
import messif.operations.Approximate;

/**
 *
 * @author xdohnal
 */
public class ApproxState {
    protected int limit;
    protected float radiusGuaranteed;
    
    protected int objectsChecked;
    protected int bucketsVisited;
    
    public static ApproxState create(Approximate limits, MTree tree) {
        switch (limits.getLocalSearchType()) {
            case ABS_OBJ_COUNT:
                return new ApproxStateObjects(limits.getLocalSearchParam(), limits.getRadiusGuaranteed());
            case PERCENTAGE:
                return new ApproxStateObjects(Math.round((float)tree.getObjectCount() * (float)limits.getLocalSearchParam() / 100f), limits.getRadiusGuaranteed());
            case DATA_PARTITIONS:
                return new ApproxStateBuckets(limits.getLocalSearchParam(), limits.getRadiusGuaranteed());
            default:
                throw new IllegalArgumentException("Unsupported approximation type: " + limits.getLocalSearchType());
        }
    }

    protected ApproxState(int limit, float radiusGuaranteed) {
        this.limit = limit;
        this.radiusGuaranteed = radiusGuaranteed;
    }
    
    public void update(LeafNode n) {
        objectsChecked += n.getObjectCount();
        bucketsVisited++;
    }
    
    public boolean stop(float headLowerBound) {
        throw new IllegalStateException("ApproxState.stop must be implemented and must not be called!");
    }
    
    private static class ApproxStateBuckets extends ApproxState {
        protected ApproxStateBuckets(int limit, float radiusGuaranteed) {
            super(limit, radiusGuaranteed);
        }
        
        @Override
        public boolean stop(float headLowerBound) {
            return (bucketsVisited >= limit 
                    && (radiusGuaranteed == LocalAbstractObject.UNKNOWN_DISTANCE || headLowerBound > radiusGuaranteed));
        }
    }
    
    private static class ApproxStateObjects extends ApproxState {
        protected ApproxStateObjects(int limit, float radiusGuaranteed) {
            super(limit, radiusGuaranteed);
        }

        @Override
        public boolean stop(float headLowerBound) {
            return (objectsChecked >= limit 
                    && (radiusGuaranteed == LocalAbstractObject.UNKNOWN_DISTANCE || headLowerBound > radiusGuaranteed));
        }
    }
}
