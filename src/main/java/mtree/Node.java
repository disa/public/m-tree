package mtree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import mtree.utils.ParentFilter;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public abstract class Node implements Serializable {
    
    /**
     *  level of this node
     *  0: leaf
     *  1: last internal node
     *  n: root (n = tree height)
     */
    private int level;
    
    /** parent node entry (null in the case of root) */
    private NodeEntry parentNodeEntry;
    
    private static final long serialVersionUID = -2934270893617830341L;
    
    /** usefulness counter for query-driven optimization*/
    protected float nodeUsefulness;

    /** Temporary ID of node on level basis, starting from zero */
    protected transient int nodeIdPerLevel = -1;
    
    /****************** Constructors ******************/
    
    public Node(int level, NodeEntry parentNodeEntry) {
        this.level = level;
        this.parentNodeEntry = parentNodeEntry;
    }
    
    
    /****************** Abstract methods ******************/
    
    /**
     *  Returns <tt>false</tt> if the capacity of this node is not exceeded.
     *
     *  @return <tt>false</tt> if the capacity of this node is not exceeded
     */
    public abstract boolean isCapacityExceeded();
    
    /**
     *  Returns the number of stored objects in this node.
     *
     *  @return the number of stored objects in this node
     */
    public abstract int getObjectCount();
    
    /**
     *	Returns an iterator of all objects stored in this node.
     *
     *  @return an iterator of all objects stored in this node
     */
    public abstract AbstractObjectIterator<LocalAbstractObject> getObjects();
    
    /**
     *  Adjusts the radius of the parent node.
     *
     *  @return <tt>false</tt> if the radius of the parent node hasn't needed
     *  to be adjusted
     */
    protected abstract boolean adjustParentRadius();
    
    /**
     *  Adjusts the distances of HR array of the parent node.
     *
     *  @return <tt>false</tt> if the distances of the parent node haven't
     *  needed to be adjusted
     */
    protected abstract boolean adjustParentHR();
    
    
    /****************** Methods ******************/
    
    /**
     *  Returns the level of this node.
     *
     *  @return the level of this node
     */
    public int getLevel() {
        return level;
    }
    
    public float getNodeUsefulness() {
        return nodeUsefulness;
    }
    
    /**
     * increases the usefulness index of the node
     * 
     * Matej Antol
     */
    public void incNodeUsefulness() {
        nodeUsefulness++;
    }
    
    /**
     * increases the usefulness index of the node
     * Matej Antol
     */
    public void incNodeUsefulness(float howMuch) {
        nodeUsefulness += howMuch;
    }
    
    /**
     *  Returns <tt>true</tt> if descendants of this node are leaves.
     *
     *  @return <tt>true</tt> if descendants of this node are leaves
     */
    public boolean isLastInternalNode() {
        return level == 1;
    }
    
    /**
     *  Returns <tt>true</tt> if this node is a leaf.
     *
     *  @return <tt>true</tt> if this node is a leaf
     */
    public boolean isLeaf() {
        return level == 0;
    }
    
    /** Get the number of nodes routed from this node inclusively.
     * @return number of nodes routed from this node + 1 (this node)
     */
    public abstract int getNodeCount();
    
    /**
     *  Returns the parent node of this node.
     *
     *  @return the parent node of this node (<tt>null</tt> in the case of root)
     */
    public InternalNode getParentNode() {
        if (parentNodeEntry == null)
            return null;
        return parentNodeEntry.getNode();
    }
    
    /**
     *  Returns the parent node entry pointing to this node.
     *
     *  @return the parent node entry pointing to this node (<tt>null</tt> in
     *  the case of root)
     */
    public NodeEntry getParentNodeEntry() {
        return parentNodeEntry;
    }
    
    /**
     *  Creates a root from this node. Sets the parent node entry to null.
     */
    public void createRootNode() {
        parentNodeEntry = null;
    }
    
    /**
     *  Returns the covering radius of the parent node.
     *
     *  @return the covering radius of the parent node (<tt>0</tt> if the
     *  parent node does not exist)
     */
    public float getParentRadius() {
        float r = 0f;
        NodeEntry pne = this.getParentNodeEntry();
        if (pne != null)
            r = pne.getRadius();
        return r;
    }
    
    /**
     *  Adjusts the radius r and the distances of HR array of the parent node.
     *
     *  @return <tt>false</tt> if the radius and the distances of HR array of
     *  the parent node haven't needed to be adjusted
     */
    public boolean updateDistances() {
        return this.adjustParentRadius() | this.adjustParentHR();   // This is bit-wise OR, so oobth the arguments are called.
    }
    
    /**
     *  Returns a sorted list of all objects stored in this node.
     *
     *	@return a sorted list of all objects stored in this node (objects are
     *  sorted by the distance to their parent pivot (in descending manner)
     */
    public List<LocalAbstractObject> getSortedObjectList() {
	List<LocalAbstractObject> sortedList = new ArrayList<LocalAbstractObject>(getObjectCount());
	TreeSet<Pair> sortedSet = new TreeSet<Pair>();
	
        AbstractObjectIterator<LocalAbstractObject> objectIt = this.getObjects();
        while (objectIt.hasNext()) {
            LocalAbstractObject o = objectIt.next();
	    sortedSet.add(new Pair(o, o.getDistanceFilter(ParentFilter.class).getParentDist()));
        }
	
        Iterator<Pair> it = sortedSet.iterator();
	while (it.hasNext())
	    sortedList.add(it.next().o);
        
	return sortedList;
    }
    
    /**
     *  Static class used for sorting objects.
     */
    private static class Pair implements Comparable {
	
	private LocalAbstractObject o;
	private float d;
	
        
        /****************** Constructors ******************/
        
	public Pair(LocalAbstractObject o, float d) {
	    this.o = o;
	    this.d = d;
	}
	
        
        /****************** Implemented interface Comparable ******************/
        
	public int compareTo(Object o) {
	    if (!(o instanceof Pair)) return -1;
            Pair pair = (Pair) o;
            
            if (this.equals(pair)) return 0;
            
            if (d != pair.d)
                return (d > pair.d) ? -1 : 1;
            else
                return (this.hashCode() < pair.hashCode()) ? -1 : 1;
        }
	
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.level;
        hash = 89 * hash + this.nodeIdPerLevel;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Node other = (Node) obj;
        if (this.level != other.level) {
            return false;
        }
        if (this.nodeIdPerLevel != other.nodeIdPerLevel) {
            return false;
        }
        return true;
    }
}
