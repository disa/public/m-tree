package mtree;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import messif.buckets.BucketStorageException;
import messif.buckets.LocalBucket;
import messif.buckets.OccupationLowException;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFixedArrayFilter;
import messif.operations.data.DeleteOperation;
import mtree.utils.ParentFilter;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class LeafNode extends Node implements Serializable {
    
    /** storage for leaf objects */
    private LocalBucket bucket;
    
    private static final long serialVersionUID = -124776407006299704L;
    
    
    /****************** Constructors ******************/
    
    public LeafNode(NodeEntry parentNodeEntry, LocalBucket bucket) {
        super(0, parentNodeEntry);
        this.bucket = bucket;
    }
    
    
    /****************** Methods ******************/
    
    /**
     *  Returns the bucket of this leaf.
     *
     *  @return the bucket of this leaf
     */
    public LocalBucket getBucket() {
        return bucket;
    }
    
//    /**
//     * increases the usefulness index of leaf nodes and all its parents. 
//     * 
//     * Do not call the predecesor's method here to avoid incorrect results in leaf nodes!!!
//     * 
//     * Matej Antol
//     */
//       
//    private Map readCSV(String file) throws IOException {
//        BufferedReader scanner = new BufferedReader(new FileReader(file));
//
//        Map map = new TreeMap();
//        String line;
//        while ( (line = scanner.readLine()) != null) {
//            String[] matches = line.split(",");
//            map.put(matches[0], matches[1]);
//        }
//        return map;
//    }
//   
//    public void incNodeUsefulnessByFrequencyRecursivelyUpToRoot(int id) {
//        int count;
//        try {
//            Map map = readCSV("GA1000entries.csv");
//            count = Integer.valueOf((String) map.get(id));
//        } catch (IOException ex) {
//            count = 1;
//        }
//        nodeUsefulness += count;
//        
//        InternalNode p = getParentNode();
//        while (p != null) {
//            for (int i=0; i<count; i++) {
//                p.incNodeUsefulness();
//            }
//            p = p.getParentNode();
//        }
//    }

    public void incNodeUsefulnessRecursivelyUpToRoot() {
        nodeUsefulness ++;
        
        InternalNode p = getParentNode();
        while (p != null) {
            p.incNodeUsefulness();
            p = p.getParentNode();
        }
    }
    
    
    
    
//    /**
//     *  Deletes the specified object from the bucket.
//     *
//     *  @return the number of deleted objects
//     */
//    public int deleteObject(LocalAbstractObject o) {
//        try {
//            return bucket.deleteObject(o);
//        } catch (NoSuchElementException ex) {
//        } catch (BucketStorageException ex) {
//            ex.printStackTrace();
//        }
//        return 0;
//    }
    
    /**
     *  Deletes the specified object from the bucket.
     *
     *  @return the number of deleted objects
     */
    public int deleteObjects(DeleteOperation oper) {
        try {
            int deleted = 0;
            for (LocalAbstractObject o : oper.getObjectsToDelete()) {
                int del = bucket.deleteObject(o);
                if (del > 0) {
                    oper.addDeletedObject(o);
                    deleted += del;
                }
            }
            return deleted;
        } catch (NoSuchElementException ex) {
            return 0;
        } catch (BucketStorageException ex) {
            ex.printStackTrace();
        }
        return 0;
    }
    
    /**
     *  Sets the radius of the parent node.
     *
     *  @return <tt>false</tt> if the radius of the parent node hasn't needed
     *  to be adjusted
     */
    protected boolean adjustParentRadius(float r) {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null)
            return false;
        
        // Adjustment of the parent radius.
        if (r != pne.getRadius())
            pne.setRadius(r);
        pne.getNode().adjustParentRadius();
        return true;
    }
    
    /**
     *  Sets the distances of HR array of the parent node according to the
     *  object o.
     *
     *  @return <tt>false</tt> if the distances of the parent node haven't
     *  needed to be adjusted
     */
    protected boolean adjustParentHR(PrecomputedDistancesFixedArrayFilter hrDistsObj) {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null || hrDistsObj == null)
            return false;
        
        // Computation of the parent HR array.
        NodeEntry.HREntry[] hr = pne.getHR();
        
        // Adjustment of the parent HR array.
        for (int l = 0; l < hr.length; l++) {
            float d = hrDistsObj.getPrecompDist(l);
            if (hr[l].getMin() > d)
                hr[l].setMin(d);
            if (hr[l].getMax() < d)
                hr[l].setMax(d);
        }
        pne.getNode().adjustParentHR();
        return true;
    }
    
    /**
     *  Sets the radius r and adjusts the distances of HR array of the parent
     *  node.
     *
     *  @return <tt>false</tt> if the radius and the distances of HR array of
     *  the parent node haven't needed to be adjusted
     */
    public boolean updateDistances(float r) {
        return this.adjustParentRadius(r) | this.adjustParentHR();
    }
    
    /**
     *  Sets the radius r and adjusts the distances of HR array of the parent
     *  node according to the object o.
     *
     * @param hrDistsObj the precomputed distances from a new object to the 'nhr' pivots
     * 
     *  @return <tt>false</tt> if the radius and the distances of HR array of
     *  the parent node haven't needed to be adjusted
     */
    public boolean updateDistances(float r, PrecomputedDistancesFixedArrayFilter hrDistsObj) {
        return this.adjustParentRadius(r) | this.adjustParentHR(hrDistsObj);
    }
    
    
    /****************** Implemented class Node ******************/
    
    /**
     *  Returns <tt>false</tt> if the capacity of this node is not exceeded.
     *
     *  @return <tt>false</tt> if the capacity of this node is not exceeded
     */
    public boolean isCapacityExceeded() {
        return bucket.isSoftCapacityExceeded();
    }
    
    /**
     *  Returns the number of stored objects in this node.
     *
     *  @return the number of stored objects in this node
     */
    public int getObjectCount() {
        return bucket.getObjectCount();
    }

    @Override
    public int getNodeCount() {
        return 1;
    }
    
    /**
     *	Returns an iterator of all objects stored in this node.
     *
     *  @return an iterator of all objects stored in this node
     */
    public AbstractObjectIterator<LocalAbstractObject> getObjects() {
        return bucket.getAllObjects();
    }
    
    /**
     *  Adjusts the radius of the parent node.
     *
     *  @return <tt>false</tt> if the radius of the parent node hasn't needed
     *  to be adjusted
     */
    protected boolean adjustParentRadius() {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null)
            return false;
        
        // Computation of the parent radius.
        float r = 0f;
        AbstractObjectIterator<LocalAbstractObject> objectIt = this.getObjects();
        while (objectIt.hasNext()) {
            LocalAbstractObject o = objectIt.next();
            float d = o.getDistanceFilter(ParentFilter.class).getParentDist();
            if (r < d)
                r = d;
        }
        
        // Adjustment of the parent radius.
        if (r != pne.getRadius())
            pne.setRadius(r);
        pne.getNode().adjustParentRadius();
        return true;
    }
    
    /**
     *  Adjusts the distances of HR array of the parent node.
     *
     *  @return <tt>false</tt> if the distances of the parent node haven't
     *  needed to be adjusted
     */
    protected boolean adjustParentHR() {
        NodeEntry pne = getParentNodeEntry();
        if (pne == null)
            return false;
        
        // Computation of the parent HR array.
        NodeEntry.HREntry[] hr = pne.getHR();
        int nhr = hr.length;
        float[] min = new float[nhr];
        float[] max = new float[nhr];
        Arrays.fill(min, Float.MAX_VALUE);
        Arrays.fill(max, 0f);
        
        AbstractObjectIterator<LocalAbstractObject> objectIt = this.getObjects();
        while (objectIt.hasNext()) {
            LocalAbstractObject o = objectIt.next();
            float[] dist = o.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
            int lim = Math.min(nhr, ((dist == null) ? 0 : dist.length));
            for (int l = 0; l < lim; l++) {     // Exploit all npd distances
                if (dist[l] < min[l]) min[l] = dist[l];
                if (dist[l] > max[l]) max[l] = dist[l];
            }
            for (int l = lim; l < nhr; l++) {     // If nhr is greater than npd, compute the necessary distances
                float d = getParentNodeEntry().getObject().getDistance(o);
                if (d < min[l]) min[l] = d;
                if (d > max[l]) max[l] = d;
            }
        }
        
        // Adjustment of the parent HR array.
        for (int l = 0; l < nhr; l++) {
            if (hr[l].getMin() != min[l])
                hr[l].setMin(min[l]);
            if (hr[l].getMax() != max[l])
                hr[l].setMax(max[l]);
        }
        pne.getNode().adjustParentHR();
        return true;
    }
    
    
    /****************** Overrided class Object ******************/
    
    @Override
    public String toString() {
        return "Leaf (#" + nodeIdPerLevel + ", bck#" + bucket.getBucketID() + ")";
    }
    
}
