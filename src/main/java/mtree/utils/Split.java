package mtree.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;
import messif.buckets.split.SplitPolicy;
import messif.buckets.split.impl.SplitPolicyBallPartitioning;
import messif.buckets.split.impl.SplitPolicyGeneralizedHyperplane;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import mtree.MTree;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class Split {
    
    /**
     *  Decides whether objects o1 and o2 belong to one component determined
     *  by edges stored in objectMap.
     *
     *  @return <code>true</code> if objects o1 and o2 belong to one component
     */
    public static boolean formsOneComponent(Map<LocalAbstractObject, List<Edge>> objectMap, LocalAbstractObject o1, LocalAbstractObject o2) {
        
        // Set of objects which should be processed.
        Stack<LocalAbstractObject> objects = new Stack<LocalAbstractObject>();
        objects.add(o1);
        
        // Set of processed objects.
        Set<LocalAbstractObject> processed = new HashSet<LocalAbstractObject>();
        processed.add(o1);
        
        LocalAbstractObject p1, p2;
        while (!objects.isEmpty()) {
            p1 = objects.pop();
            List<Edge> edgeList = objectMap.get(p1);
            for (Edge e: edgeList) {
                if (e.getObject1() == p1)
                    p2 = e.getObject2();
                else
                    p2 = e.getObject1();
                
                if (p2 == o2) 
                    return true;
                if (processed.add(p2))
                    objects.add(p2);
            }
        }
        return false;
    }
    
    /**
     *  Returns a list of objects which belong to one component (component is
     *  determined by object o).
     *
     *  @return a list of objects which belong to one component (component is determined by object o)
     */
    public static List<LocalAbstractObject> findAllObjectsInComponent(Map<LocalAbstractObject, List<Edge>> objectMap, LocalAbstractObject o) {
        List<LocalAbstractObject> objectList = new ArrayList<LocalAbstractObject>();
        objectList.add(o);
        findAllObjectsInComponent(objectMap, o, objectList);
        return objectList;
    }
    
    /**
     *  Recursive method for findAllObjectsInComponent.
     */
    private static void findAllObjectsInComponent(Map<LocalAbstractObject, List<Edge>> objectMap, LocalAbstractObject o, List<LocalAbstractObject> objectList) {
        LocalAbstractObject p;
        List<Edge> edgeList = objectMap.get(o);
        for (Edge e: edgeList) {
            if (e.getObject1().equals(o))
                p = e.getObject2();
            else
                p = e.getObject1();
            
            if (!objectList.contains(p)) {
                objectList.add(p);
                findAllObjectsInComponent(objectMap, p, objectList);
            }
        }
    }
    
    /**
     *  Looks for a pivot (the most convenient object) from the list of objects
     *  objectList.
     *
     *  @return a pivot (the most convenient object) from the list of objects objectList
     */
    public static LocalAbstractObject findPivot(Map<LocalAbstractObject, List<Edge>> objectMap, List<LocalAbstractObject> objectList) {
        
        // Map saves the maximum distance from the distances to other objects
        // for each object in the given set.
        Map<LocalAbstractObject, Float> nmd = new HashMap<LocalAbstractObject, Float>();
        
        for (LocalAbstractObject o: objectList) {
            float max = 0f;
            for (Edge e: objectMap.get(o))
                if (e.getDistance() > max)
                    max = e.getDistance();
            nmd.put(o, max);
        }
        
        LocalAbstractObject p = null;
        float min = Float.MAX_VALUE;
        for (LocalAbstractObject o: objectList)
            if (nmd.get(o) < min) {
                min = nmd.get(o);
                p = o;
            }
        
        return p;
    }
    
    /**
     *  Returns an edge which is the most convenient to divide the spanning tree
     *  into 2 components.
     *
     *  @return an edge which is the most convenient to divide the spanning tree
     *  into 2 components
     */
    public static Edge findBestSpanEdge(Map<LocalAbstractObject, List<Edge>> objectMap, Set<Edge> spanSet) {
        int min = Integer.MAX_VALUE;
        Edge le = null;
        int minIdx = -1;
        List<LocalAbstractObject> objectList1 = null;
        List<LocalAbstractObject> objectList2 = null;

        // Locating and removing edge which divides the spanning tree into 2
        // components.
//        for (int i = 0; i < 2; i++) {
            Iterator<Edge> it = spanSet.iterator();
            for (int i = 0; i <= spanSet.size() / 2; i++) {     // Do not test all, but just half of longest edges
                Edge e = it.next();
                
                LocalAbstractObject o1 = e.getObject1(), o2 = e.getObject2();
                objectMap.get(o1).remove(e);
                objectMap.get(o2).remove(e);

                objectList1 = Split.findAllObjectsInComponent(objectMap, o1);
                objectList2 = Split.findAllObjectsInComponent(objectMap, o2);
                if (Math.abs(objectList1.size() - objectList2.size()) < min) {
                    min = Math.abs(objectList1.size() - objectList2.size());
                    le = e;
                    minIdx = i;
                }

                objectMap.get(o1).add(e);
                objectMap.get(o2).add(e);
            }
            
//            if (min > objectMap.size() / 2) {
//                objectList1 = null;
//            } else {
//                break;
//            }
//        }

        if (minIdx > 0) {
            System.err.println("Notice: Splitting used " + (minIdx+1) + "th longest edge to partition data: " + ((objectList1 == null) ? -1 : objectList1.size())
                    + ":" + ((objectList2 == null) ? -1 : objectList2.size()));
        }
        
        return le;
    }
    
    /**
     *  Fills the distances between pivot p and other objects of a given
     *  component (the component is determined by pivot p).
     *
     *  @return covering radius of the pivot p
     */
    public static float fillDistances(Map<LocalAbstractObject, List<Edge>> objectMap, LocalAbstractObject p) {
        float r = 0f;
        for(Edge e: objectMap.get(p)) {
            LocalAbstractObject o = (e.getObject1().equals(p)) ? e.getObject2() : e.getObject1();
            
            if (r < e.getDistance())
                r = e.getDistance();
            o.getDistanceFilter(ParentFilter.class).setParentDist(e.getDistance());
        }
        return r;
    }
    
    /**
     *  Sets the parent distance of all objects in the passed list. The distance is calculated from the passed pivot.
     * @return maximum of the distances which can be used as a covering radius.
     */
    public static float setParentDistances(LocalAbstractObject pivot, List<LocalAbstractObject> list) {
        float r = 0f;
        for (LocalAbstractObject o : list) {
            final float d = o.getDistance(pivot);
            if (d > r)
                r = d;
            o.getDistanceFilter(ParentFilter.class).setParentDist(d);
        }
        return r;
    }    
    
    /**
     *  Sets the parent distance of the split policy pivots to the given distance.
     */
    public static void setParentDistances(SplitPolicy sp, float dist) {
        if (sp instanceof SplitPolicyBallPartitioning) {
            LocalAbstractObject pivot = ((SplitPolicyBallPartitioning) sp).getPivot();
            ParentFilter filter = pivot.getDistanceFilter(ParentFilter.class);
            if (filter == null) {
                filter = new ParentFilter();
                pivot.chainFilter(filter, false);
            }
            filter.setParentDist(dist);
        }
        if (sp instanceof SplitPolicyGeneralizedHyperplane) {
            SplitPolicyGeneralizedHyperplane gh = (SplitPolicyGeneralizedHyperplane) sp;
            ParentFilter filter = gh.getLeftPivot().getDistanceFilter(ParentFilter.class);
            if (filter == null) {
                filter = new ParentFilter();
                gh.getLeftPivot().chainFilter(filter, false);
            }
            filter.setParentDist(dist);
            filter = gh.getRightPivot().getDistanceFilter(ParentFilter.class);
            if (filter == null) {
                filter = new ParentFilter();
                gh.getRightPivot().chainFilter(filter, false);
            }
            filter.setParentDist(dist);
        }
    }
    
    /**
     *  Computes and sets the parent distance of the split policy pivots to the
     *  distance between the pivot and the given object.
     */
    public static void setParentDistances(SplitPolicy sp, LocalAbstractObject o) {
        if (sp instanceof SplitPolicyBallPartitioning) {
            LocalAbstractObject pivot = ((SplitPolicyBallPartitioning) sp).getPivot();
            pivot.getDistanceFilter(ParentFilter.class).setParentDist(pivot.getDistance(o));
        }
        if (sp instanceof SplitPolicyGeneralizedHyperplane) {
            LocalAbstractObject leftPivot = ((SplitPolicyGeneralizedHyperplane) sp).getLeftPivot();
            LocalAbstractObject rightPivot = ((SplitPolicyGeneralizedHyperplane) sp).getRightPivot();
            leftPivot.getDistanceFilter(ParentFilter.class).setParentDist(leftPivot.getDistance(o));
            rightPivot.getDistanceFilter(ParentFilter.class).setParentDist(rightPivot.getDistance(o));
        }
    }
    
    /**
     *  Sets the parent distance of the split policy pivots to the distance 
     *  between the pivot and the last processed object.
     */
    public static void setParentDistances(SplitPolicy sp) {
        if (sp instanceof SplitPolicyBallPartitioning) {
            SplitPolicyBallPartitioning spbp = (SplitPolicyBallPartitioning) sp;
            float dist = spbp.getDistanceToPivot();
            spbp.getPivot().getDistanceFilter(ParentFilter.class).setParentDist(dist);
        }
        if (sp instanceof SplitPolicyGeneralizedHyperplane) {
            SplitPolicyGeneralizedHyperplane spgh = (SplitPolicyGeneralizedHyperplane) sp;
            float dist1 = spgh.getDistanceToLeftPivot();
            float dist2 = spgh.getDistanceToRightPivot();
            spgh.getLeftPivot().getDistanceFilter(ParentFilter.class).setParentDist(dist1);
            spgh.getRightPivot().getDistanceFilter(ParentFilter.class).setParentDist(dist2);
        }
    }
    
    /**
     *  Determining and computing the missing parameters of the split policy.
     *
     */
    public static void completeSplitPolicy(SplitPolicy sp, MTree mtree) {
        
        // Determining and computing the missing parameters of the split policy.
        if (!sp.isComplete()) {
            
            // In the case of ball partitioning, a missing radius is computed.
            if (sp instanceof SplitPolicyBallPartitioning) {
                
                // Computing the missing radius.
                LocalAbstractObject p = ((SplitPolicyBallPartitioning) sp).getPivot();
                float r = 0f;
                float[] distances = new float[mtree.getObjectCount()];
                int pos = 0;
                AbstractObjectIterator<LocalAbstractObject> it = mtree.getAllObjects();
                while (it.hasNext()) {
                    distances[pos] = it.next().getDistance(p);
                    pos++;
                }
                
                Arrays.sort(distances);
                int center = distances.length / 2;
                if (distances.length % 2 == 1)
                    r = distances[center];
                else
                    r = (distances[center - 1] + distances[center]) / 2f;
                ((SplitPolicyBallPartitioning) sp).setRadius(r);
            }
        }
    }
    
    /**
     *  Static class used for building the spanning tree.
     */
    public static class Edge implements Comparable, Serializable {
        
        private LocalAbstractObject o1;
        private LocalAbstractObject o2;
        private float d;
        private static AtomicInteger nextHash = new AtomicInteger(1);
        final int hash = nextHash.getAndIncrement();
        static final long serialVersionUID = 1L;
        
        
        /****************** Constructors ******************/
        
        public Edge(LocalAbstractObject o1, LocalAbstractObject o2) {
            this.o1 = o1;
            this.o2 = o2;
            d = o1.getDistance(o2);
        }
        
        
        /****************** Methods ******************/
        
        public LocalAbstractObject getObject1() {
            return o1;
        }
        
        public LocalAbstractObject getObject2() {
            return o2;
        }
        
        public float getDistance() {
            return d;
        }
        
        
        /****************** Implemented interface Comparable ******************/
        
        public int compareTo(Object o) {
            if (!(o instanceof Edge)) return -1;
            
            return defaultOrder.compare(this, (Edge)o);
        }
        
        private static final Comparator<Edge> defaultOrder = comparatorByIncreasingDistance();
        
        public static Comparator<Edge> comparatorByIncreasingDistance() {
            return (e1, e2) -> {
                if (e1.equals(e2)) return 0;

                if (e1.d != e2.d)
                    return (e1.d < e2.d) ? -1 : 1;
                else
                    return (e1.hashCode() < e2.hashCode()) ? -1 : 1;
            };
        }
        
        public static Comparator<Edge> comparatorByDecreasingDistance() {
            return (e1, e2) -> {
                if (e1.equals(e2)) return 0;

                if (e1.d != e2.d)
                    return (e1.d > e2.d) ? -1 : 1;
                else
                    return (e1.hashCode() < e2.hashCode()) ? -1 : 1;
            };
        }
        
        /****************** Overrided class Object ******************/
        
        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Edge))
                return false;
            return hash == ((Edge) obj).hash;
        }
        
        @Override
        public int hashCode() {
            return hash;
        }
        
    }
    
}
