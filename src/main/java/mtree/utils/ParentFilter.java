package mtree.utils;

import java.io.IOException;
import java.io.OutputStream;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFilter;
import messif.objects.nio.BinaryInput;
import messif.objects.nio.BinaryOutput;
import messif.objects.nio.BinarySerializator;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class ParentFilter extends PrecomputedDistancesFilter {
    
    /** precomputed distance to the parent pivot */
    private float parentDist = LocalAbstractObject.UNKNOWN_DISTANCE;
    
    static final long serialVersionUID = 1L;
    
    
    /****************** Constructors ******************/
    
    public ParentFilter() {
        super();
    }
    
    public ParentFilter(LocalAbstractObject object) {
        object.chainFilter(this, true);
    }
    
    
    /****************** Methods ******************/
    
    /**
     *  @return Returns the distance to the parent pivot.
     */
    public synchronized float getParentDist() {
        return parentDist;
    }
    
    /**
     *  Sets the distance to the parent pivot.
     */
    public synchronized void setParentDist(float parentDist) {
        this.parentDist = parentDist;
    }
    
    
    /****************** Implemented class PrecomputedDistancesFilter ******************/

    public float getPrecomputedDistance(LocalAbstractObject obj, float[] metaDistances) {
        return LocalAbstractObject.UNKNOWN_DISTANCE;
    }

    @Override
    protected boolean addPrecomputedDistance(LocalAbstractObject obj, float distance, float[] metaDistances) {
        if (parentDist != LocalAbstractObject.UNKNOWN_DISTANCE)
            return false;
        parentDist = distance;
        return true;
    }

    /**
     * Returns true if object can be filtered out using stored precomputed distances.
     * Otherwise returns false, i.e. when obj must be checked using original distance (getDistance()).
     *
     * In other words, method returns true if this object and obj are more distant than radius. By
     * analogy, returns false if this object and obj are within distance radius. However, both this cases
     * use only precomputed distances! Thus, the real distance between this object and obj can be greater
     * than radius although the method returned false!!!
     */
    public final boolean excludeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        try {
            return excludeUsingPrecompDist((ParentFilter) targetFilter, radius);
        } catch (ClassCastException e) {
            return false;
        }
    }
    
    public boolean excludeUsingPrecompDist(ParentFilter targetFilter, float radius) {
        
        // We have no precomputed distance to the parent pivot either in the query or this object.
        if (parentDist == LocalAbstractObject.UNKNOWN_DISTANCE || targetFilter.getParentDist() == LocalAbstractObject.UNKNOWN_DISTANCE)
            return false;
        
        return Math.abs(parentDist - targetFilter.getParentDist()) > radius;
    }
    
    public final boolean includeUsingPrecompDist(PrecomputedDistancesFilter targetFilter, float radius) {
        try {
            return includeUsingPrecompDist((ParentFilter) targetFilter, radius);
        } catch (ClassCastException e) {
            return false;
        }
    }
    
    public boolean includeUsingPrecompDist(ParentFilter targetFilter, float radius) {
        
        // We have no precomputed distance to the parent pivot either in the query or this object.
        if (parentDist == LocalAbstractObject.UNKNOWN_DISTANCE || targetFilter.getParentDist() == LocalAbstractObject.UNKNOWN_DISTANCE)
            return false;
        
        return parentDist + targetFilter.getParentDist() <= radius;
    }

    @Override
    protected boolean isDataWritable() {
        return false;
    }

    @Override
    protected void writeData(OutputStream stream) throws IOException {
        throw new UnsupportedOperationException("The pivot map filter is not to be written into the text file");
    }


    /****************** Implemented interface Cloneable ******************/
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        ParentFilter rtv = (ParentFilter) super.clone();
        rtv.setParentDist(parentDist);
        return rtv;
    }


    //************ BinarySerializable interface ************//

    /**
     * Creates a new instance of ParentFilter loaded from binary input buffer.
     * 
     * @param input the buffer to read the ParentFilter from
     * @param serializator the serializator used to write objects
     * @throws IOException if there was an I/O error reading from the buffer
     */
    protected ParentFilter(BinaryInput input, BinarySerializator serializator) throws IOException {
        super(input, serializator);
        parentDist = serializator.readFloat(input);
    }

    @Override
    public int binarySerialize(BinaryOutput output, BinarySerializator serializator) throws IOException {
        return super.binarySerialize(output, serializator) + serializator.write(output, parentDist);
    }

    @Override
    public int getBinarySize(BinarySerializator serializator) {
        return super.getBinarySize(serializator) + 4;
    }

}
