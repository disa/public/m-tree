package mtree.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import messif.buckets.LocalBucket;
import messif.objects.LocalAbstractObject;
import messif.objects.PrecomputedDistancesFixedArrayFilter;
import mtree.InternalNode;
import mtree.LeafNode;
import mtree.Node;
import mtree.NodeEntry;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class Search {
    
    
    /****************** Filtering methods ******************/

    /**
     *  Clears redundant precomputed distances to external pivots from the passed object.
     * 
     * @param obj the external-pivots pivotfilter of this object might be changed (if maxPvts &lt; the number of the object's precomputed distances)
     * @param maxPvts only distances to the first 'maxPvts' pivots will remain in object's filter
     * @return the original object's filter (it contains all the precomputed distances, i.e. possibly more than maxPvts)
     */
    public static PrecomputedDistancesFixedArrayFilter clearPrecompDistances(LocalAbstractObject obj, int maxPvts) {
        // Fixed pivots filter.
        PrecomputedDistancesFixedArrayFilter pivotFilter = obj.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class);
        if (pivotFilter != null && maxPvts < pivotFilter.getPrecompDistSize()) {   // Limit the precomputed distances
            PrecomputedDistancesFixedArrayFilter copyFilter = new PrecomputedDistancesFixedArrayFilter();
            copyFilter.setFixedPivotsPrecompDist(pivotFilter.getPrecompDist());
            pivotFilter.removeLastPrecompDists(pivotFilter.getPrecompDistSize() - maxPvts);
            return copyFilter;
        } else {
            return pivotFilter;
        }
    }
    
    /**
     *  Sets the parent-distance filter as well as the fixed array of precomputed
     *  distances to the pivots (when Pivoting M-tree is initialized).
     *  In both cases the filters are created only if they are not presented within the object q.
     * @param maxPvts initialize only distances to the first 'maxPvts' pivots 
     */
    public static void setPrecompDistances(List<LocalAbstractObject> lst, LocalAbstractObject[] pivots, int maxPvts) {
        for (LocalAbstractObject q : lst)
            setPrecompDistances(q, pivots, pivots.length);            
    }
        
    /**
     *  Sets the parent-distance filter as well as the fixed array of precomputed
     *  distances to the pivots (when Pivoting M-tree is initialized).
     *  In both cases the filters are created only if they are not presented within the object q.
     */
    public static LocalAbstractObject setPrecompDistances(LocalAbstractObject q, LocalAbstractObject[] pivots) {
        return setPrecompDistances(q, pivots, pivots.length);
    }
        
    /**
     *  Sets the parent-distance filter as well as the fixed array of precomputed
     *  distances to the pivots (when Pivoting M-tree is initialized).
     *  In both cases the filters are created only if they are not presented within the object q.
     * 
     * @param maxPvts initialize only distances to the first 'maxPvts' pivots 
     */
    public static LocalAbstractObject setPrecompDistances(LocalAbstractObject q, LocalAbstractObject[] pivots, int maxPvts) {
        
        // Parent distance filter.
        ParentFilter parentFilter = q.getDistanceFilter(ParentFilter.class);
        if (parentFilter == null)
            parentFilter = new ParentFilter(q);
        parentFilter.setParentDist(LocalAbstractObject.UNKNOWN_DISTANCE);
        
        // Fixed pivots filter.
        PrecomputedDistancesFixedArrayFilter pivotFilter = q.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class);
        if (pivotFilter == null) {
            pivotFilter = new PrecomputedDistancesFixedArrayFilter(q);
            for (int l = 0; l < maxPvts; l++)
                pivotFilter.addPrecompDist(q.getDistance(pivots[l]));
        } else {
            for (int l = 0; l < maxPvts && l < pivotFilter.getPrecompDistSize(); l++)
                if (pivotFilter.getPrecompDist(l) == LocalAbstractObject.UNKNOWN_DISTANCE)
                    pivotFilter.setPrecompDist(l, q.getDistance(pivots[l]));
            for (int l = pivotFilter.getPrecompDistSize(); l < maxPvts; l++)
                pivotFilter.addPrecompDist(q.getDistance(pivots[l]));
//            if (pivotFilter.getPrecompDistSize() != pivots.length) {
//                try {
//                    throw new AlgorithmMethodException("The number of precomputed distances is not equal to the number of pivots!");
//                } catch (AlgorithmMethodException ex) {
//                    ex.printStackTrace();
//                }
//            } else {
//                for (int l = 0; l < pivots.length; l++)
//                    if (pivotFilter.getPrecompDist(l) == LocalAbstractObject.UNKNOWN_DISTANCE)
//                        pivotFilter.setPrecompDist(l, q.getDistance(pivots[l]));
//            }
        }
        
        return q;
    }
    
    /**
     *  Decides whether the specified node entry could be filtered.
     *
     *  @param oParentDist the distance between the object stored in node entry
     *  ne and its parent pivot
     *  @param qParentDist the distance between the query object and its parent pivot
     *  @param startNode the node which has run the query
     *  @return <tt>true</tt> if the node entry is filtered
     */
    /*
    public static boolean filterNodeEntry(NodeEntry ne, float oParentDist, LocalAbstractObject q, float qParentDist, float r, Node startNode) {
        //return ((startNode != ne.getNode() && oParentDist != LocalAbstractObject.UNKNOWN_DISTANCE && qParentDist != LocalAbstractObject.UNKNOWN_DISTANCE && Math.abs(qParentDist - oParentDist) > ne.getRadius() + r) || Search.filterNodeEntryUsingPivots(ne, q, r));
        return ((startNode != ne.getNode() && Math.abs(qParentDist - oParentDist) > ne.getRadius() + r) || Search.filterNodeEntryUsingPivots(ne, q, r));
    }
    */
    
    /**
     *  Decides whether the specified node entry could be filtered.
     *
     *  @param ne node entry to be filtered
     *  @param oParentDist the distance between the object stored in node entry ne and its parent pivot
     *  @param q query object
     *  @param qParentDist the distance between the query object and its parent pivot
     *  @param r query radius
     *  @return <tt>true</tt> if the node entry is filtered out
     */
    public static boolean filterNodeEntry(NodeEntry ne, float oParentDist, LocalAbstractObject q, float qParentDist, float r) {
        if (oParentDist == LocalAbstractObject.UNKNOWN_DISTANCE || qParentDist == LocalAbstractObject.UNKNOWN_DISTANCE)
            return Search.filterNodeEntryUsingPivots(ne, q, r);
        
        return (Math.abs(qParentDist - oParentDist) > ne.getRadius() + r) || Search.filterNodeEntryUsingPivots(ne, q, r);
    }
    
    /**
     *  Decides whether the specified node entry could be filtered in the sense
     *  of filtering using HR array.
     *
     *  @return <tt>true</tt> if the node entry is filtered
     */
    public static boolean filterNodeEntryUsingPivots(NodeEntry ne, LocalAbstractObject q, float r) {
        float[] dist = q.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        NodeEntry.HREntry[] hr = ne.getHR();
	for (int i = 0; i < hr.length; i++) {
            // the query region intersects the external pivot's cut region?
            if (!((dist[i] - r <= hr[i].getMax()) && (dist[i] + r >= hr[i].getMin())))
                return true;
        }
        return false;
    }
    
    /**
     *  Decides whether the distance of an object o to a pivot p1 is farther
     *  than the distance to a pivot p2 on the basis of precomputed distances to
     *  pivots without any additional distance computations.
     *
     *  @return -1 if the distance to the pivot p1 is shorter than to the pivot p2;  
     *           1 if the distance to the pivot p1 is farther than to the pivot p2; 
     *           0 it cannot be resolved if the distance to the pivot p1 is shorter than to the pivot p2 without any distance computations
     */
    public static int divideObjectUsingPivots(LocalAbstractObject o, LocalAbstractObject p1, LocalAbstractObject p2) {
        float[] oPrecompDist = o.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        float[] p1PrecompDist = p1.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        float[] p2PrecompDist = p2.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        if (oPrecompDist != null) {
            for (int i = 0; i < oPrecompDist.length; i++) {
                if (oPrecompDist[i] + p1PrecompDist[i] < Math.abs(oPrecompDist[i] - p2PrecompDist[i]))
                    return -1;
                if (oPrecompDist[i] + p2PrecompDist[i] < Math.abs(oPrecompDist[i] - p1PrecompDist[i]))
                    return 1;
            }
        }
        return 0;
    }
    
    /**
     *  Decides whether an object o is placed inside a region determined by a
     *  pivot p and a radius r on the basis of precomputed distances to pivots
     *  without any additional distance computations.
     *
     *  @return -1 if the object o is placed inside the region;
     *           1 if the object o is placed outside the region;
     *           0 it cannot be resolved if the object o is placed inside or outside the region without any distance computations
     */
    public static int divideObjectUsingPivots(LocalAbstractObject o, LocalAbstractObject p, float r) {
        float[] oPrecompDist = o.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        float[] pPrecompDist = p.getDistanceFilter(PrecomputedDistancesFixedArrayFilter.class).getPrecompDist();
        if (oPrecompDist != null) {
            for (int i = 0; i < oPrecompDist.length; i++) {
                if (oPrecompDist[i] + pPrecompDist[i] < r)
                    return -1;
                if (Math.abs(oPrecompDist[i] - pPrecompDist[i]) > r)
                    return 1;
            }
        }
        return 0;
    }
    
    
    /****************** Approximate filtering methods ******************/
    
    /**
     *  Decides whether the specified node entry could be filtered. Note that
     *  the filtering is approximative.
     *
     *  @param oParentDist the distance between the object stored in node entry ne and its parent pivot
     *  @param qParentDist the distance between the query object and its parent pivot
     *  @return <code>true</code> if the node entry is filtered
     */
    public static boolean filterApproxNodeEntry(NodeEntry ne, float oParentDist, LocalAbstractObject q, float qParentDist, float r) {
        if (oParentDist == LocalAbstractObject.UNKNOWN_DISTANCE || qParentDist == LocalAbstractObject.UNKNOWN_DISTANCE)
            return Search.filterNodeEntryUsingPivots(ne, q, r);
        
        return (Math.abs(qParentDist - oParentDist) > ne.getRadius() + r) || Search.filterNodeEntryUsingPivots(ne, q, r);
    }
    
    
    /****************** Searching methods ******************/
    
    /**
     *  Looks for all leaves of the subtree determined by node n.
     */
    public static void getAllLeaves(Node n, List<LeafNode> leaves) {
        if (n.isLeaf())
            leaves.add(((LeafNode) n));
        else
            for (NodeEntry ne: ((InternalNode) n).getNodeEntryList())
                getAllLeaves(ne.getSubtree(), leaves);
    }
    
    /**
     *  Looks for all buckets stored in all leaves of the subtree determined by
     *  node n.
     */
    public static void getAllBuckets(Node n, Collection<LocalBucket> buckets) {
        if (n.isLeaf())
            buckets.add(((LeafNode) n).getBucket());
        else
            for (NodeEntry ne: ((InternalNode) n).getNodeEntryList())
                getAllBuckets(ne.getSubtree(), buckets);
    }
    
    public static void getNodesByLevel(Node n, Map<Integer, List<Node>> nodesByLevel) {
        // Add this node
        List<Node> lst = nodesByLevel.get(n.getLevel());
        if (lst == null) {
            lst = new ArrayList<>();
            nodesByLevel.put(n.getLevel(), lst);
        }
        lst.add(n);
        
        if (n.isLeaf())
            return;
        
        for (NodeEntry ne: ((InternalNode) n).getNodeEntryList())
            getNodesByLevel(ne.getSubtree(), nodesByLevel);
    }

    /**
     *  Looks for all internal nodes with node level 1 (descendants are leaves).
     *
     *  @return a list of internal nodes with node level 1
     */
    public static List<InternalNode> findLastInternalNodes(Node n) {
        List<InternalNode> nodes = new ArrayList<InternalNode>();
        findLastInternalNodes(n, nodes);
        return nodes;
    }
    
    /**
     *  Recursive method for findLastInternalNodes.
     */
    private static void findLastInternalNodes(Node n, List<InternalNode> nodes) {
        if (!n.isLeaf()) {
            InternalNode node = (InternalNode) n;
            if (n.isLastInternalNode()) {
                nodes.add(node);
            } else {
                if (n.getLevel() == 2) {
                    for (NodeEntry ne: node.getNodeEntryList())
                        nodes.add((InternalNode) ne.getSubtree());
                } else {
                    for (NodeEntry ne: node.getNodeEntryList())
                        findLastInternalNodes(ne.getSubtree(), nodes);
                }
            }
        }
    }
    
    /**
     *  Looks for all leaves which are incident to the region R(q, r).
     *
     *  @return a list of leaves which are incident to the region R(q, r)
     */
    public static List<LeafNode> findLeavesByQuery(Node n, LocalAbstractObject q, float r) {
        List<LeafNode> leaves = new ArrayList<LeafNode>();
        findLeavesByQuery(n, LocalAbstractObject.UNKNOWN_DISTANCE, q, r, new HashMap<LocalAbstractObject, Float>(), leaves);
        return leaves;
    }
    
    /**
     *  Looks for all leaves which are incident to the region R(q, r).
     *
     *  @return a list of leaves which are incident to the region R(q, r)
     */
    public static List<LeafNode> findLeavesByQuery(Node n, LocalAbstractObject q, float r, Map<LocalAbstractObject, Float> pd) {
        List<LeafNode> leaves = new ArrayList<LeafNode>();
        findLeavesByQuery(n, LocalAbstractObject.UNKNOWN_DISTANCE, q, r, pd, leaves);
        return leaves;
    }
    
    /**
     *  Recursive method for findLeavesByQuery.
     */
    private static void findLeavesByQuery(Node n, float qParentDist, LocalAbstractObject q, float r, Map<LocalAbstractObject, Float> pd, List<LeafNode> leaves) {
        if (!n.isLeaf()) {
            
            // All subtrees of the given node are searched.
            for (NodeEntry ne: ((InternalNode) n).getNodeEntryList()) {
                float oParentDist = ne.getParentDist();
                
                if (!Search.filterNodeEntry(ne, oParentDist, q, qParentDist, r)) {
                    float qPivotDist = (oParentDist == 0f && qParentDist != LocalAbstractObject.UNKNOWN_DISTANCE) ? qParentDist : ne.getObject().getDistance(q);
                    pd.put(ne.getObject(), qPivotDist);
                    if (qPivotDist <= ne.getRadius() + r) {
                        if (n.isLastInternalNode())
                            leaves.add((LeafNode) ne.getSubtree());
                        else
                            findLeavesByQuery(ne.getSubtree(), qPivotDist, q, r, pd, leaves);
                    }
                }
            }
        }
    }
    
}