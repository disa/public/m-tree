package mtree.utils;

import java.util.Iterator;
import messif.objects.LocalAbstractObject;
import mtree.InternalNode;
import mtree.LeafNode;
import mtree.Node;
import mtree.NodeEntry;

/**
 *
 * @author Jan Sedmidubsky, xsedmid@fi.muni.cz, FI MU Brno, Czech Republic
 */
public class Insert {
    
    /**
     * Returns a leaf which is the most convenient for inserting new object.
     * The {@link ParentFilter} of the object is set to the distance to the leaf's parent pivot.
     *
     * @param objIns object to insert
     * @param n node to start search from (this method is recursive)
     * @return a leaf which is the most convenient for inserting new object
     */
    public static LeafNode findNearestLeafList(LocalAbstractObject objIns, Node n) {
        
        // If given node n is a leaf then it is returned. Else all descendants 
        // are recursively searched to find the most convenient leaf.
        if (n.isLeaf()) {
            return (LeafNode) n;
        } else {
            InternalNode in = (InternalNode) n;
            
            Node bestNode = null;
            float d = Float.MAX_VALUE;
            float r = Float.MAX_VALUE;
            for (NodeEntry ne: in.getNodeEntryList()) {
                float dist = objIns.getDistance(ne.getObject());
                if ((dist < d) || (dist == d && r > ne.getRadius())) {
                    d = dist;
                    r = ne.getRadius();
                    bestNode = ne.getSubtree();
                }
            }
            
            objIns.getDistanceFilter(ParentFilter.class).setParentDist(d);
            return findNearestLeafList(objIns, bestNode);
        }
    }
    
    /**
     *  Deletes empty nodes from the tree recursively (from a node n towards the
     *  root of the tree). If it is necessary the root of the tree is replaced
     *  by its descendant.
     *
     *  @return <tt>null</tt> if the root has not been replaced, otherwise a new
     *  root of the tree
     */
    public static Node deleteSlimNodes(Node n) {
        return deleteSlimNodes(n, false);
    }
    
    /**
     *  Recursive method for deleteSlimNodes.
     */
    private static Node deleteSlimNodes(Node n, boolean topDown) {
        
        int objectCount = n.getObjectCount();
        InternalNode pn = n.getParentNode();
        
        if (pn == null || topDown) {
            if (pn == null && objectCount > 1)
                return null;
            if (n.isLeaf() || objectCount > 1)
                return n;
            else
                return deleteSlimNodes(((InternalNode) n).getNodeEntryList().iterator().next().getSubtree(), true);
            
        } else {
            if (objectCount == 0) {
                pn.removeNodeEntry(n.getParentNodeEntry());
                pn.updateDistances();
                return deleteSlimNodes(pn, false);
            }
        }
        
        return null;
    }
    
    /**
     *  Deletes empty nodes from the tree recursively (from a node n towards the
     *  root of the tree). If it is necessary the root of the tree is replaced
     *  by its descendant.
     *
     *  @return <tt>null</tt> if the root has not been replaced, otherwise a new
     *  root of the tree
     */
    public static Node deleteSlimNodes(Node n, Iterator<NodeEntry> nodeEntryIterator) {
        return deleteSlimNodes(n, nodeEntryIterator, false);
    }
    
    /**
     *  Recursive method for deleteSlimNodes.
     */
    private static Node deleteSlimNodes(Node n, Iterator<NodeEntry> nodeEntryIterator, boolean topDown) {
        
        int objectCount = n.getObjectCount();
        InternalNode pn = n.getParentNode();
        
        if (pn == null || topDown) {
            if (pn == null && objectCount > 1)
                return null;
            if (n.isLeaf() || objectCount > 1)
                return n;
            else
                return deleteSlimNodes(((InternalNode) n).getNodeEntryList().iterator().next().getSubtree(), true);
            
        } else {
            if (objectCount == 0) {
                nodeEntryIterator.remove();
                pn.updateDistances();
                return deleteSlimNodes(pn, false);
            }
        }
        
        return null;
    }
    
}
