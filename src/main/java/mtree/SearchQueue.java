/*
 * SearchQueue
 * 
 */

package mtree;

import java.io.Serializable;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 *
 * @param <T> class of object to hold in the queue and to prioritize them
 * @author xbatko
 */
public class SearchQueue<T extends Serializable> implements Serializable {
    /** Serialization ID */
    private static final long serialVersionUID = 1L;

    /** The queue holder array -- sorted by {@link #entryComparator} descendingly. */
    private Entry<T>[] entries;

    /** The actual size of the queue array */
    private int size;
    
    /** Entries comparator */
    private EntryComparator entryComparator;
    
    /**
     * Create a new instance of SearchQueue.
     * @param initialSize the initial size of the queue
     * @param comparator instance of a comparator to compare entries
     */
    private SearchQueue(int initialSize, EntryComparator comparator) {
        this.size = 0;
        this.entries = createEntryArray(initialSize);
        this.entryComparator = comparator;
    }

    /**
     * Create a new instance of SearchQueue with default entry comparator (by main distance and then secondary distance)
     * Initial size is set to 10.
     */
    public SearchQueue() {
        this(10, null);
        this.entryComparator = new EntryComparatorByLowerBoundNatural();
    }
    
    public SearchQueue(EntryComparator comparator) {
        this(10, comparator);
    }

    /**
     * Returns the actual number of elements in this queue.
     * @return the actual number of elements in this queue
     */
    public int getSize() {
        return size;
    }

    /**
     * Returns <code>true</code> if this queue contains no elements.
     * @return <code>true</code> if this queue contains no elements
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the value of the main sort key of the current queue head. (Use as a stop-condition of search algorithms.)
     * @return the minimum value of lower-bound of an entry that has the highest priority (by the sort key) in the queue
     * @throws IndexOutOfBoundsException if the queue is empty
     */
    public float getHeadLowerBound() throws IndexOutOfBoundsException {
        if (entryComparator.hasConsistentOrderingToLowerBound())
            return entries[size - 1].distLowerBound;      // mainDistance is the ordering
        
        // Fallback: Get the minimum out of the whole queue (based on lower bound only!!!!!)
        float min = entries[0].distLowerBound;
        for (int i = 1; i < size; i++)
            if (entries[i].distLowerBound < min)
                min = entries[i].distLowerBound;
        return min;
    }

    /**
     * Returns the secondary sort key of the current queue head.
     * @return the secondary sort key of the current queue head
     * @throws IndexOutOfBoundsException if the queue is empty
     */
    public float getHeadDistanceQueryPivot() throws IndexOutOfBoundsException {
        return entries[size - 1].distQueryPivot;
    }

    /**
     * Add a node to the queue.
     * 
     * @param object the data to associate with the keys
     * @param level level of the node (0=leaf level, 1=last internal level, ... n=root level)
     * @param distLowerBound lower bound on distance to objects from query in a node around pivot
     * @param distQueryPivot distance between query and pivot
     * @param distUpperBound upper bound on distance to objects from query in a node around pivot
     */
    public void add(T object, int level, float distLowerBound, float distQueryPivot, float distUpperBound) {
        add(new Entry<T>(object, level, distLowerBound, distQueryPivot, distUpperBound));
    }

    /**
     * Add an object to the queue.
     * 
     * @param object the data to associate with the keys
     * @param distQueryObject distance from query to an object (so, lowerbound and upperbound is the same) 
     */
    public void add(T object, float distQueryObject) {
        add(object, -1, distQueryObject, distQueryObject, distQueryObject);
    }

    /**
     * Retrieves and removes the head of this queue.
     * This method does not free the memory allocated for array - use {@link #compact} method.
     *
     * @return the head of this queue
     * @throws NoSuchElementException if the queue is empty
     */
    public T remove() throws NoSuchElementException {
        if (size == 0)
            throw new NoSuchElementException();
        Entry<T> rtv = entries[--size];
        entries[size] = null; // Clear the position so that it can be garbage-collected
        return rtv.data;
    }

    /**
     * Retrieves the head of this queue, but does not remove it.
     * This method does not free the memory allocated for array - use {@link #compact} method.
     *
     * @return the head of this queue
     * @throws NoSuchElementException if the queue is empty
     */
    public T peek() throws NoSuchElementException {
        if (size == 0)
            throw new NoSuchElementException();
        return entries[size-1].data;
    }

    /**
     * Compact the memory used by internal array of this queue.
     */
    public void compact() {
        if (entries.length == size)
            return;

        Entry<T>[] updatedEntries = createEntryArray(size);
        System.arraycopy(entries, 0, updatedEntries, 0, size);
        entries = updatedEntries;
    }

    ///////////////////////// Internals /////////////////////////

    /**
     * Add an entry to the list preserving the reversed natural order.
     * @param entry the entry to add
     */
    private void add(Entry<T> entry) {
        // Search for insertion point
        int index = getInsertionPoint(entry);

        // Resize the array if needed
        Entry<T>[] updatedEntries;
        if (entries.length < size + 1) {
            // Create an array with 50% bigger capacity
	    updatedEntries = createEntryArray((entries.length * 3)/2 + 1);

            // Copy the beginning of the array
            System.arraycopy(entries, 0, updatedEntries, 0, index);
        } else updatedEntries = entries;
        
        // Shift data after index
        if (index < size)
            System.arraycopy(entries, index, updatedEntries, index + 1, size - index);

        // Finally add the entry and update queue state
        updatedEntries[index] = entry;
	size++;
        entries = updatedEntries;
    }

    /**
     * Returns the insertion point for the specified entry using the reversed {@linkplain Comparable natural ordering}.
     * If the given entry is smaller than all current entries, size is returned.
     * 
     * @param entry the entry for which to search the insertion point
     * @return the insertion point for the specified entry
     */
    private int getInsertionPoint(Entry entry) {
	// Boundaries
        int low = 0;
	int high = size - 1;

        // Search while withing boundaries
	while (low <= high) {
	    // Get the midpoint (half between low and high)
            int mid = (low + high) >>> 1;

            // Compare the entry at mid with the provided entry
	    int cmp = entryComparator.compare(entries[mid], entry); // entries[mid].compareTo(entry);

            // Equal key found, insertion point is here
            if (cmp == 0)
                return mid;

            // Adjust boundaries to search for lower or higher half
	    if (cmp > 0)
		low = mid + 1;
	    else // cmp < 0
		high = mid - 1;
	}

        // Key not found, return the insertion point
	return low;
    }

    /**
     * Creates a static array of Entries.
     * This method is to avoid STUPID generics unchecked warnings that generics arrays can't be created!
     * 
     * @param size the size of the array to create
     * @return the new array
     */
    @SuppressWarnings("unchecked")
    private Entry<T>[] createEntryArray(int size) {
        return new Entry[size];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("SearchQueue{size=" + size + ", entries=");
        boolean first = true;
        for (Entry<T> e : entries) {
            if (e == null)
                break;
            if (!first)
                sb.append(',');
            else
                first = false;
            sb.append(e);
            // DBG:
            if (e.data instanceof InternalNode) {
                sb.append(",containsMe=");
                sb.append(((InternalNode)e.data).findNode((n) -> { return (n instanceof LeafNode) ? (((LeafNode)n).getBucket().getBucketID() == 2) : false; }) != null);
            }
        }
        sb.append('}');
        return sb.toString();
    }

    /** Internal class for holding queue entries */
    private static class Entry<E> implements Serializable /*, Comparable<Entry>*/ {
        /** Serialization ID */
        private static final long serialVersionUID = 1L;

        /** The data of this entry */
        private final E data;
        
        /** Level of the node */
        private final int nodeLevel;
        
        /** Lower bound on distance to objects from query in a node around pivot */
        private final float distLowerBound;
        /** The distance between query and pivot */
        private final float distQueryPivot;
        /** Upper bound on distance to objects from query in a node around pivot */
        private final float distUpperBound;

        /**
         * Create a new entry.
         * @param data the data for the new entry
         * @param distLowerBound lower bound on distance to objects from query in a node around pivot
         * @param distQueryPivot distance between query and pivot
         * @param distUpperBound upper bound on distance to objects from query in a node around pivot
         */
        public Entry(E data, int level, float distLowerBound, float distQueryPivot, float distUpperBound) {
            this.data = data;
            this.nodeLevel = level;
            this.distLowerBound = distLowerBound;
            this.distQueryPivot = distQueryPivot;
            this.distUpperBound = distUpperBound;
        }

        @Override
        public String toString() {
            return "Entry{" + "data=" + data + ", level=" + nodeLevel + ", distLB=" + distLowerBound + ", dist=" + distQueryPivot + ", distUB=" + distUpperBound + '}';
        }

//        @Override
//        public int compareTo(Entry compare) {
//            //swich between original comparison and comparison by distance   
//            if (useUsefulness)
//                return compareToByUsefulness(compare);
//            else
//                return compareToByDistances(compare);
//        }
//
//        /** Original compareTo that uses distances only */
//        private int compareToByDistances(Entry compare) {
//            // Main distances differ
//            if (mainDistance != compare.mainDistance)
//                return (mainDistance < compare.mainDistance) ? -1 : 1;
//
//            // Secondary distance is not set or equal
//            if (secondaryDistance == Float.POSITIVE_INFINITY || compare.secondaryDistance == Float.POSITIVE_INFINITY || secondaryDistance == compare.secondaryDistance)
//                return 0;
//
//            return (secondaryDistance < compare.secondaryDistance) ? -1 : 1;
//        }
    }

    public static abstract class EntryComparator<E> implements Comparator<Entry<E>>, Serializable {
        public abstract boolean hasConsistentOrderingToLowerBound();//        public abstract float getPriority(E o);
    }
    
    /** Original M-tree's queue ordering.
     * Orders entries by <b>lower bound</b> on distance, then by the distance between query and pivot.
     * The ordering uses: d_min(T(Or)) = max{d(Or,Q) − r(Or),0}
     * Defined in http://www-db.deis.unibo.it/research/papers/SEBD97.pdf
     */
    public static class EntryComparatorByLowerBoundNatural<S> extends EntryComparator<S> {
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return true;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            float lb1 = Math.max(o1.distLowerBound, 0f);
            float lb2 = Math.max(o2.distLowerBound, 0f);
            
            // Both LBs are negative, so use pivot distances to prefer the ball that has its pivot closer to the query
            if (lb1 == 0f && lb2 == 0f)
                return compareSecondary(o1, o2);
            
            // Main distances differ
            if (lb1 != lb2)
                return (lb1 < lb2) ? -1 : 1;

            // Lower bounds of balls that does not contain the query objects are the same, so use the pivot distances
            return compareSecondary(o1, o2);
        }
        
        private int compareSecondary(Entry<S> o1, Entry<S> o2) {
            // Secondary distance is not set or equal
            if (o1.distQueryPivot == Float.POSITIVE_INFINITY || o2.distQueryPivot == Float.POSITIVE_INFINITY || o1.distQueryPivot == o2.distQueryPivot)
                return 0;
            else
                return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;
        }
    }

    /** Original M-tree's queue ordering.
     * Orders entries by lower bound on distance, then by the distance between query and pivot.
     */
    public static class EntryComparatorByUpperBoundNatural<S> extends EntryComparator<S> {
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            // Main distances differ
            if (o1.distUpperBound != o2.distUpperBound)
                return (o1.distUpperBound < o2.distUpperBound) ? -1 : 1;

            // Secondary distance is not set or equal
            if (o1.distQueryPivot == Float.POSITIVE_INFINITY || o2.distQueryPivot == Float.POSITIVE_INFINITY || o1.distQueryPivot == o2.distQueryPivot)
                return 0;

            return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;
        }
    }
    
    /** M-tree's queue ordering based on query distance only. */
    public static class EntryComparatorByQueryDistanceNatural<S> extends EntryComparator<S> {
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            // Main distances differ
            if (o1.distQueryPivot != o2.distQueryPivot)
                return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;

            // Use lower bound then.
            if (o1.distLowerBound == o2.distLowerBound)
                return 0;

            return (o1.distLowerBound < o2.distLowerBound) ? -1 : 1;
        }
    }
       
    
    private static float modifyDistanceByUsefulness(float origDist, float usefulness, float logBase) {
        double denom = Math.log(usefulness + logBase) / Math.log(logBase);
        return (float)(origDist / denom);
    }

    /** Queue ordering by node usefulness applied to main distance (i.e. pivot-query lower-bound on distances between query and objects).
     * Orders entries by main distance (lower bound on query-object distance) that is changed by current node's usefulness.
     * 
     * Matej's original implementation.
     */
    public static class EntryComparatorByUsefulnessOnLowerBound<S> extends EntryComparator<S> {
        private float logBase = 2;
        
        public void setLogBase(float logBase) {
            this.logBase = logBase;
        }
        
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            float distO1 = modifyDistanceByUsefulness(o1.distLowerBound, ((Node)o1.data).getNodeUsefulness(), logBase);      // Use secondary distance here????
            float distO2 = modifyDistanceByUsefulness(o2.distLowerBound, ((Node)o2.data).getNodeUsefulness(), logBase);
            
            if (distO1 != distO2)
                return (distO1 < distO2) ? -1 : 1;

            // Secondary distance is not set or equal
            if (o1.distQueryPivot == Float.POSITIVE_INFINITY || o2.distQueryPivot == Float.POSITIVE_INFINITY || o1.distQueryPivot == o2.distQueryPivot)
                return 0;

            return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;
        }
    }
    
    /** Queue ordering by node usefulness.
     * Orders entries by secondary distance (original pivot-query distance) that is changed by current node's usefulness. Main distance is not used in ordering!
     * 
     * Vlasta's version.
     */
    public static class EntryComparatorByUsefulnessOnPivotQueryDistance<S> extends EntryComparator<S> {
        private float logBase = 2;
        
        public void setLogBase(float logBase) {
            this.logBase = logBase;
        }
        
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            float distO1 = modifyDistanceByUsefulness(o1.distQueryPivot, ((Node)o1.data).getNodeUsefulness(), logBase);      // Use secondary distance here????
            float distO2 = modifyDistanceByUsefulness(o2.distQueryPivot, ((Node)o2.data).getNodeUsefulness(), logBase);
            
            if (distO1 != distO2)
                return (distO1 < distO2) ? -1 : 1;
            else
                return 0;
        }
    }

    /** Queue ordering by node usefulness but using "gravity".
     * Orders entries by secondary distance (original pivot-query distance) that is changed by current node's usefulness. Main distance is not used in ordering!
     */
    public static class EntryComparatorByUsefulnessGravityOnPivotQueryDistance<S> extends EntryComparator<S> {
        private float logBase = 2;
        private float distPower = 2;
        private float normDist = 10;
        
        public void setLogBase(float logBase) {
            this.logBase = logBase;
        }
        
        public void setDistancePower(float distPower) {
            this.distPower = distPower;
        }

        public void setNormalizationDist(float normDist) {
            this.normDist = normDist;
        }
        
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            float distO1 = modifyDistanceByUsefulnessAsGravity(o1.distQueryPivot, ((Node)o1.data).getNodeUsefulness());      // Use secondary distance here????
            float distO2 = modifyDistanceByUsefulnessAsGravity(o2.distQueryPivot, ((Node)o2.data).getNodeUsefulness());
            
            if (distO1 != distO2)
                return (distO1 < distO2) ? -1 : 1;
            else
                return 0;
        }

        private float modifyDistanceByUsefulnessAsGravity(float origDist, float usefulness) {
            double mass = Math.log(usefulness + logBase) / Math.log(logBase);

            final float distForGravity = (origDist / normDist) + 1f;     // Should this be max. distance in data? Or half of the max distance? Or avg dist?
            double gravity = mass / Math.pow(distForGravity, distPower);

            return (float)(origDist / gravity);
        }
    }


    /** Comparator based on a compound of query-pivot distance and lower-bound distance.
     * Simple linear regression y = k*x + q.
     * @param <S> 
     */
    public static class EntryComparatorByDistanceCompound<S> extends EntryComparator<S> {
        private float lineSlope = 0.8f;
        private float lineShift = 2000f;
        
        public void setSlope(float slope) {
            this.lineSlope = slope;
        }
        
        public void setShift(float shift) {
            this.lineShift = shift;
        }
        
        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            // Compare by projection only leaf-node levels!
            float distO1 = (o1.nodeLevel == 1) ? Math.max(projectDistance(o1.distLowerBound), o1.distQueryPivot) : o1.distLowerBound;
            float distO2 = (o2.nodeLevel == 1) ? Math.max(projectDistance(o2.distLowerBound), o2.distQueryPivot) : o2.distLowerBound;
            
            // Main distances differ
            if (distO1 != distO2)
                return (distO1 < distO2) ? -1 : 1;
            else if (o1.distQueryPivot != o2.distQueryPivot)
                return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;
            else
                return 0;
        }
        
        private float projectDistance(float distance) {
            return lineSlope * distance + lineShift;
        }
    }

    /** Comparator based on a compound of query-pivot distance and lower-bound distance by projecting to the line.
     * Projection using line: priority = k*"lower-bound" + "query-distance".
     * @param <S> 
     */
    public static class EntryComparatorByDistanceLineProjection<S> extends EntryComparator<S> {
        private float lineSlope = 0.8f;
        private boolean limitToLeaves = true;
        
        public void setSlope(float slope) {
            this.lineSlope = slope;
        }

        public void setShift(float shift) {
            // This is ignored, method is present to have the same interface as EntryComparatorByDistanceCompound only
        }
        
        void setApplyToLeavesOnly(boolean limitToLeaves) {
            this.limitToLeaves = limitToLeaves;
        }

        @Override
        public boolean hasConsistentOrderingToLowerBound() {
            return false;
        }

        @Override
        public int compare(Entry<S> o1, Entry<S> o2) {
            // Compare by projection only leaf-node levels!
            float distO1 = (!limitToLeaves || o1.nodeLevel == 1) ? projectDistance(o1.distLowerBound, o1.distQueryPivot) : o1.distLowerBound;
            float distO2 = (!limitToLeaves || o2.nodeLevel == 1) ? projectDistance(o2.distLowerBound, o2.distQueryPivot) : o2.distLowerBound;
            
            // Comparison by projection, next by "query-distance"
            if (distO1 != distO2)
                return (distO1 < distO2) ? -1 : 1;
            else if (o1.distQueryPivot != o2.distQueryPivot)
                return (o1.distQueryPivot < o2.distQueryPivot) ? -1 : 1;
            else
                return 0;
        }
        
        private float projectDistance(float lowerBound, float queryDistance) {
            return lineSlope * lowerBound + queryDistance;
        }
    }
    
    /** Comparator based on a compound of query-pivot distance and lower-bound distance by projecting to the line.
     * Projection using line: priority = k*"lower-bound" + "query-distance".
     * @param <S> 
     */
    public static class EntryComparatorByDistanceLineProjectionAllNodes<S> extends EntryComparatorByDistanceLineProjection<S> {
        public EntryComparatorByDistanceLineProjectionAllNodes() {
            setApplyToLeavesOnly(false);
        }
    }
}
