# Welcome to M-tree project

A Java implementation of M-tree that builds on top the [MESSIF library](https://gitlab.fi.muni.cz/disa/public/messif). The M-tree is an index structure for metric data (i.e., data for which we can define a distance function).

The structure was developed and this project initiazed by the [DISA laboratory](http://disa.fi.muni.cz/) at Masaryk University, Brno, Czech Republic. This implementation is used in most of [demo application](http://disa.fi.muni.cz/prototype-applications/) of DISA. The package is a Java Maven project containing both centralized M-Index and its distributed version (restricted functionality).


## References

* Ciaccia, P., Patella, M., & Zezula, P. (1997, August). M-tree: An efficient access method for similarity search in metric spaces. In Vldb (Vol. 97, pp. 426-435).

* Antol, Matej a Dohnal, Vlastislav. Towards Artificial Priority Queues for Similarity Query Execution. In 2018 IEEE 34th International Conference on Data Engineering Workshops (ICDEW). Paris, France: IEEE, 2018. s. 78-83. ISBN 978-1-5386-6306-6. doi:10.1109/ICDEW.2018.00020.

If this code helps you in your research, please, refer to these publications (typically the first one).

## Contact Person

Vlastislav Dohnal, [homepage](https://is.muni.cz/auth/osoba/dohnal?lang=en)
Jan Sedmidubsky, [homepage](https://is.muni.cz/auth/osoba/sedmidubsky?lang=en)


## Licence

It is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program.  If not, see http://www.gnu.org/licenses/.
